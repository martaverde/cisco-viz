

#ifndef __scene_03_data_display__
#define __scene_03_data_display__

#include "ofxScene.h"
#include "ofxHapPlayer.h"
#include "ofxParagraph.h"
#include "dataInput.h"

class scene_03_data_display : public ofxScene {
public:
    void setup();
    void update();
    void draw();    
	
    ofxHapPlayer player;
    ofxHapPlayer player2;
    ofxHapPlayer player3;

    string URL;
	float percentage;
    int frameCount;
    int duration;
    int framesTotal;
	string copy;
    float catchUpSpeed ;
    int mode;
    int posX, posY;
    int posYfinal;
    int randomNumber;
    std::string number;
    std::string what;
    
    ofTrueTypeFont ciscoSans, ciscoSansLight, ciscoSans2;
    int typeSize;
    
    vector<ofxParagraph*> paragraphs;
    ofxParagraph paragraph;
    
    
    void setupFeatured();
    void updateFeatured();
    void drawFeatured();
    
    
    void setupSecondary();
    void updateSecondary();
    void drawSecondary();
    
    
    void setupBricks();
    void updateBricks();
    void drawBricks();
    
    ofFbo text1; //2 cajas de texto normales
    ofFbo text2; //caja de texto normal con paragraph
    string text2Claim;
    
    
    
    
    string secondaryCopy;
    float posYSecondary;
    float destinationPosYSecondary;
    
};

#endif 
