#include "scene_07_claim.h"

void scene_07_claim::setup() {
    data = dataInput::getInstance();
    URL = data->getPath();
    
    player.load("videos/claim/claim.mov");
	player.play();
	player.setLoopState(OF_LOOP_NONE);
}

void scene_07_claim::update() {
    data->update();
    player.update();
    
    percentage = player.getPosition();
    data->sendStatePercentage(percentage);
    
    if (player.getIsMovieDone() == 1) {
        data->sendStateIsFinished();
    }
}

void scene_07_claim::draw() {
	ofBackground(0);
	if (player.isLoaded()){
		ofSetColor(255, 255, 255);
		player.draw(0, 0, player.getWidth(), player.getHeight());
	}

	ofSetColor(255);
    ofDrawBitmapString(percentage, 20, 200);

	ofDrawBitmapString("CLAIM", 20, 300);
	// Draw the FPS display
	ofSetColor(255);
	ofDrawBitmapString(ofToString(ofGetFrameRate(), 0) + " FPS", 20, 20);
}

