
#ifndef __scene_06_call_to_action__
#define __scene_06_call_to_action__

#include "ofxScene.h"
#include "ofxHapPlayer.h"
#include "dataInput.h"


class scene_06_call_to_action : public ofxScene {
public:
    void setup();
    void update();
    void draw(); 

	ofxHapPlayer player;

	float percentage;

	string copy;
};

#endif /* defined(__example_Simple__FirstScene__) */
