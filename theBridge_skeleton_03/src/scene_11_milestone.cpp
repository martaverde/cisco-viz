
#include "scene_11_milestone.h"

//CHECK FRAMERATE


void scene_11_milestone::setup() {
    CiscoSansBold.load("fonts/CiscoSansTTBold.ttf", 230);
    CiscoSansLight.load("fonts/CiscoSansTTLight.ttf", 20);
    CiscoSansOblique.load("fonts/CiscoSansTTLightOblique.ttf", 30);
    textLayer.allocate(ofGetWidth(), ofGetHeight(), GL_RGBA);
    compoundTextLayer.allocate(ofGetWidth(), ofGetHeight(), GL_RGBA);
    
    data = dataInput::getInstance();
    URL = data->getPath(); //?????

    player1.load("videos/milestone/mile_0.mov");
	player1.play();
	player1.setLoopState(OF_LOOP_NONE);

    player2.load("videos/milestone/mile_1.mov");
    player2.play();
    player2.setLoopState(OF_LOOP_NONE);
    
    player3.load("videos/milestone/mile_2.mov");
    player3.play();
    player3.setLoopState(OF_LOOP_NONE);
    
    
    spacing = 50;
    
    for (int i=0; i<2; i++) {
        ofxParagraph* p = new ofxParagraph();
        p->setColor(white);
        p->drawBorder(ofColor::fromHex(0x777777));
        p->drawWordBoundaries();
        paragraphs.push_back(p);
    }
    
    pWidth = ofGetWidth() - 1000;
    int pFontSize = 60;
    int pFontSize2 = 90;
    paragraphs[0]->setFont(ofxSmartFont::add("fonts/CiscoSansTTBold.ttf", pFontSize, "cisco-bold"));
    paragraphs[1]->setFont(ofxSmartFont::add("fonts/CiscoSansTTLightOblique.ttf", pFontSize2, "cisco-oblique"));
  
    for (int i=0; i<paragraphs.size(); i++){
        paragraphs[0]->setWidth(pWidth);
        paragraphs[0]->setSpacing(pFontSize*.7);
        paragraphs[0]->setText("Our network is more powerful than ever. Enabling a total of 53.400 connected devices right in this second.");
        paragraphs[0]->setPosition(ofGetWidth()/2 - paragraphs[0]->getWidth()/2, ofGetHeight()/3);
        paragraphs[0]->setAlignment(ofxParagraph::ALIGN_CENTER);
        
        paragraphs[1]->setText("connected devices");
        paragraphs[1]->setPosition(ofGetWidth()/2 + spacing, ofGetHeight()/3 - paragraphs[1]->getHeight()/2);
        paragraphs[1]->setAlignment(ofxParagraph::ALIGN_LEFT);
    }
    
    
    
        darkBlue = ofColor(22, 73, 108);
        lightBlue = ofColor(27, 168, 223);
    
        posYText = ofGetWidth();
    
    
    claimNumber = "800000";
}



/*
 
 Caja de texto completa 1
 
 
 caja de text bounding Box + párrafo
 
 
*/
void scene_11_milestone::update() {
    data->update();
    
    copy = data->getCopy();
    
    player1.update();
    player2.update();
    player3.update();
    
    percentage = player1.getPosition();
    data->sendStatePercentage(percentage);
    
    if (player1.getIsMovieDone() == 1) {
        data->sendStateIsFinished();
    }
    
    //****** get frameCount
    duration = player1.getDuration();
    framesTotal = duration * 30;
    frameCount = ofMap(percentage,0,1,0,framesTotal);
    //******
    
    //***** TEXT WITH BLENDING
    textLayer.begin();
        for (int i=0; i<paragraphs.size(); i++){
            paragraphs[0]->draw();
        }
    
        ofEnableBlendMode(OF_BLENDMODE_MULTIPLY);
        ofSetColor(darkBlue);
        ofDrawRectangle(ofGetMouseX(), ofGetMouseY(), 200, 200);
        ofDisableBlendMode();
    
    textLayer.end();

    if(frameCount > 17*30){
         posYText = posYText - 35;
         std::cout << posYText << endl;
     }
    
    
    
    compoundTextLayer.begin();
        ofRectangle number = CiscoSansBold.getStringBoundingBox(claimNumber,0,0);
    
        claim2Width = paragraphs[1]->getWidth() + number.getWidth() + spacing;
    
        ofRectangle textBox = CiscoSansBold.getStringBoundingBox(claimNumber, 0, 0);
        CiscoSansBold.drawString(claimNumber, ofGetWidth()/2 - textBox.getWidth(), player1.getHeight()/3);
    
        for (int i=0; i<paragraphs.size(); i++){
            paragraphs[1]->draw();
        }
    
    compoundTextLayer.end();
}

void scene_11_milestone::draw() {
	ofBackground(0);
	if (player1.isLoaded()){
		ofSetColor(255, 255, 255);
		player1.draw(0, 0, player1.getWidth(), player1.getHeight());
	}
    
    if (player2.isLoaded()){
        ofSetColor(255, 255, 255);
        player2.draw(0, 0, player2.getWidth(), player2.getHeight());
    }

      ofSetColor(255);
  
   // if(frameCount > 28*30 && frameCount < 32*30){
        //segundo 28-32 TEXTO GORDO single + parrafo
      //  ofPushMatrix();
     //   ofTranslate(ofGetWidth()/2, 0);
            compoundTextLayer.draw(ofGetWidth()/2 - claim2Width,0);
     //   ofPopMatrix();
  //  }
    
    
    if(frameCount > 17*30 && frameCount < 29*30){
        ofSetColor(lightBlue);
        CiscoSansBold.drawString("Our network is more powerful than ever.", posYText, ofGetHeight()/3);
    }
    
    if(frameCount > 33*30){
        //DRAW COPY TEXT BOLD & Oblique en blanco PARRAFO
        textLayer.draw(0,0);
    }

    //segundo 33 - final claim párrafo
    if (player3.isLoaded()){
        ofSetColor(255, 255, 255);
        player3.draw(0, 0, player3.getWidth(), player3.getHeight());
    }
  

	ofSetColor(255);
	ofDrawBitmapString(percentage, 20, 200);
	ofDrawBitmapString("milestone", 20, 300);
    
    ofDrawBitmapString(frameCount, 20, 500);
    
	// Draw the FPS display
	ofSetColor(255);
	ofDrawBitmapString(ofToString(ofGetFrameRate(), 0) + " FPS", 20, 20);
}

