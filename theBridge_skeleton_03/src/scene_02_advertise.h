
#ifndef __scene_02_advertise__
#define __scene_02_advertise__

#include "ofxScene.h"
#include "ofxHapPlayer.h"
#include "dataInput.h"


class scene_02_advertise : public ofxScene {
public:
    void setup();
    void update();
    void draw();
   
	ofxHapPlayer player;

	string URL;
	float percentage;
    dataInput *data;
    
    
};

#endif /* defined(__example_Simple__FirstScene__) */
