

#include "scene_01_welcome.h"
//#include "oscManager.h"

void scene_01_welcome::setup() {
	player.load("ad/imagine/Output_ad_imagine.mov");
	player.play();
	player.setLoopState(OF_LOOP_NONE);
}

void scene_01_welcome::update() {
	player.update();

	//std::cout << "position :" << percentage << endl;
	//std::cout << "end :" << player.getIsMovieDone() << endl;
	//OSCpointer
	/*
    oscManager *data = oscManager::getInstance();
	data->update();
	copy = data->getCopy();


	percentage = player.getPosition();
	data->sendStatePercentage(percentage);

	if (player.getIsMovieDone() == 1) {
		data->sendStateIsFinished();
	}*/
}

void scene_01_welcome::draw() {
	ofBackground(0);

	if (player.isLoaded()){
		ofSetColor(255, 255, 255);
		player.draw(0, 0);
	}

	ofSetColor(255);
	ofDrawBitmapString(copy, 20, 100);
	ofDrawBitmapString(percentage, 20, 200);

	ofDrawBitmapString("WELCOME Screen", 20, 300);

	// Draw the FPS display
	ofSetColor(255);
	ofDrawBitmapString(ofToString(ofGetFrameRate(), 0) + " FPS", 20, 20);

}

