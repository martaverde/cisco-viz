
#ifndef __scene_07_claim__
#define __scene_07_claim__

#include "ofxScene.h"
#include "ofxHapPlayer.h"
#include "dataInput.h"


class scene_07_claim : public ofxScene {
public:
    void setup();
    void update();
    void draw();
   
    ofxHapPlayer player;
    float percentage;
    dataInput *data;
    string URL;
};

#endif 
