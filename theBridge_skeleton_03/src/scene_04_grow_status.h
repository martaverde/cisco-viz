
#ifndef __scene_04_grow_status__
#define __scene_04_grow_status__

#include "ofxScene.h"
#include "ofxHapPlayer.h"
#include "Brick.h"


class scene_04_grow_status : public ofxScene {
public:
    void setup();
    void update();
    void draw();
	void keyReleased(int key);
	void drawGrid();
    
	float percentage;

	ofxHapPlayer player;

	string copy;

	vector <Brick> groupOfBricks;

	float initTime;
	float endPosition;
	float position;

	int ancho = 56;
	vector <ofVec2f> gridPositions;
    
    bool move;
};

#endif /* defined(__example_Simple__FirstScene__) */
