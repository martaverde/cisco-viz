
#ifndef __scene_09_keynote__
#define __scene_09_keynote__

#include "ofxScene.h"
#include "ofxHapPlayer.h"
#include "dataInput.h"

class scene_09_keynote : public ofxScene {
public:
    void setup();
    void update();
    void draw();
	ofxHapPlayer player;
	float percentage;
    dataInput *data;
    string URL;
};

#endif
