#include "scene_03_data_display.h"
#include "dataInput.h"

//CHEKEAR URL

//CON DATA IN

/*
Featured - 3 capas de video. 2 momentos de texto
Secondary - 2 capas de video. 1 momento de texto
 */

//CAMBIAR BLENDING (COLOR) SEGUN MODO

void scene_03_data_display::setup() {
    //URL = data->getPath();
    mode = 0;
    
    ofEnableSmoothing();
    ofEnableAntiAliasing();
    catchUpSpeed = 0.05f;
    
    if(mode == 0) setupFeatured();
    if(mode == 1) setupSecondary();
    
    text1.allocate(ofGetWidth(), ofGetHeight(), GL_RGBA);
    text2.allocate(ofGetWidth(), ofGetHeight(), GL_RGBA);
    
}

void scene_03_data_display::update() {
    if(mode == 0) updateFeatured();
    if(mode == 1) updateSecondary();
	dataInput *data;
//  data->update();
   copy = data->getCopy();

	percentage = player.getPosition();
	data->sendStatePercentage(percentage);
  
    /*
	if (player.getIsMovieDone() == 1) {
		data->sendStateIsFinished();
	}
 */
}

void scene_03_data_display::draw() {
        ofBackground(0);

    if(mode == 0) drawFeatured();
    if(mode == 1 ) drawSecondary();
    
    ofSetColor(255);
	ofDrawBitmapString(copy, 20, 100);
	ofDrawBitmapString(percentage, 20, 200);
    ofDrawBitmapString(duration, 20, 250);
    ofDrawBitmapString(frameCount, 20, 270);
    
	ofDrawBitmapString("DATA Display with COPYS", 20, 320);

    ofSetColor(255);
	ofDrawBitmapString(ofToString(ofGetFrameRate(), 0) + " FPS", 20, 20);
 
    
}

//====== FEATURED

void scene_03_data_display::setupFeatured(){
    
    for (int i=0; i<1; i++) {
        ofxParagraph* p = new ofxParagraph();
        p->setColor(ofColor(255));
        p->drawBorder(ofColor::fromHex(0x777777));
        p->drawWordBoundaries();
        paragraphs.push_back(p);
    }
    
    text2Claim = "Lorem ipsum doloramet, 583 adipiscin, elit, sed eiusmod incididunt ut labore dolore";
   
    int pWidth = ofGetWidth() - 1200;
    int pFontSize = 90;
   
    paragraphs[0]->setFont(ofxSmartFont::add("fonts/CiscoSansTTBold.ttf", pFontSize, "cisco-bold"));
    
    for (int i=0; i<paragraphs.size(); i++){
        paragraphs[0]->setWidth(pWidth);
        paragraphs[0]->setSpacing(pFontSize*.7);
        paragraphs[0]->setText(text2Claim);
        paragraphs[0]->setPosition(ofGetWidth()/2 - paragraphs[0]->getWidth()/2, ofGetHeight()/3 - pFontSize);
        paragraphs[0]->setAlignment(ofxParagraph::ALIGN_CENTER);
    }
    
    typeSize = 120;
    
    ciscoSans.load("fonts/CiscoSansTTBold.ttf", typeSize, true,true);
    ciscoSans.setLineHeight(34.0f);
    ciscoSans.setLetterSpacing(1.035);
    ciscoSansLight.load("fonts/CiscoSansTTLight.ttf", typeSize, true, true);
    ciscoSansLight.setLineHeight(34.0f);
    ciscoSansLight.setLetterSpacing(1.035);
    
    ciscoSans2.load("fonts/CiscoSansTTBold.ttf", typeSize-40, true,true);
    
    
    posX= 0;
    posY= 230;
    posYfinal = 20;
    what = "patatas";
    
    player.load("videos/data_display/behaviour/featured/feat_beh_0.mov");
    player2.load("videos/data_display/behaviour/featured/feat_beh_1.mov");
    player3.load("videos/data_display/behaviour/featured/feat_beh_2.mov");
    
    player.play();
    player2.play();
    player3.play();
    
    
}
void scene_03_data_display::updateFeatured(){
    player.setLoopState(OF_LOOP_NONE);
    player2.setLoopState(OF_LOOP_NONE);
    player3.setLoopState(OF_LOOP_NONE);
    
    randomNumber = ofRandom(100, 999);
    
    number = ofToString(randomNumber);
    player.update();
    player2.update();
    player3.update();
    
    duration = player2.getDuration();
    framesTotal = duration * 60;
    
    percentage = player2.getPosition();
    frameCount = ofMap(percentage, 0, 1, 0, framesTotal);
    
   
    if(frameCount >= 486) posY = catchUpSpeed * posYfinal + (1-catchUpSpeed) * posY;
    
    //PRIMER TEXTO. NUMBER + WHAT
    text1.begin();
        ofClear(0,0);
        ofPushMatrix();
        ofRectangle rect = ciscoSans.getStringBoundingBox(number, 0,0);
        ofRectangle rect2 = ciscoSansLight.getStringBoundingBox(what, 0,0);
        int rectangleComplete = rect.width + rect2.width + typeSize/2 ;
        ofTranslate(player3.getWidth()/2 - rectangleComplete/2, posY);
        ofSetColor(255);
        ciscoSans.drawString(number, 0 ,120 );
        ofSetColor(107, 189, 82);
        ciscoSansLight.drawString(what, rect.width + typeSize/2, 120 );
        ofPopMatrix();
    text1.end();
    
    //SEGUNDO TEXTO. PÁRRAFO
    text2.begin();
        paragraphs[0]->draw();
    
        ofEnableBlendMode(OF_BLENDMODE_MULTIPLY);
        ofSetColor(255,0,0);
        ofDrawRectangle(ofGetMouseX(), ofGetMouseY(), 200, 200);
        ofDisableBlendMode();
    
    text2.end();
    

}
void scene_03_data_display::drawFeatured(){
    
    if (player.isLoaded()){
        ofSetColor(255, 255, 255);
        player.draw(0, 0, player.getWidth(), player.getHeight());
    }
    
    if(frameCount >= 486 && frameCount <= 780){
        text1.draw(0,0);
    }
    
    if (player2.isLoaded()){
        ofSetColor(255, 255, 255);
        player2.draw(0, 0, player2.getWidth(), player2.getHeight());
    }
    
    
    if(frameCount > 900 && frameCount < 1560 ){
        text2.draw(0,0);
    }
    
    
    if (player3.isLoaded()){
        ofSetColor(255, 255, 255);
        player3.draw(0, 0, player3.getWidth(), player3.getHeight());
    }
}



//===== SECONDARY

void scene_03_data_display::setupSecondary(){
    player.load("videos/data_display/behaviour/secondary/sec_beh_0.mov");
    player2.load("videos/data_display/behaviour/secondary/sec_beh_1.mov");
    player.play();
    player2.play();
    player.setLoopState(OF_LOOP_NONE);
    player2.setLoopState(OF_LOOP_NONE);
    
    
    secondaryCopy = "Mauris suscipit, massa sed malesuada tistique, arcu neque molestie nunc, ut 3612 sagittis dolor eget arcu.";

    for (int i=0; i<1; i++) {
        ofxParagraph* p = new ofxParagraph();
        p->setColor(ofColor(255));
        p->drawBorder(ofColor::fromHex(0x777777));
        p->drawWordBoundaries();
        paragraphs.push_back(p);
    }
    
    int pWidth = ofGetWidth() - 1200;
    int pFontSize = 90;
    paragraphs[0]->setFont(ofxSmartFont::add("fonts/CiscoSansTTBold.ttf", pFontSize, "cisco-bold"));
    
    for (int i=0; i<paragraphs.size(); i++){
        paragraphs[0]->setWidth(pWidth);
        paragraphs[0]->setSpacing(pFontSize*.7);
        paragraphs[0]->setText(secondaryCopy);
        paragraphs[0]->setPosition(ofGetWidth()/2 - paragraphs[0]->getWidth()/2, ofGetHeight()/3 - pFontSize);
        paragraphs[0]->setAlignment(ofxParagraph::ALIGN_CENTER);
    }
    
    destinationPosYSecondary = - pFontSize * 6;
}

void scene_03_data_display::updateSecondary(){
    player.update();
    player2.update();
    duration = player.getDuration();
    framesTotal = duration * 60;

    percentage = player.getPosition();
    frameCount = ofMap(percentage, 0, 1, 0, framesTotal);
 
    
    text2.begin();
        ofClear(0,0);
        paragraphs[0]->draw();
    text2.end();
    
    if(frameCount > 13*60){
        posYSecondary = catchUpSpeed * destinationPosYSecondary + (1-catchUpSpeed) * posYSecondary;
    }
    
}

void scene_03_data_display::drawSecondary(){
    if (player.isLoaded()){
        ofSetColor(255, 255, 255);
        player.draw(0, 0, player.getWidth(), player.getHeight());
    }
    
    if(frameCount > 5 * 60){
        text2.draw(0, posYSecondary);
    }
    
    if (player2.isLoaded()){
        ofSetColor(255, 255, 255);
        player2.draw(0, 0, player2.getWidth(), player2.getHeight());
    }
}


