#include "ofApp.h"
//#include "oscManager.h"
#include "scene_0_initial.h"
#include "scene_01_welcome.h"
#include "scene_02_advertise.h"
#include "scene_03_data_display.h"
#include "scene_04_grow_status.h"
#include "scene_05_pillar_dump.h"
#include "scene_06_call_to_action.h"
#include "scene_07_claim.h"
#include "scene_08_recap.h"
#include "scene_09_keynote.h"
#include "scene_10_CAE.h"
#include "scene_11_milestone.h"

//#include "json.h"
#include "dataInput.h"

//--------------------------------------------------------------
void ofApp::setup(){
	XMLinit.loadFile("mySettings.xml");

	posX = XMLinit.getValue("WINDOW_POSITION", 0);

	ofSetWindowPosition(posX, 0);
   
	//SCENES STUFF
	scene_0_initial* initial = new scene_0_initial;
	scene_01_welcome* welcome = new scene_01_welcome;
	scene_02_advertise* advertise = new scene_02_advertise;
	scene_03_data_display* dataDisplay = new scene_03_data_display;
	scene_04_grow_status* growStatus = new scene_04_grow_status;
	scene_05_pillar_dump* pillarDump = new scene_05_pillar_dump;
	scene_06_call_to_action* callToAction = new scene_06_call_to_action;
	scene_07_claim* claim = new scene_07_claim;
	scene_08_recap* recap = new scene_08_recap;
	scene_09_keynote* keynote = new scene_09_keynote;
	scene_10_CAE* CAE = new scene_10_CAE;
    scene_11_milestone* milestone = new scene_11_milestone;
    
	sceneManager.addScene(ofPtr<ofxScene>(initial));
	sceneManager.addScene(ofPtr<ofxScene>(welcome));
	sceneManager.addScene(ofPtr<ofxScene>(advertise));
	sceneManager.addScene(ofPtr<ofxScene>(dataDisplay));
	sceneManager.addScene(ofPtr<ofxScene>(growStatus));
	sceneManager.addScene(ofPtr<ofxScene>(pillarDump));
	sceneManager.addScene(ofPtr<ofxScene>(callToAction));
	sceneManager.addScene(ofPtr<ofxScene>(claim));
	sceneManager.addScene(ofPtr<ofxScene>(recap));
	sceneManager.addScene(ofPtr<ofxScene>(keynote));
	sceneManager.addScene(ofPtr<ofxScene>(CAE));
    sceneManager.addScene(ofPtr<ofxScene>(milestone));
    
	sceneManager.setExitByTime(false);

    sceneManager.setTransitionDissolve();

	sceneManager.run();
	sceneManager.changeScene(3);


    
}

//--------------------------------------------------------------
void ofApp::update(){
    //=======
    dataInput *object2 = dataInput::getInstance();
    object2->update();
    //========
    
    
   // cout << "recibo recibo State" << object2->getState() << "\n";
   // cout << "recibo recibo StartState" << object2->startState << "\n";
  
	
	if (object2->startState) {
		if (object2->getState() == 1) sceneManager.changeScene(1);
		if (object2->getState() == 2) sceneManager.changeScene(2);
		if (object2->getState() == 3) sceneManager.changeScene(3);
		if (object2->getState() == 4) sceneManager.changeScene(4);
		if (object2->getState() == 5) sceneManager.changeScene(5);
		if (object2->getState() == 6) sceneManager.changeScene(6);
		if (object2->getState() == 7) sceneManager.changeScene(7);
		if (object2->getState() == 8) sceneManager.changeScene(8);
		if (object2->getState() == 9) sceneManager.changeScene(9);
		if (object2->getState() == 10) sceneManager.changeScene(10);

		object2->startState = false;
	}
    
    //scenemanager update
    sceneManager.update();
}

//--------------------------------------------------------------
void ofApp::draw(){
	ofBackground(0);
	sceneManager.draw();
    ofSetColor(255);
 
}

//--------------------------------------------------------------
/*
void ofApp::startState(){
    statePercent = 0.f;
    lastElapsedTime = ofGetElapsedTimef();
}
*/
//--------------------------------------------------------------
/*
void ofApp::updateState(){
    if(ofGetElapsedTimef() < lastElapsedTime + durPercent){
        statePercent = ofMap(ofGetElapsedTimef(), lastElapsedTime, lastElapsedTime+durPercent, 0.f, 1.f);
        oscManager.sendStatePercentage(statePercent);
    } else {
        finishState();
        startState();
    }
}*/

//--------------------------------------------------------------
/*
void ofApp::finishState(){
    oscManager.sendStateIsFinished(); 
}
*/
//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    if (key == '1') sceneManager.changeScene(1);
    if (key == '2') sceneManager.changeScene(2);
    if (key == '3') sceneManager.changeScene(3);
    if (key == '4') sceneManager.changeScene(4);
    if (key == '5') sceneManager.changeScene(5);
    if (key == '6') sceneManager.changeScene(6);
    if (key == '7') sceneManager.changeScene(7);
    if (key == '8') sceneManager.changeScene(8);
    if (key == '9') sceneManager.changeScene(9);
    if (key == '0') sceneManager.changeScene(10);

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------

void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
