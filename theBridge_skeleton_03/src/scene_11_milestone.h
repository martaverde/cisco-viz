
#ifndef __scene_11_milestone__
#define __scene_11_milestone__

#include "ofxScene.h"
#include "ofxHapPlayer.h"
#include "dataInput.h"
#include "ofxParagraph.h"

class scene_11_milestone : public ofxScene {
public:
    void setup();
    void update();
    void draw();
    
    ofxHapPlayer player1;
    ofxHapPlayer player2;
    ofxHapPlayer player3;
    
    float percentage;
    dataInput *data;
    string URL;
    string copy;
    
    ofTrueTypeFont CiscoSansBold;
    ofTrueTypeFont CiscoSansLight;
    ofTrueTypeFont CiscoSansOblique;
    vector<ofxParagraph*> paragraphs;
    ofxParagraph paragraph;
    int pWidth;
    
    int framesTotal;
    int frameCount;
    int duration;
    
    ofFbo textLayer;
    ofFbo compoundTextLayer;
    
    ofColor darkBlue;
    ofColor lightBlue;
    ofColor white;
    
    float posYText;
    
    int claim2Width;
    
    string claimNumber;
    int spacing;
    
    
};

#endif /* defined(__example_Simple__FirstScene__) */
