

#include "scene_02_advertise.h"
#include "dataInput.h"


void scene_02_advertise::setup() {
    data = dataInput::getInstance();
    URL = data->getPath();
    //player.load(URL);
    player.load("videos/Ad/Cisco/cisco_0.mov");
   
    player.play();
	player.setLoopState(OF_LOOP_NONE);
    
}

void scene_02_advertise::update() {
    data->update();
	player.update();

    percentage = player.getPosition();
    data->sendStatePercentage(percentage);
   
    if (player.getIsMovieDone() == 1) {
        data->sendStateIsFinished();
    }
    
    
}

void scene_02_advertise::draw() {
	ofBackground(0);

	if (player.isLoaded()){
		ofSetColor(255, 255, 255);
		player.draw(0, 0);
	}

	ofSetColor(255);
	ofDrawBitmapString(URL, 20, 100);
	ofDrawBitmapString(percentage, 20, 200);

	ofDrawBitmapString("ADVERTISE Screen", 20, 300);
	// Draw the FPS display
	ofSetColor(255);
	ofDrawBitmapString(ofToString(ofGetFrameRate(), 0) + " FPS", 20, 20);

}

