

#pragma once
#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxJSON.h"
#include "ofxXmlSettings.h"


class dataInput{
    
private:
    dataInput(void);
    static std::unique_ptr<dataInput> myInstance;
    
public:
    
    void update();
   
    
    //=====================
    int LOCAL_PORT;
    string REMOTE_IP;
    int REMOTE_PORT;
    ofxXmlSettings XML;
    
    // Receive OSC
    void receiveMessages();
    ofxOscReceiver receiver;
    // Send OSC
    void okConnected();
    void okStartState();
    void okNextState();
    void sendResponse(string response);
    void sendStatePercentage(float percent);
    void sendStateIsFinished();
    ofxOscSender sender;
    
    // Utils: color function
    ofColor getColor(string colorName);
    
    bool startState;

	
	int stateId;
    string stateName;
    string topicName;
    string topicColorName;
    ofColor topicColor;
    string copy;
    string dataPath;
    string otherMsg;
	string recap;
    int getState();
    string getCopy();
    string getPath();
	string getRecap();
    //=====================
    ~dataInput(void);
    
    static dataInput * getInstance()
    {
        if (myInstance.get() == nullptr)
            myInstance = std::unique_ptr<dataInput>(new dataInput);
        return static_cast<dataInput *>(myInstance.get());
    }
    
    static dataInput & get_fucking_instance()
    {
        return *getInstance();
    }
    
  
    
    
    
};


