#include "scene_09_keynote.h"
#include "dataInput.h"

//solo video

void scene_09_keynote::setup() {
    data = dataInput::getInstance();
    URL = data->getPath();
    
    //player.load(URL);
    player.load("identity/Featured/Output_Bridge_Featured_Identity_Layer_1.mov");
	player.play();
	player.setLoopState(OF_LOOP_NONE);
}

void scene_09_keynote::update() {
    data->update();
    player.update();
    
    percentage = player.getPosition();
    data->sendStatePercentage(percentage);
    
    if (player.getIsMovieDone() == 1) {
        data->sendStateIsFinished();
    }
}

void scene_09_keynote::draw() {
    ofBackground(0);

	if (player.isLoaded()){
		ofSetColor(255, 255, 255);
		player.draw(0, 0, player.getWidth(), player.getHeight());
	}

	ofSetColor(255);
	ofDrawBitmapString(percentage, 20, 200);
	ofDrawBitmapString("KEYNOTE TIME", 20, 300);
	// Draw the FPS display
	ofSetColor(255);
	ofDrawBitmapString(ofToString(ofGetFrameRate(), 0) + " FPS", 20, 20);
}

