

#include "scene_04_grow_status.h"
#include "Brick.h"

/*
 Añadir json reader
*/


void scene_04_grow_status::setup() {
	ofEnableAntiAliasing();
	ofEnableSmoothing();
}

void scene_04_grow_status::update() {
	for (int i = 0; i < groupOfBricks.size(); i++) {
		groupOfBricks[i].update();
	}
}

void scene_04_grow_status::draw() {

	ofBackground(0);
	drawGrid();

	for (int i = ancho / 2; i < ofGetWidth(); i = i + ancho) {
		for (int j = -ancho / 2; j < ofGetHeight(); j = j + ancho) {
			ofSetColor(255);
			ofDrawEllipse(i, j, 5, 5);
			ofVec2f tempPoint = ofVec2f(i, j);
			gridPositions.push_back(tempPoint);
		}
	}

	for (int i = 0; i < groupOfBricks.size(); i++) {
		groupOfBricks[i].draw();
	}

	ofSetColor(255);
	ofDrawBitmapString("GROW STATUS", 20, 300);
	// Draw the FPS display
	ofDrawBitmapString(ofToString(ofGetFrameRate(), 0) + " FPS", 20, 20);
}


void scene_04_grow_status::keyReleased(int key) {
    if(key == 'a'){
       int tempTypeBrick = int(ofRandom(4));
       Brick tempBrick;
       ofVec2f initPos = gridPositions[ofRandom(gridPositions.size())];
       // ofVec2f initPos = gridPositions[3];
       ofVec2f endPos = ofVec2f(ofRandom(ofGetWidth(), ofRandom(ofGetHeight())));
       tempBrick.setup(initPos, endPos, ancho, tempTypeBrick, groupOfBricks.size());

       groupOfBricks.push_back(tempBrick);

       std::cout << "Type of new brick: " << tempTypeBrick << endl;
       std::cout << "Number of bricks: " << groupOfBricks.size() << endl;
    }
       
    

}

void scene_04_grow_status::drawGrid() {
	for (int i = 0; i < ofGetWidth(); i = i + 56) {
		ofSetColor(255);
		ofDrawLine(i, 0, i, ofGetHeight());
	}
	for (int j = 0; j < ofGetHeight(); j = j + 56) {
		ofDrawLine(0, j, ofGetWidth(), j);
	}
}

