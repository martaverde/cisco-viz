#include "scene_05_pillar_dump.h"

//CONECTAR CON PILLAR

void scene_05_pillar_dump::setup() {
	
}

void scene_05_pillar_dump::update() {
	
}

void scene_05_pillar_dump::draw() {

	ofBackground(0);

	ofSetColor(255);

	ofDrawBitmapString("PILLAR DUMP", 20, 300);
	
    // Draw the FPS display
	ofSetColor(255);
	ofDrawBitmapString(ofToString(ofGetFrameRate(), 0) + " FPS", 20, 20);
}

