#include "scene_08_recap.h"

//MODO BILLBOARD


/*
Añadir json reader
*/


void scene_08_recap::setup() {
    
	data = dataInput::getInstance();


    ofBuffer buffer = ofBufferFromFile("lorem.txt");
    
    for(auto line : buffer.getLines()){
        stringGroup.push_back(line);
    }
    
    std::cout << stringGroup.size() << endl;
    
    testString = "There is a 3.246 super long text about data and fucking nice connections between data servers from Cisco to Domestic Data Streamers";
    ancho = 56;
    ciscoSansBold.load("fonts/CiscoSansTTBold.ttf", 28);
    plantilla.load("bridge_base.png");
    
    for(int i = -ancho/2; i < ofGetHeight()/2; i = i + ancho){
        verticalPositions.push_back(i);
    }
    
    
}

void scene_08_recap::update() {
	data->update();

	recap = data->getRecap();

    for(int i = 0; i < groupOfBlocks.size(); i++){
        groupOfBlocks[i].update();
    }
/*
	//OSCpointer
	oscManager *data = oscManager::getInstance();
	data->update();

	copy = data->getCopy();

	percentage = player.getPosition();
	data->sendStatePercentage(percentage);

	if (player.getIsMovieDone() == 1) {
		data->sendStateIsFinished();
	}
 */
}

void scene_08_recap::draw() {

	ofBackground(0);

    ofSetColor(255, 100);
    plantilla.draw(0,0);

   
    for(int i = 0; i < verticalPositions.size(); i++){
        ofDrawLine(0, verticalPositions[i], ofGetWidth(), verticalPositions[i]);
    }
    
    for(int i = 0; i < groupOfBlocks.size(); i++){
        groupOfBlocks[i].draw();
    }
    
    
	ofSetColor(255,0,0);
	ofDrawBitmapString(copy, 20, 100);
	ofDrawBitmapString(percentage, 20, 200);
	ofDrawBitmapString("HOUR RECAP", 20, 300);
	// Draw the FPS display
	ofSetColor(255);
	ofDrawBitmapString(ofToString(ofGetFrameRate(), 0) + " FPS", 20, 20);
    
}


void scene_08_recap::keyReleased(int key){
    /*
    recapBlock tempBlock;
    ofVec2f pos = ofVec2f(ofGetWidth(), verticalPositions[int(ofRandom(verticalPositions.size()))]);
    tempBlock.setup(pos, int(ofRandom(10)), stringGroup[int(ofRandom(20))]);
    groupOfBlocks.push_back(tempBlock);
    */
    
    //plantilla.draw(0,0);
    
    for(int i = 1; i < groupOfBlocks.size()+1; i++){
        groupOfBlocks.clear();
    }
    
    for ( int p = 0; p < 7; p++){
         recapBlock tempBlock;
        //ofVec2f pos = ofVec2f(ofGetWidth() + blockWidth, verticalPositions[int(ofRandom(verticalPositions.size()))]);
        
        ofVec2f pos = ofVec2f(56/2, verticalPositions[1]);
      
        if(groupOfBlocks.size() == 1 || groupOfBlocks.size() == 0){
            tempBlock.setup(pos , int(ofRandom(10)), stringGroup[0]);
        }
    
        if(groupOfBlocks.size() > 1){
            tempBlock.setup(ofVec2f(groupOfBlocks[p-1].getBlockPosition() + 3 + groupOfBlocks[p-1].getBlockWidth(), pos.y)  , int(ofRandom(10)), stringGroup[p]);
        }
        
        groupOfBlocks.push_back(tempBlock);
    
        //std::cout << groupOfBlocks[0].getBlockWidth() << endl;
    }

    
    
    
}

