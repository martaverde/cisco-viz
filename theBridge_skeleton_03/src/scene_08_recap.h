
#ifndef __scene_08_recap__
#define __scene_08_recap__

#include "ofxScene.h"
#include "recapBlock.h"
#include "ofMain.h"
#include "dataInput.h"


class scene_08_recap : public ofxScene {
public:
    void setup();
    void update();
    void draw();
    void keyReleased(int key);
    //BOLD
	
    float percentage;
	string copy;
    ofImage plantilla;
    
    ofTrueTypeFont ciscoSansBold;
    
    string testString;
    
    vector <recapBlock> groupOfBlocks;
    int tempTypeBlock;

    vector <int> verticalPositions;
    int ancho;
    
	string recap;

	dataInput *data;
    vector <string> stringGroup;
};

#endif /* defined(__example_Simple__FirstScene__) */
