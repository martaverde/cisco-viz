#include "scene_06_call_to_action.h"
#include "dataInput.h"


void scene_06_call_to_action::setup() {
	player.load("videos/cta.mov");
	player.play();
	player.setLoopState(OF_LOOP_NONE);
}

void scene_06_call_to_action::update() {
    dataInput *data = dataInput::getInstance();
    data->update();
    
    percentage = player.getPosition();
    data->sendStatePercentage(percentage);
	player.update();
    
    if (player.getIsMovieDone() == 1) {
        data->sendStateIsFinished();
    }
    
}

void scene_06_call_to_action::draw() {
    ofBackground(0);

	if (player.isLoaded()){
		ofSetColor(255, 255, 255);
		player.draw(0, 0, player.getWidth(), player.getHeight());
	}



	ofSetColor(255);
	ofDrawBitmapString(percentage, 20, 200);

	ofDrawBitmapString("CALL TO ACTION STATIC VIDEO", 20, 300);

    ofSetColor(255);
	ofDrawBitmapString(ofToString(ofGetFrameRate(), 0) + " FPS", 20, 20);
}

