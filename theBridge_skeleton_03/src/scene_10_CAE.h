
#ifndef __scene_10_CAE__
#define __scene_10_CAE__

#include "ofxScene.h"
#include "ofxHapPlayer.h"
#include "dataInput.h"

class scene_10_CAE : public ofxScene {
public:
    void setup();
    void update();
    void draw();
    
    ofxHapPlayer player;
    float percentage;
    dataInput *data;
    string URL;
};

#endif /* defined(__example_Simple__FirstScene__) */
