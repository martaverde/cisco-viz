#include "jsoninput.h"

std::unique_ptr<jsoninput> jsoninput::myInstance = std::unique_ptr<jsoninput>(nullptr);

jsoninput::jsoninput(void){
    // listen on the given port
    cout << "listening for osc messages on port " << PORT << "\n";
    receiver.setup(PORT);
    // open an outgoing connection to HOST:PORT
    sender.setup(HOST, SENDPORT);
    
    ofxOscMessage m;
    m.setAddress("/beef");
    m.addIntArg(1);
    sender.sendMessage(m, false);
}

void jsoninput::update(){
    
    while(receiver.hasWaitingMessages()){
        // get the next message
        ofxOscMessage m;
        receiver.getNextMessage(m);
        msg_string = m.getAddress();
        msg_string += ": ";
        
        inputString = msg_string;
        result.parse(inputString);
        
		type = result["type"].asInt();
		stringData = result["data"].asString();    
    }
    
}




int jsoninput::getType(){
    return type;
}


string jsoninput::getStringData() {
	return stringData;


}


jsoninput::~jsoninput(void){
}
