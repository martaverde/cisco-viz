//
//  Brick.cpp
//  cisco_v1
//
//  Created by Marta Verde on 14/12/18.
//

#include "Brick.h"

Brick::Brick(){
}

void Brick::setup(ofVec2f _initPos, ofVec2f _endPos, int _dim, int _type, int _id){
    x =  _initPos.x;
    y =  _initPos.y;
    dim = _dim;
    type = _type;
    endPos = _endPos;
    brick_id = _id;
    speedX = ofRandom(-1, 1);
    speedY = ofRandom(-1, 1);
    
    counter = 0;

	green = ofColor(107, 189, 82);
	red = ofColor(221, 42, 39);
	yellow = ofColor(242, 166, 40);
	blue = ofColor(27, 168, 223);

	darkGreen = ofColor(34, 92, 58);
	darkRed = ofColor(150, 25, 33);
	darkBlue = ofColor(22, 73, 108);
	darkYellow = ofColor(221, 42, 39);


}

void Brick::update(){
   
    counter = counter + 0.5;
    
    /*
    ofVec2f morePos = initPos + endPos;
    
    x = morePos.x;
    y = morePos.y;
    */
    
    /*
    if(x < 0 ){
        x = 0;
        speedX *= -1;
    } else if(x > ofGetWidth()){
        x = ofGetWidth();
        speedX *= -1;
    }
    
    if(y < 0 ){
        y = 0;
        speedY *= -1;
    } else if(y > ofGetHeight()){
        y = ofGetHeight();
        speedY *= -1;
    }
    
    x+=speedX;
    y+=speedY;
     */
    
    
}

void Brick::draw(){
    if(type == 0){
        backColor = yellow;
        frontColor = darkYellow;
        
    }
    if(type == 1){
        backColor = red;
        frontColor = darkRed;
    }
    
    if(type == 2){
        backColor = blue;
        frontColor = darkBlue;
    }
    
    if(type == 3){
        backColor = green;
        frontColor = darkGreen;
    }
    
    ofSetRectMode(OF_RECTMODE_CENTER);
    ofSetColor(backColor);
    ofDrawRectangle(x, y, dim, dim);
    
    ofPushMatrix();
    
    ofTranslate(x, y);
    
            if(type == 0)   ofRotate(counter*2);
            if(type == 1)   ofRotate(-counter);
            if(type == 2)   ofRotate(-counter*2);
            if(type == 3)   ofRotate(counter);
   
            ofSetColor(frontColor);
    
            if(type == 0)  simpleHexagon(0,0, dim/4);
            if(type == 1)  simpleTriangle(0,0, dim/2, dim/2);
            if(type == 2)  simpleDiamond(0,0, (dim/3)+10, (dim/3)+5);
            if(type == 3)  simpleHexagon(0,0, dim/4);
    
    ofSetColor(255);
    ofDrawBitmapString(brick_id, 0,0);
    
    ofPopMatrix();
}

void Brick::moveTo(ofVec2f _finalPos){
    x = _finalPos.x;
    y = _finalPos.y;
}


void Brick::simpleDiamond(float x, float y, float width, float height){
    ofBeginShape();
        ofVertex(x-width/2,y);  //left
        ofVertex(x, y + height/2); //bottom
        ofVertex(x+width/2, y); //right
        ofVertex(x, y - height/2);
    ofEndShape();

}

void Brick::simpleHexagon(float x, float y, float gs){
    ofBeginShape();
    for(int i=0;i<6;i++){
        float angle = i * 2 * PI / 6;
        ofVertex(x + gs*cos(angle),y + gs*sin(angle));
    }
    ofEndShape();
}

void Brick::simpleTriangle(float x, float y, float w, float h){
    float xArray[3];
    float yArray[3];
    
    float currentAngle = -HALF_PI;
    float wr = w * 0.5;
    float hr = h * 0.5;
    
    for (int i = 0; i < 3; i++) {
        xArray[i] = x + wr*cos(currentAngle);
        yArray[i] = y + hr*sin(currentAngle);
        
        currentAngle = currentAngle + ofDegToRad(360 / 3);
    }
    
    ofDrawTriangle(xArray[0], yArray[0],
                   xArray[1], yArray[1],
                   xArray[2], yArray[2]);
}


