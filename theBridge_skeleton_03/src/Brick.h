//
//  Brick.hpp
//  cisco_v1
//
//  Created by Marta Verde on 14/12/18.
//



//Settings Needed:

/*
 
Type of brick - > color front and back

animation:
Initial position
Final position
 
Cuando se está animando (boolean)
 

Brick id con settings guardados (guardar en XML?) (posicion FINAL)
*/


#ifndef Brick_h
#define Brick_h

#include <stdio.h>
#include "ofMain.h"



class Brick {
    
public:
 
    void setup(ofVec2f _initPos, ofVec2f _endPos , int _dim, int _type, int _id);
    void update();
    void draw();
    void moveTo(ofVec2f _finalPos);
    ofVec2f initPos;
    ofVec2f endPos;
    float x;
    float y;
    float speedY;
    float speedX;
    int dim;
    
    ofColor backColor;
    ofColor frontColor;
    
    float counter;
    int brick_id;
    int type;
    
    ofColor green;
    ofColor red;
    ofColor yellow;
    ofColor blue;
    
    ofColor darkGreen;
    ofColor darkRed;
    ofColor darkBlue;
    ofColor darkYellow;
    
    void simpleTriangle(float x, float y, float w, float h);
    void simpleHexagon(float x, float y, float gs);
    void simpleDiamond(float x, float y, float width, float height);
        
    float initTime;
    float endPosition;
    float position;
    
    Brick();
    
private:
};

#endif



