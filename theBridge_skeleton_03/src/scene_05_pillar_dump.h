
#ifndef __scene_05_pillar_dump__
#define __scene_05_pillar_dump__

#include "ofxScene.h"
#include "ofxHapPlayer.h"

class scene_05_pillar_dump : public ofxScene {
public:
    void setup();
    void update();
    void draw();
    
	ofxHapPlayer player;

	float percentage;

	string copy;
};

#endif /* defined(__example_Simple__FirstScene__) */
