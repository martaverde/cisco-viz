

#pragma once
#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxJSON.h"

//ESPECIFICAR PUERTOS PARA ENVIAR BRICKS

// listen on port 12345
#define PORT 7500
#define NUM_MSG_STRINGS 20

#define HOST "localhost"
#define SENDPORT 8500

class jsoninput{
    
private:
    jsoninput(void);
    static std::unique_ptr<jsoninput> myInstance;
    
public:
    
    void update();
    
	~jsoninput(void);
    
    static jsoninput * getInstance()
    {
        if (myInstance.get() == nullptr)
            myInstance = std::unique_ptr<jsoninput>(new jsoninput);
        return static_cast<jsoninput *>(myInstance.get());
    }
    
    static jsoninput & get_fucking_instance()
    {
        return *getInstance();
    }
    
    ofxOscReceiver receiver;
    ofxOscSender sender;
    
    ofxJSONElement result;
    std::string inputString;
    
    string msg_string;
    
	
	
	int getType();
	string getStringData();


	int type;
	string stringData;
    
    
    
};


