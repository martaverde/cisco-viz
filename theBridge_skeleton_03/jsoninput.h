

#pragma once
#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxJSON.h"
#include "ofxXmlSettings.h"


class jsoninput{
    
private:
    jsoninput(void);
    static std::unique_ptr<jsoninput> myInstance;
    
public:
    
    void update();
   
    
    //=====================
    int LOCAL_PORT;
    string REMOTE_IP;
    int REMOTE_PORT;
    ofxXmlSettings XML;
    
    // Receive OSC
    void receiveMessages();
    ofxOscReceiver receiver;
    // Send OSC
    void okConnected();
    void okStartState();
    void okNextState();
    void sendResponse(string response);
    void sendStatePercentage(float percent);
    void sendStateIsFinished();
    ofxOscSender sender;
    
    // Utils: color function
    ofColor getColor(string colorName);
    
    bool startState;
    
    int stateId;
    string stateName;
    string topicName;
    string topicColorName;
    ofColor topicColor;
    string copy;
    string dataPath;
    string otherMsg;
    
    int getState();
    string getCopy();
    
    
    //=====================
    ~jsoninput(void);
    
    static jsoninput * getInstance()
    {
        if (myInstance.get() == nullptr)
            myInstance = std::unique_ptr<jsoninput>(new jsoninput);
        return static_cast<jsoninput *>(myInstance.get());
    }
    
    static jsoninput & get_fucking_instance()
    {
        return *getInstance();
    }
    
  
    
    
    
};


