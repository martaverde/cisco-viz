/*
#pragma once

#include "ofxOsc.h"
#include "ofxXmlSettings.h"


//#define LOCAL_PORT 12345
//#define REMOTE_IP "localhost"
//#define REMOTE_PORT 20001

class oscManager{
private:
	oscManager(void);
	static std::unique_ptr<oscManager> myInstance;


public:
    void setup(int localPort, string remoteIp, int remotePort);
    void update();
    
    // Receive OSC
    void receiveMessages();
    ofxOscReceiver receiver;
	ofxXmlSettings XML;
    // Send OSC
    void okConnected();
    void okStartState();
    void okNextState();
    void sendResponse(string response);
    void sendStatePercentage(float percent);
    void sendStateIsFinished();
    ofxOscSender sender;

    // Utils: color function
    ofColor getColor(string colorName);
    
    bool startState;
    
    int stateId;
    string stateName;
    string topicName;
    string topicColorName;
    ofColor topicColor;
    string copy;
    string dataPath;
    string otherMsg;


	int LOCAL_PORT;
	string REMOTE_IP;
	int REMOTE_PORT;


	int getState();
	string getCopy();


	~oscManager(void);

	static oscManager * getInstance() {
		if (myInstance.get() == nullptr) {
			myInstance = std::unique_ptr<oscManager>(new oscManager);
			return static_cast <oscManager *>(myInstance.get());
		}
	}

	static oscManager & get_fucking_instance() {
		return *getInstance();
	}
};
 
 */
