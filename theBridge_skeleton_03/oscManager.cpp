
/*
#include "oscManager.h"

//std::unique_ptr<oscManager> oscManager::myInstance = std::unique_ptr<oscManager>(nullptr);
std::unique_ptr<oscManager> oscManager::myInstance = std::make_unique<oscManager>();

oscManager::oscManager(void) {
	 XML.loadFile("mySettings.xml");
	 LOCAL_PORT = XML.getValue("LOCAL_PORT",0);
	 REMOTE_IP = XML.getValue("REMOTE_IP","");
	 REMOTE_PORT = XML.getValue("REMOTE_PORT",0);
	 // Setup Sender
	 sender.setup(REMOTE_IP, REMOTE_PORT);
     // Setup Receiver
     receiver.setup(LOCAL_PORT);
	 startState = false;
}

void oscManager::update(){
    // Receive messages
    receiveMessages();
}

void oscManager::receiveMessages(){
    
    // check for waiting messages
    while(receiver.hasWaitingMessages()){
        // get the next message
        ofxOscMessage m;
        receiver.getNextMessage(m);
        
        if(m.getAddress() == "/state_id"){
            stateId = m.getArgAsInt32(0);
        }
        else if(m.getAddress() == "/state_name"){
            stateName = m.getArgAsString(0);
            sendResponse("ok_next_state");
        }
        else if(m.getAddress() == "/topic_name"){
            topicName = m.getArgAsString(0);
        }
        else if(m.getAddress() == "/topic_color_name"){
            topicColorName = m.getArgAsString(0);
            topicColor = getColor(topicColorName);
        }
        else if(m.getAddress() == "/copy"){
            copy = m.getArgAsString(0);
        }
        else if(m.getAddress() == "/path"){
            dataPath = m.getArgAsString(0);
        }
        else if(m.getAddress() == "/check_connection"){
            sendResponse("ok_connected");
        }
        else if(m.getAddress() == "/start_state"){
            sendResponse("ok_start_state");
            startState = true;
        }
        else{
            string msg_string;
            msg_string = m.getAddress();
            msg_string += ": ";
            for(int i = 0; i < m.getNumArgs(); i++){
                // get the argument type
                msg_string += m.getArgTypeName(i);
                msg_string += ":";
                // display the argument - make sure we get the right type
                if(m.getArgType(i) == OFXOSC_TYPE_INT32){
                    msg_string += ofToString(m.getArgAsInt32(i));
                }
                else if(m.getArgType(i) == OFXOSC_TYPE_FLOAT){
                    msg_string += ofToString(m.getArgAsFloat(i));
                }
                else if(m.getArgType(i) == OFXOSC_TYPE_STRING){
                    msg_string += m.getArgAsString(i);
                }
                else{
                    msg_string += "unknown";
                }
            }
            otherMsg = msg_string;
        }
    }
    
}



int oscManager::getState() {
	return stateId;
}

string oscManager::getCopy() {
	return copy;
}

//--------------------------------------------------------------
void oscManager::sendResponse(string response){
    ofxOscMessage m;
    m.setAddress("/response");
    m.addStringArg(response);
    sender.sendMessage(m);
}

//--------------------------------------------------------------
void oscManager::sendStatePercentage(float percent){
    ofxOscMessage m;
    m.setAddress("/state_percentage");
    m.addFloatArg(percent);
    sender.sendMessage(m);
}

//--------------------------------------------------------------
void oscManager::sendStateIsFinished(){
    ofxOscMessage m;
    m.setAddress("/state_finished");
    sender.sendMessage(m);
}

//--------------------------------------------------------------
ofColor oscManager::getColor(string colorName){
    ofColor color;
    if(colorName == "Yellow"){
        color = ofColor::yellow;
    }
    else if(colorName == "Green")
    {
        color = ofColor::green;
    }
    else if(colorName == "Red"){
        color = ofColor::red;
    }
    else if(colorName == "Blue"){
        color = ofColor::blue;
    }
    else
    {
        color = ofColor::black;
    }
    return color;
}


oscManager::~oscManager(void) {
}

*/

