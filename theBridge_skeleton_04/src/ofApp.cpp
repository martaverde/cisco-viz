#include "ofApp.h"
#include "oscManager.h"



string fuckData;

//--------------------------------------------------------------
void ofApp::setup(){

	mainFBO.allocate(BRIDGE_WIDTH, BRIDGE_HEIGHT, GL_RGBA);

	XMLinit.loadFile("mySettings.xml");
	ofSetFrameRate(60);
	posX = XMLinit.getValue("WINDOW_POSITION", 0);
	posX = 3840;

	ofEnableAntiAliasing();

	ofSetWindowPosition(0, 0);
   
	//SCENES STUFF
	scene_0_initial* initial = new scene_0_initial;
	scene_00_videoPlayer* videoPlayer = new scene_00_videoPlayer;
	scene_03_data_display* dataDisplay = new scene_03_data_display;

	growStatus = new scene_04_grow_status;
	growStatus->inicializa(&bricks, posicionesGrid, secuenciaPosiciones);

	pillarDump = new scene_05_pillar_dump;
	pillarDump->inicializa(&bricks, posicionesGrid, secuenciaPosiciones);

	scene_08_recap* recap = new scene_08_recap;
	scene_11_milestone* milestone = new scene_11_milestone;
	scene_13_screensaver* screensaver = new scene_13_screensaver;
	scene_12_final* final = new scene_12_final;
	scene_14_shut_down* shutdown = new scene_14_shut_down;

	//sceneManager.addScene(ofPtr<ofxScene>(initial));

	sceneManager.addScene(ofPtr<ofxScene>(videoPlayer)); //0
	sceneManager.addScene(ofPtr<ofxScene>(dataDisplay)); //1
	sceneManager.addScene(ofPtr<ofxScene>(growStatus)); //2
	sceneManager.addScene(ofPtr<ofxScene>(pillarDump)); //3
	sceneManager.addScene(ofPtr<ofxScene>(recap)); //4
	sceneManager.addScene(ofPtr<ofxScene>(milestone)); //5
	sceneManager.addScene(ofPtr<ofxScene>(screensaver)); //6
	sceneManager.addScene(ofPtr<ofxScene>(final)); //7
	sceneManager.addScene(ofPtr<ofxScene>(shutdown)); //8
	sceneManager.addScene(ofPtr<ofxScene>(initial)); //9

	sceneManager.setExitByTime(false);
    sceneManager.setTransitionDissolve();
	sceneManager.run();

	sceneManager.changeScene(3);

	setupSecuencia();
	setupGrid();
}



void ofApp::setupSecuencia()
{
	int numPosiciones = XMLinit.getNumTags("SECUENCIA_BRIDGE");

	if (numPosiciones > 0)
	{
		for (int i = 0; i < numPosiciones; i++)
		{
			string churro = XMLinit.getValue("SECUENCIA_BRIDGE", "", i);
			if (churro == "")
				cout << "ERROR: secuencia de posiciones vacia" << endl;
			else
			{
				vector<string> p = ofSplitString(churro, ",");
				if (p.size() != BRIDGE_NUM_POSICIONES_GRID)
					cout << "ERROR: la secuencia " << i << " no tiene " << BRIDGE_NUM_POSICIONES_GRID << " posiciones" << endl;
				else
				{
					for (int a = 0; a < p.size(); a++)
						secuenciaPosiciones[a] = BRIDGE_NUM_POSICIONES_GRID - ofToInt(p[a]);
				}
			}

		}
	}
	else
		cout << "ERROR: no hay sencuencias de posiciones del Bridge en el xml" << endl;

}

//--------------------------------------------------------------
void ofApp::setupGrid() {

	int contador = 0;

	for (int fila = 0; fila < 12; fila++)
		for (int columna = 0; columna < 84; columna++)
			posicionesGrid[contador++].pos = ofVec2f((ANCHO_BRICK / 2.0f) + columna * ANCHO_BRICK, (ANCHO_BRICK / 2.0f) + fila * ANCHO_BRICK);

	for (int fila = 12; fila < 18; fila++)
	{
		for (int columna = 0; columna < 18; columna++)
			posicionesGrid[contador++].pos = ofVec2f((ANCHO_BRICK / 2.0f) + columna * ANCHO_BRICK, (ANCHO_BRICK / 2.0f) + fila * ANCHO_BRICK);
		for (int columna = 66; columna < 84; columna++)
			posicionesGrid[contador++].pos = ofVec2f((ANCHO_BRICK / 2.0f) + columna * ANCHO_BRICK, (ANCHO_BRICK / 2.0f) + fila * ANCHO_BRICK);
	}

	for (int fila = 18; fila < 23; fila++)
	{
		for (int columna = 0; columna < 9; columna++)
			posicionesGrid[contador++].pos = ofVec2f((ANCHO_BRICK / 2.0f) + columna * ANCHO_BRICK, (ANCHO_BRICK / 2.0f) + fila * ANCHO_BRICK);
		for (int columna = 75; columna < 84; columna++)
			posicionesGrid[contador++].pos = ofVec2f((ANCHO_BRICK / 2.0f) + columna * ANCHO_BRICK, (ANCHO_BRICK / 2.0f) + fila * ANCHO_BRICK);
	}

	int pos = 0;
	for (int i = 0; i < 700; i++)
	{
		int gridPos = secuenciaPosiciones[i];

		ofVec2f initPos = posicionesGrid[gridPos].pos;
		int tipo = ofRandom(9);

		Brick *brick = new Brick;
		brick->setup(initPos, tipo, gridPos, 0, 0, ofGetElapsedTimeMillis());
		bricks.push_back(*brick);
		bricks.back().addAnimation(initPos, Brick::ANIMACION_BRICK::CREACION, 0);
		bricks.back().startAnimation();
		pos++;

	}
}

//--------------------------------------------------------------
void ofApp::update(){
	


	sceneManager.update();
	fuckData = "bla bla bla ";


	oscManager *data = oscManager::getInstance();
	data->update();
	
	if (data->startState) {
		if (data->getState() == 1) sceneManager.changeScene(0); 
		if (data->getState() == 2) sceneManager.changeScene(0); 
		if (data->getState() == 6) sceneManager.changeScene(0); 
		if (data->getState() == 7) sceneManager.changeScene(0); 
		if (data->getState() == 9) sceneManager.changeScene(0); 
		if (data->getState() == 10) sceneManager.changeScene(0); 

		if (data->getState() == 3)
		{
			sceneManager.changeScene(3); // 
		}
		if (data->getState() == 4) sceneManager.changeScene(4); // 
		if (data->getState() == 5) sceneManager.changeScene(5); // 
		if (data->getState() == 8) sceneManager.changeScene(8); // 
		if (data->getState() == 11) sceneManager.changeScene(11); // 
		if (data->getState() == 12) sceneManager.changeScene(12); // 
		if (data->getState() == 13) sceneManager.changeScene(13); // 
		if (data->getState() == 14) sceneManager.changeScene(14); // 
	data->startState = false;
	}
	

	mainFBO.begin();
		ofBackground(0);
		sceneManager.draw();
		//drawGrid();
	mainFBO.end();


}

//--------------------------------------------------------------
void ofApp::draw(){
	ofBackground(0);
	
	float aspect = (float)BRIDGE_WIDTH / (float)BRIDGE_HEIGHT;
	
	//mainFBO.draw(0, 0,2560,2560/aspect);
	mainFBO.draw(0, 0, 1920, 1920 / aspect);

	//drawGrid();
	//donde se dibuja, ancho alto, desde donde empieza la esquina 
	/*
	mainFBO.getTexture().drawSubsection(0, 0, 1920, 1080, 0, 0); //trozo 1
    mainFBO.getTexture().drawSubsection(1920, 0, 1920, 1080, 1920, 0); // trozo 2
	mainFBO.getTexture().drawSubsection(1920*2, 0, 864, 1080, 1920*2, 0); // trozo 2
	mainFBO.getTexture().drawSubsection(1920*2 + 864, 0, 504, 264, 0, 1080);
	mainFBO.getTexture().drawSubsection(1920*2 + 864, 264, 504, 264, 4200, 1080);
	*/

}

void ofApp::drawGrid()
{
	ofSetColor(255);
	ofNoFill();
	for (int i = 0; i < BRIDGE_NUM_POSICIONES_GRID; i++)
	{
		ofRectangle r;
		r.setFromCenter(posicionesGrid[i].pos, ANCHO_BRICK, ANCHO_BRICK);
		ofDrawRectangle(r);
		ofDrawBitmapString(ofToString(i), posicionesGrid[i].pos);
	}
}

void ofApp::keyPressed(int key){
	

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	if (key == '0') sceneManager.changeScene(0);
	if (key == '1') sceneManager.changeScene(1); 
	if (key == '2') {
		growStatus->setGrowStatus(100, 500);
		sceneManager.changeScene(2); // data display
	}
	if (key == '3') {
		secuenciaPosicionesActual += 5;
		pillarDump->setSiguiente(secuenciaPosicionesActual);

		sceneManager.changeScene(3);
	}
	if (key == '4') sceneManager.changeScene(4);
	if (key == '5') sceneManager.changeScene(5);
	if (key == '6') sceneManager.changeScene(6);
	if (key == '7') sceneManager.changeScene(7);
	if (key == '8') sceneManager.changeScene(8);
	if (key == '9') sceneManager.changeScene(9);
	
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------

void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}