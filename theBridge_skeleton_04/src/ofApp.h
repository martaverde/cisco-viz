#pragma once

#include "ofMain.h"
#include "ofxSceneManager.h"
#include "ofxXmlSettings.h"
#include "Brick.h"
#include "Constantes.h"

#include "scene_0_initial.h"
#include "scene_00_videoPlayer.h"
#include "scene_03_data_display.h"
#include "scene_04_grow_status.h"
#include "scene_05_pillar_dump.h"
#include "scene_08_recap.h"
#include "scene_11_milestone.h"
#include "scene_12_final.h"
#include "scene_13_screensaver.h"
#include "scene_14_shut_down.h"

extern string fuckData;


class ofApp : public ofBaseApp{
	public:
		void setup();
		void update();
		void draw();
		
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y);
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

	private:
		void			setupSecuencia	( );
		void			setupGrid		( );
		void			drawGrid		( );

		ofxXmlSettings	XMLinit;
		int				posX;

		ofFbo			mainFBO;
		ofTexture		mainTexture;

		ofxSceneManager sceneManager;
		scene_05_pillar_dump *pillarDump;
		scene_04_grow_status* growStatus;

		GRID_POSICIONES posicionesGrid[BRIDGE_NUM_POSICIONES_GRID];
		
		vector<Brick>	bricks;
		
		int				secuenciaPosiciones[BRIDGE_NUM_POSICIONES_GRID];
		int				secuenciaPosicionesActual = 0;

};
