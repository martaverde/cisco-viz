
#ifndef __scene_04_grow_status__
#define __scene_04_grow_status__

#include "ofxScene.h"
#include "ofxHapPlayer.h"
#include "Brick.h"
#include "oscManager.h"
#include "Constantes.h"

class scene_04_grow_status : public ofxScene {
public:
    void setup();
    void update();
    void draw();
	void keyReleased(int key);
	void inicializa(vector<Brick> *bricks, GRID_POSICIONES *pos, int *secuenciaPosiciones);
	void setGrowStatus(int grow1, int grow2);

private:

	oscManager		*dataOscManager;

	vector<Brick>   *bricks_global;
	GRID_POSICIONES	*posicionesGrid;
	int				*secuenciaPosiciones;

	//vector<Brick>   bricks_local;

	ofxHapPlayer player;

	float initTime;

	float sceneDuration;

	int grow1;
	int grow2;
	
	float percentage;
	
	int lastIndex;

};
#endif /* defined(__example_Simple__FirstScene__) */
