

#include "scene_04_grow_status.h"
#include "oscManager.h"
#include "Brick.h"
#include "Constantes.h"
//la escena en total dura 20 segundos
//5 segundos video1 + 5 segundos recap 9AM
//5 segnundos video2 + 5 segundos recap NOW

#define GROW_START 15 * 1000



//pedir a State machine dos estados de crecimiento, Array. 

void scene_04_grow_status::setup() {

	dataOscManager = oscManager::getInstance();

	sceneDuration = 20000;

	initTime = ofGetElapsedTimeMillis();

	player.load("/videos/grow_status/grow_status_bridge.mov");
	player.setLoopState(OF_LOOP_NONE);
	player.play();

}

void scene_04_grow_status::inicializa(vector<Brick> *bricks, GRID_POSICIONES *pos, int *_secuencia)
{
	this->posicionesGrid = pos;
	this->bricks_global = bricks;
	this->secuenciaPosiciones = _secuencia;

}

void scene_04_grow_status::update() {

	player.update();

	float now = ofGetElapsedTimeMillis() - initTime;

	if (now > sceneDuration) {
		dataOscManager->sendStateIsFinished();
	}

	percentage = ofMap(now, 0, sceneDuration, 0, 1, true);
	dataOscManager->sendStatePercentage(percentage);

}

void scene_04_grow_status::draw() {

	ofBackground(0);
	//ofEnableAlphaBlending();
	ofSetColor(255);
	//ofEnableBlendMode(OF_BLENDMODE_ALPHA);

	float now = ofGetElapsedTimeMillis() - initTime;

	ofPushStyle();
	if (now > 4000 && now < 12000)
	{
		for (int i = 0; i < grow1; i++) {
			//int posGrid = secuenciaPosiciones[i];
			bricks_global->at(i).draw();
		}
	}
	else if (now > GROW_START && now < sceneDuration)
	{
		for (int i = 0; i < grow2; i++) {
			bricks_global->at(i).draw();
		}
		int index = ofMap(now, GROW_START, sceneDuration - 3000, grow1, grow2 - 1, true);
		if (index > lastIndex)
			for (int i =lastIndex + 1 ; i <= index ; i++)
			{
				cout << "animando brick  " << index << endl;
				bricks_global->at(i).setScaleGlobal(1.0f);
				bricks_global->at(i).addAnimation(bricks_global->at(i).getPos(), Brick::ANIMACION_BRICK::GROW_STATUS, 1000);
				bricks_global->at(i).startAnimation();
			}

			lastIndex = index;
	}
	ofPopStyle();

	ofSetColor(255);
	player.draw(0, 0, BRIDGE_WIDTH, BRIDGE_HEIGHT);


	ofSetRectMode(OF_RECTMODE_CENTER);

	ofSetColor(255);
	ofDrawBitmapString(percentage, 20, 100);
	ofDrawBitmapString("GROW STATUS", 20, 300);

	// Draw the FPS display
	ofDrawBitmapString(ofToString(ofGetFrameRate(), 0) + " FPS", 20, 20);
}

void scene_04_grow_status::setGrowStatus(int _g1, int _g2) {

	this->grow1 = _g1;
	this->grow2 = _g2;

	for (int i = grow1; i < grow2; i++)
	{
		bricks_global->at(i).setScaleGlobal(0);
	}
	lastIndex = 0;
	cout << "fin setGrowStatus" << endl;
}

void scene_04_grow_status::keyReleased(int key) {
}


