

#include "scene_08_recap.h"
#include "Constantes.h"
//#define DURACION_ESCENA 100 //segundos

//----> check percentage 


void scene_08_recap::setup() {
	cout << "escena 8 setup" << endl;
	ofEnableSmoothing();
	dataOscManager = oscManager::getInstance();
	//recapType = dataOscManager->getRecapType();
	dataString = dataOscManager-> getRecapString();
	
	recapType = 0;

	/*
	Modo 0 hour recap
	video 12 segundos
	80 de recap

	modo 1 daily recap
	video 10 segundos
	85 de recap
	*/

	timeNow = ofGetElapsedTimeMillis();

	if (recapType == 0) {
		DURACION_ESCENA = 80;
		timeIni = 12000 + timeNow;
		timer = 80000;
		actualVideo = "/videos/recap/recapHour.mov";
	}

	if (recapType == 1) {
		DURACION_ESCENA = 85;
		timeIni = 10000 + timeNow;
		timer = 85000;
		actualVideo = "/videos/recap/recapDaily.mov";
	}

	video.load(actualVideo);
	video.play();	
	sceneDuration = timer + video.getDuration() * 1000;


	//for (auto line : buffer.getLines()) {
	//	stringGroup.push_back(line);
	//}

	//std::cout << stringGroup.size() << endl;

	//testString = "There is a 3.246 super long text about data and fucking nice connections between data servers from Cisco to Domestic Data Streamers";
	ancho = 56;
	ciscoSansBold.load("fonts/CiscoSansTTBold.ttf", 28);
	plantilla.load("bridge_base.png");
	
	for (int i = 0; i < lineas.size(); i++)
	{
		lineas[i].recapBlocks.clear();
	}
	lineas.clear();
	
	//salen 12 lineas
	for (int i = 0; i < 12; i++) {
		LINEA *l = new LINEA;
		l->posY = (ancho / 2) + ancho * i;
		l->posXInicial = 0 + ofRandom(0, 500);
		//l->posXInicial = BRIDGE_WIDTH + ofRandom(0, 500);
		l->posXSiguiente = l->posXInicial;
		l->posXAnimacion = l->posXInicial + BRIDGE_WIDTH;
		//random speed de 0.2 a 0.4
		l->speed = ofRandom(1.0f, 1.2f);
		lineas.push_back(*l);
	}
	cout << "hay " << lineas.size() << " lineas " << endl;

	ofBuffer buffer = ofBufferFromFile("lorem.txt");
	parseaRecap(buffer.getText());

	lineaRecapActual = 0;
	
}

void scene_08_recap::parseaRecap( string churro ) {

	vector<string> lineas = ofSplitString(churro, "//");
	for (int i = 0; i < lineas.size(); i++)
	{
		vector<string> datos = ofSplitString(lineas[i], "|");
		if (datos.size() == 2)
		{
			addRecap(ofToInt(datos[0]), datos[1]);
		}
		else
		{
			cout << "ERROR en el parseo de los recaps, dato numero: " << i << "=" << lineas[i] << endl;
		}
	}

}

void scene_08_recap::addRecap(int tipo, string texto) {
	
	cout << "a�adiendo recap en linea " << lineaRecapActual << " -> " <<  texto << endl;

	recapBlock *rb = new recapBlock;
	ofVec2f pos = ofVec2f( lineas[lineaRecapActual].posXSiguiente + BRIDGE_WIDTH, lineas[lineaRecapActual].posY);
	rb->setup(pos, tipo, texto);
	int width = rb->getBlockWidth();
	lineas[lineaRecapActual].recapBlocks.push_back(*rb);
	lineas[lineaRecapActual].posXSiguiente = lineas[lineaRecapActual].posXSiguiente + width + rb->getBorder();
 
	lineaRecapActual++;
	
	if (lineaRecapActual == lineas.size())
		lineaRecapActual = 0;
}


void scene_08_recap::update() {
	video.update();

	percentage = ofMap(ofGetElapsedTimeMillis() - timeNow, 0, DURACION_ESCENA * 1000 + timer, 0, 1);
	dataOscManager->sendStatePercentage(percentage);

	timerActual = ofGetElapsedTimeMillis();
		
		if (timerActual < sceneDuration) {
			int maxLongitud = 0;
			for (int i = 0; i < lineas.size(); i++)
			{
				if (lineas[i].posXSiguiente + lineas[i].posXInicial > maxLongitud)
					maxLongitud = lineas[i].posXSiguiente + lineas[i].posXInicial;
			}


			for (int i = 0; i < lineas.size(); i++)
			{
				float posX = ofMap(ofGetElapsedTimeMillis() - timeIni, 0, DURACION_ESCENA * 1000, BRIDGE_WIDTH + lineas[i].posXInicial, -lineas[i].posXSiguiente);
				posX = posX * lineas[i].speed;
				float incrX = posX - lineas[i].posXAnimacion;
				lineas[i].posXAnimacion = posX;
				for (int b = 0; b < lineas[i].recapBlocks.size(); b++)
				{
					lineas[i].recapBlocks[b].incrementaX(incrX);
				}
			}
		}

	if (ofGetElapsedTimeMillis() > sceneDuration) {
		dataOscManager->sendStateIsFinished();

	}
	
}

void scene_08_recap::draw() {
	ofBackground(0);
	video.draw(0,0, BRIDGE_WIDTH, BRIDGE_HEIGHT);

	for (int i = 0; i < lineas.size(); i++) {
	//	ofDrawLine(0, lineas[i].posY, ofGetWidth(), lineas[i].posY);
		for (int b = 0 ; b < lineas[i].recapBlocks.size() ; b++)
			lineas[i].recapBlocks[b].draw();
	}


	ofSetColor(255, 0, 0);
	ofDrawBitmapString(copy, 20, 100);
	ofDrawBitmapString(percentage, 20, 200);
	ofDrawBitmapString("RECAP", 20, 300);
	
	// Draw the FPS display
	ofSetColor(255);
	ofDrawBitmapString(ofToString(ofGetFrameRate(), 0) + " FPS", 20, 20);
}

