
#ifndef __scene_11_milestone__
#define __scene_11_milestone__

#include "ofxScene.h"
#include "ofxHapPlayer.h"
#include "oscManager.h"
#include "ofxParagraph.h"

class scene_11_milestone : public ofxScene {
public:
    void setup();
    void update();
    void draw();
    
    ofxHapPlayer player1;
    ofxHapPlayer player2;
    ofxHapPlayer player3;
    
    float percentage;

    string URL;
    string copy;
    
    ofTrueTypeFont CiscoSansBold;
    ofTrueTypeFont CiscoSansBold2;
    ofTrueTypeFont CiscoSansOblique;
	ofTrueTypeFont CiscoSansLight;
    vector<ofxParagraph*> paragraphs;
    ofxParagraph paragraph;
    int pWidth;
    
    int framesTotal;
    int frameCount;
    int duration;
    
    ofFbo textLayer;
    ofFbo compoundTextLayer;
    
    ofColor darkBlue;
    ofColor lightBlue;
    ofColor white;
    
    float posYText;
    
    int claim2Width;
    
    string claimNumber;
    int spacing;
	
	int scrollingSize;
	int numberHighlightSize;
	int whatHighlightSize;
	int copySize;

	string highlights;
    
    
};

#endif /* defined(__example_Simple__FirstScene__) */
