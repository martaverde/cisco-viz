#pragma once

#include "ofxOsc.h"
#include "ofxXmlSettings.h"


//#define LOCAL_PORT 12345
//#define REMOTE_IP "localhost"
//#define REMOTE_PORT 20001

class oscManager{
private:
	oscManager(void);
	static std::unique_ptr<oscManager> myInstance;
	string strRecap; //guardar� el churro del recap

public:
    void setup(int localPort, string remoteIp, int remotePort);
    void update();
    
    // Receive OSC
    void receiveMessages();
    ofxOscReceiver receiver;
	ofxXmlSettings XML;
	
    // Send OSC
    void okConnected();
    void okStartState();
    void okNextState();
    void sendResponse(string response);
    void sendStatePercentage(float percent);
    void sendStateIsFinished();
    ofxOscSender sender;
    
    bool startState;
    
    int stateId;
    string stateName;
    string topicName;
    string topicColorName;
    ofColor topicColor;
    string copy;
    string dataPath;
    string otherMsg;


	int LOCAL_PORT;
	string REMOTE_IP;
	int REMOTE_PORT;
	int MACHINE;

	int family;
	//string path;
	string highlight;

	int screensaver;
	int keynote;
	int welcome;
	int advertise;
	int recapType;
	int grow1;
	int grow2;
	string bricks;

	int getState();
	string getCopy();
	string getRecapString();
	int getRecapType();
	int getFamily();
	string getPath();
	string getHighlights();
	int getScreensaver();
	int getKeynote();
	int getWelcome();
	int getAdvertise();
	int getGrow1();
	int getGrow2();
	string getBricks();

	~oscManager(void);

	static oscManager * getInstance() {
		if (myInstance.get() == nullptr) {
			myInstance = std::unique_ptr<oscManager>(new oscManager);
			return static_cast <oscManager *>(myInstance.get());
		}


		return myInstance.get();
	}

	static oscManager & get_fucking_instance() {
		return *getInstance();
	}
};
