
#include "scene_13_screensaver.h"
#include "oscManager.h"
#include "Constantes.h"

void scene_13_screensaver::setup() {
	dataOscManager = oscManager::getInstance();
	//player.load("");
	//player.setLoopState(OF_LOOP_NORMAL);
}

void scene_13_screensaver::update() {
//	player.update();
	dataOscManager->update();
	//tiempo dado por maquina de estados. LOOP MODE ON
	
	time = dataOscManager->getScreensaver()*1000;

	percentage = ofMap(ofGetElapsedTimeMillis(), 0, time, 0, 1);
	//percentage = ofClamp(percentage, 0, sceneDuration);
	
	dataOscManager->sendStatePercentage(percentage);

	if (time > sceneDuration) {
		dataOscManager->sendStateIsFinished();
	}

}

void scene_13_screensaver::draw() {
	ofBackground(0);
	//player.draw();


}

