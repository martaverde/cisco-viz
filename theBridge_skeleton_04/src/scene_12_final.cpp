

#include "scene_12_final.h"
#include "oscManager.h"
#include "Constantes.h"
//brick final gigante en video
//capa de atras bricks colocados en alpha
//duraci�n por determinar

void scene_12_final::setup() {

	dataOscManager = oscManager::getInstance();
	//pedir tiempo de duraci�n de la escena a la state machine 
	sceneDuration = 10000;
	
	player.load("/videos/final_brick/finalBrick.mov");
	player.play();
	player.setLoopState(OF_LOOP_NORMAL);
	timer = 0;

}

void scene_12_final::update() {
	dataOscManager->update();
	player.update();

	timer = timer + ofGetElapsedTimeMillis();

	percentage = ofMap(ofGetElapsedTimeMillis(), 0, sceneDuration, 0, 1);
	percentage = ofClamp(percentage, 0, sceneDuration);
	
	dataOscManager->sendStatePercentage(percentage);

	if (timer > sceneDuration) {
		dataOscManager->sendStateIsFinished();
	}

}

void scene_12_final::draw() {
	ofBackground(0);
	
	if (player.isLoaded()) {
		player.draw(0, 0, ofGetWidth(), ofGetHeight());
	}


	ofSetColor(255);
	ofDrawBitmapString("final SCENE", 20, 20);

	ofDrawBitmapString(percentage, 20, 40);
}

