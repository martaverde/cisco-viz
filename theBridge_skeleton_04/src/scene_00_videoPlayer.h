
#ifndef __scene_01_welcome__
#define __scene_01_welcome__

#include "ofxScene.h"
#include "ofxHapPlayer.h"
#include "oscManager.h"

class scene_00_videoPlayer : public ofxScene {
public:
    void setup(ofPtr<ofxScene> previousScene);
	string newPath;

    void update();
    void draw();
   
	float percentage;
	string path;
	float duration;

	ofxHapPlayer video;
	oscManager *dataOscManager;


};

#endif
