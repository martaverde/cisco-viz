
#ifndef __example_Simple__ZeroScene__
#define __example_Simple__ZeroScenee__

#include "ofxScene.h"
#include "ofxHapPlayer.h"
#include "oscManager.h"

class scene_13_screensaver : public ofxScene {
public:
	void setup();
	void update();
	void draw();
	
	oscManager *dataOscManager;
	float time;
	//float timer;
	float percentage;
	float sceneDuration;

	ofxHapPlayer player;

};

#endif /* defined(__example_Simple__FirstScene__) */