#include "scene_00_videoPlayer.h"
#include "Constantes.h"
#include "scene_0_initial.h"

void scene_00_videoPlayer::setup(ofPtr<ofxScene> previousScene) {
	scene_0_initial* scene = dynamic_cast<scene_0_initial *>(previousScene.get());



//	newPath = scene->mondongo;
	dataOscManager = oscManager::getInstance();
    path = dataOscManager->getPath();
	cout << "desde la escena, path= " << path << endl;
	//path = "/videos/advertise/cisco_0.mov";
	video.load(path);
	video.play();
	video.setLoopState(OF_LOOP_NONE);
	duration = video.getDuration();
}

void scene_00_videoPlayer::update() {
	video.update();

	dataOscManager->update();

	percentage = video.getPosition();
	dataOscManager->sendStatePercentage(percentage);

	if (video.getIsMovieDone() == 1) {
		dataOscManager->sendStateIsFinished();
	}
}

void scene_00_videoPlayer::draw() {
	ofBackground(0);

	if (video.isLoaded()) {
		ofSetColor(255, 255, 255);
		video.draw(0, 0, BRIDGE_WIDTH, BRIDGE_HEIGHT);
	}

	ofSetColor(255);

	ofDrawBitmapString(percentage, 20, 200);
	ofDrawBitmapString(path, 20, 400);
	// Draw the FPS display
	ofSetColor(255);
	ofDrawBitmapString(ofToString(ofGetFrameRate(), 0) + " FPS", 20, 20);

}

