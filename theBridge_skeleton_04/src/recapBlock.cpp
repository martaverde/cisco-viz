//
//  Brick.cpp
//  cisco_v1
//
//  Created by Marta Verde on 14/12/18.
//

#include "recapBlock.h"




void recapBlock::setup(ofVec2f _Pos, int _type, string _inputString){
	cout << "ini recapBlock setup" << endl;
    x =  _Pos.x;
    y =  _Pos.y;
    dim = 56;
    type = _type;
    inputString = _inputString;
    
    counter = 0;
    ciscoSansBold.load("fonts/CiscoSansTTBold.ttf", 28);
    //speed = ofRandom(0.001, 0.005);
    speed = 0.01;
    catchUpSpeed = 0.05f;
    
    green = ofColor(107, 189, 82);
    red = ofColor(221, 42, 39);
    yellow = ofColor(242, 166, 40);
    blue = ofColor(27, 168, 223);
    
    darkGreen = ofColor(34, 92, 58);
    darkRed = ofColor(150, 25, 33);
    darkBlue = ofColor(22, 73, 108);
    darkYellow = ofColor(221, 42, 39);
    
    border = 3;
   
	ofRectangle backRect = ciscoSansBold.getStringBoundingBox(inputString, 0, 0);
	blockWidth = (backRect.getWidth() + dim * 1.25) - border;
	cout << "fin recapBlock setup" << endl;
}

float recapBlock::getBorder()
{
	return border;
}

void recapBlock::incrementaX(float _x)
{
	x = x + _x;
}

void recapBlock::update(){

    
    /*
    
    counter = counter + speed;
    x = x - counter;
    
    if(x < -blockWidth ){
        x = ofGetWidth();
    }
    
    */
    
/*
    if(type == 0)   std::cout << "security" << endl;
    if(type == 1)   std::cout << "data"  << endl;
    if(type == 2)   std::cout << "clouds"  << endl;
    if(type == 3)   std::cout << "connections"  << endl;
    if(type == 4)   std::cout << "collaborations"  << endl;
    if(type == 5)   std::cout << "conversations"  << endl;
    if(type == 6)   std::cout << "identity"  << endl;
    if(type == 7)   std::cout << "behaviour"  << endl;
    if(type == 8)   std::cout << "learning"  << endl;
    if(type == 9)   std::cout << "insight"  << endl;
*/
    
}


float recapBlock::getBlockWidth(){
    //ofRectangle backRect = ciscoSansBold.getStringBoundingBox(inputString,0,0);
    //blockWidth = (backRect.getWidth() + dim * 1.25 ) - border;
    return blockWidth;
    
}

float recapBlock::getBlockPosition(){
    return x;
}


void recapBlock:: zenoToPoint(ofVec2f finalPos){
    x = catchUpSpeed * finalPos.x + (1-catchUpSpeed) * x;
    y = catchUpSpeed * finalPos.y + (1-catchUpSpeed) * y;  
}

void recapBlock::draw(){
	if(type == 0 || type == 1 || type == 2){
        backColor = yellow;
        frontColor = darkYellow;
    }
    if(type == 3 || type == 4 || type == 5){
        backColor = red;
        frontColor = darkRed;
    }
    
    if(type == 6 || type == 7 || type == 8){
        backColor = green;
        frontColor = darkGreen;
    }
    
    if(type == 9){
        backColor = blue;
        frontColor = darkBlue;
    }
    
    //ofSetRectMode(OF_RECTMODE_CENTER);
    //ofSetColor(backColor);
   // ofDrawRectangle(x, y, dim, dim);
    
    ofPushMatrix();
    
    ofTranslate(x, y);
    
        ofSetRectMode(OF_RECTMODE_CORNER);
    
        ofSetColor(backColor);
    
        ofDrawRectangle(-dim/2, -dim/2, blockWidth, dim-border);
    
   
            ofSetColor(frontColor);
			if (type == 0)   simpleTriangle(0, 5, (dim / 1.25), (dim / 1.25));
			if (type == 1)   simpleHexagon(0, 0, (dim / 2.85));

			if (type == 2)   simpleDiamond(0, 0, (dim / 2) + 10, (dim / 2) + 5);
			if (type == 3)   simpleHexagon(0, 0, (dim / 2.85));

			if (type == 4)   simpleDiamond(0, 0, (dim / 2) + 10, (dim / 2) + 5);
			if (type == 5)   simpleTriangle(0, 5, (dim / 1.25), (dim / 1.25));
			if (type == 6)   simpleDiamond(0, 0, (dim / 2) + 10, (dim / 2) + 5);
			if (type == 7)   simpleTriangle(0, 5, dim / 1.25, dim / 1.25);
			if (type == 8)   simpleHexagon(0, 0, dim / 2.85);
			if (type == 9)   ofDrawEllipse(0, 0, dim / 1.6, dim / 1.6);
    
    
     ofSetColor(frontColor);
     ciscoSansBold.drawString(inputString, dim/2, dim/4);
    
    ofPopMatrix();
}


void recapBlock::interiorAnimation(){
    
    
    
    
    /*
    tipo de easing
    tiempos random 1 y 2 de wait
    diferenciar entre entrada y salida
    */
    
    
}


void recapBlock::keyReleased(int key){
    if(key == 's'){
        movingBricks = true;
        std::cout << "semuevemueve " << endl;
    }
}



void recapBlock::simpleDiamond(float x, float y, float width, float height){
    ofBeginShape();
        ofVertex(x-width/2,y);  //left
        ofVertex(x, y + height/2); //bottom
        ofVertex(x+width/2, y); //right
        ofVertex(x, y - height/2);
    ofEndShape();
}

void recapBlock::simpleHexagon(float x, float y, float gs){
    ofBeginShape();
    for(int i=0;i<6;i++){
        float angle = i * 2 * PI / 6;
        ofVertex(x + gs*cos(angle),y + gs*sin(angle));
    }
    ofEndShape();
}

void recapBlock::simpleTriangle(float x, float y, float w, float h){
    float xArray[3];
    float yArray[3];
    
    float currentAngle = -HALF_PI;
    float wr = w * 0.5;
    float hr = h * 0.5;
    
    for (int i = 0; i < 3; i++) {
        xArray[i] = x + wr*cos(currentAngle);
        yArray[i] = y + hr*sin(currentAngle);
        
        currentAngle = currentAngle + ofDegToRad(360 / 3);
    }
    
    ofDrawTriangle(xArray[0], yArray[0],
                   xArray[1], yArray[1],
                   xArray[2], yArray[2]);
}


