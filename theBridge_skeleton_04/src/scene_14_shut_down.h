
#ifndef __scene_14_shut_down__
#define __scene_14_shut_down__

#include "ofxScene.h"
#include "oscManager.h"

class scene_14_shut_down : public ofxScene {
public:
    void setup();
    void update();
    void draw();
   
	float percentage;
	float sceneDuration;

	oscManager *dataOscManager;
	string bricks;

};

#endif
