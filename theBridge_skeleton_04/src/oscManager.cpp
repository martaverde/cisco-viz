#include "oscManager.h"

//--------------------------------------------------------------
/*
void oscManager::setup(int localPort, string remoteIp, int remotePort){
    
    // Setup Receiver
    receiver.setup(localPort);
    
    // Setup Sender
    sender.setup(remoteIp, remotePort);
    
    startState = false;
}*/


std::unique_ptr<oscManager> oscManager::myInstance = std::unique_ptr<oscManager>(nullptr);


oscManager::oscManager(void) {
	 XML.loadFile("mySettings.xml");

	 LOCAL_PORT = XML.getValue("LOCAL_PORT",0);
	 REMOTE_IP = XML.getValue("REMOTE_IP","");
	 REMOTE_PORT = XML.getValue("REMOTE_PORT",0);
	 MACHINE = XML.getValue("MACHINE", 0);
	 cout << "listening for osc messages on port " << LOCAL_PORT << "\n";
	// Setup Receiver
	receiver.setup(LOCAL_PORT);

	// Setup Sender
	sender.setup(REMOTE_IP, REMOTE_PORT);
	startState = false;
}


oscManager::~oscManager(void) {

}

//--------------------------------------------------------------
void oscManager::update(){
    // Receive messages
    receiveMessages();
}

//--------------------------------------------------------------
void oscManager::receiveMessages(){
    
    // check for waiting messages
    while(receiver.hasWaitingMessages()){
        // get the next message
        ofxOscMessage m;
        receiver.getNextMessage(m);
        
        if(m.getAddress() == "/state_id"){
            stateId = m.getArgAsInt32(0);
        }
        else if(m.getAddress() == "/state_name"){
            stateName = m.getArgAsString(0);
            sendResponse("ok_next_state");
        }
        else if(m.getAddress() == "/topic_name"){
            topicName = m.getArgAsString(0);
        }
        
        else if(m.getAddress() == "/copy"){
            copy = m.getArgAsString(0);
        }
		else if (m.getAddress() == "/hightlights") {
			highlight = m.getArgAsString(0);
		}
        else if(m.getAddress() == "/path"){
			cout << "desde el oscManager, path= " << dataPath << endl;

            dataPath = m.getArgAsString(0);
        }
        else if(m.getAddress() == "/check_connection"){
            sendResponse("ok_connected");
        }
        else if(m.getAddress() == "/start_state"){
            sendResponse("ok_start_state");
            startState = true;
        }
		else if (m.getAddress() == "/recap") {
			strRecap = m.getArgAsString(1);
			recapType = m.getArgAsInt32(0);
		}
		//else if (m.getAddress() == "/path") {
		//	path = m.getArgAsString(0);
		//}
		else if (m.getAddress() == "/family") {
			family = m.getArgAsInt32(0);
		}
		else if (m.getAddress() == "/screensaver") {
			screensaver = m.getArgAsInt32(0);
		}
		else if (m.getAddress() == "/keynote") {
			keynote = m.getArgAsInt32(0);
		}
		else if (m.getAddress() == "/welcome") {
			welcome = m.getArgAsInt32(0);
		}
		else if (m.getAddress() == "/advertise") {
			advertise = m.getArgAsInt32(0);
		}
		else if (m.getAddress() == "/grow") {
			grow1 = m.getArgAsInt(0);
			grow2 = m.getArgAsInt(1);
		}
		else if (m.getAddress() == "/bricks") {
			bricks = m.getArgAsString(0);
		}
		
        else{
            string msg_string;
            msg_string = m.getAddress();
            msg_string += ": ";
            for(int i = 0; i < m.getNumArgs(); i++){
                // get the argument type
                msg_string += m.getArgTypeName(i);
                msg_string += ":";
                // display the argument - make sure we get the right type
                if(m.getArgType(i) == OFXOSC_TYPE_INT32){
                    msg_string += ofToString(m.getArgAsInt32(i));
                }
                else if(m.getArgType(i) == OFXOSC_TYPE_FLOAT){
                    msg_string += ofToString(m.getArgAsFloat(i));
                }
                else if(m.getArgType(i) == OFXOSC_TYPE_STRING){
                    msg_string += m.getArgAsString(i);
                }
                else{
                    msg_string += "unknown";
                }
            }
            otherMsg = msg_string;
        }
    }
    
}


int oscManager::getState() {
	return stateId;
}
string oscManager::getPath() {
	return dataPath;
}
string oscManager::getCopy() {
	return copy;
}
int oscManager::getFamily() {
	return family;
}
string oscManager::getHighlights() {
	return highlight;
}
int oscManager::getScreensaver() {
	return screensaver;
}
int oscManager::getKeynote() {
	return keynote;
}
int oscManager::getWelcome() {
	return welcome;
}
int oscManager::getAdvertise() {
	return advertise;
}

int oscManager::getRecapType() {
	return recapType;
}

string oscManager::getRecapString() {
	return strRecap;
}

int oscManager::getGrow1() {
	return grow1;
}
int oscManager::getGrow2() {
	return grow2;
}

string oscManager::getBricks() {
	return bricks;
}
//--------------------------------------------------------------
void oscManager::sendResponse(string response){
    ofxOscMessage m;
    m.setAddress("/"+ ofToString(MACHINE)+"/response");
    m.addStringArg(response);
    sender.sendMessage(m);
}

//--------------------------------------------------------------
void oscManager::sendStatePercentage(float percent){
    ofxOscMessage m;
    m.setAddress("/" + ofToString(MACHINE) + "/state_percentage");
    m.addFloatArg(percent);
    sender.sendMessage(m);
}

//--------------------------------------------------------------
void oscManager::sendStateIsFinished(){
    ofxOscMessage m;
    m.setAddress("/" + ofToString(MACHINE) + "/state_finished");
    sender.sendMessage(m);
}

