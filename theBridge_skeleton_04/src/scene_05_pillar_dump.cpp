

#include "scene_05_pillar_dump.h"
#include "oscManager.h"
#include "Constantes.h"

//video 10 segundos
//25 segundos animación colocacion brick 

void scene_05_pillar_dump::setup() {
	dataOscManager = oscManager::getInstance();
	sceneDuration = 35000;

	if (bFirstTime)
	{
		receiver.setup(PORT);
		bFirstTime = false;
		std::cout << "DUMP Listener listening for messages on port " << PORT << endl;
		player.load(VIDEO_PATH);
		player.setLoopState(OF_LOOP_NONE);
	}

	//player.stop();
	player.setPosition(0);
	player.play();

	azulPosActual = 0;
	variosPosActual = 0;
	//brickGrandeCentroPos = posicionesGrid[84 * 5 + 84 / 2].pos;
	brickGrandeCentroPos = (84 * 5) + (84 / 2);
	timeIni = ofGetElapsedTimeMillis();
	bricks_local.clear();

	estado = ESTADO_INTRO;

}

void scene_05_pillar_dump::setSiguiente(int _s)
{
	this->siguientePosicion = _s;
}

void scene_05_pillar_dump::inicializa(vector<Brick> *bricks, GRID_POSICIONES *pos, int *_secuencia)
{
	this->posicionesGrid = pos;
	this->bricks_global = bricks;
	this->secuenciaPosiciones = _secuencia;

}

void scene_05_pillar_dump::update() {

	player.update();

	percentage = ofMap(ofGetElapsedTimeMillis() - timeIni, 0, sceneDuration, 0, 1);
	dataOscManager->sendStatePercentage(percentage);

	for (int i = 0; i < bricks_local.size(); i++)
		bricks_local[i].update();

	if (ofGetElapsedTimeMillis() > sceneDuration) {
		dataOscManager->sendStateIsFinished();
	}

	while (receiver.hasWaitingMessages()) {
		ofxOscMessage m;
		receiver.getNextMessage(m);
		if (m.getAddress() == "/brick_arrival") {
			int idPillar = m.getArgAsInt(0);
			int brickType = m.getArgAsInt(1);
			int brickPos = m.getArgAsInt(2);
			brickArrival(idPillar, brickType, brickPos);
		}
	}
	
	updateStatus();

}


void scene_05_pillar_dump::updateStatus( )
{
	int duracionFade = 1000;
	int duracionCreacion = 1500;
	int duracionScale = 1000;

	if (estado == ESTADO_INTRO)
	{
		if (ofGetElapsedTimeMillis() - timeIni > durationIntro)
			estado = ESTADO_LLEGADA;
	}

	if (estado == ESTADO_LLEGADA)
	{
		if (ofGetElapsedTimeMillis() - timeIni > durationIntro + durationLlegada)
		{
			estado = ESTADO_AGRUPAR;
			//hay que hacer desaparecer el interior de los bricks azules
			for (int i = 0; i < bricks_local.size(); i++)
			{
				bricks_local[i].addAnimation(bricks_local[i].getPos(), Brick::ANIMACION_BRICK::FADE_INSIDE, duracionFade);
				bricks_local[i].startAnimation();
			}
		}
	}

	if (estado == ESTADO_AGRUPAR)
	{
		if (ofGetElapsedTimeMillis() - timeIni > durationIntro + durationLlegada + duracionFade)
		{
			estado = ESTADO_CREAR;
			bricks_local.clear();
			//hay que crear el brick gordo
			Brick *brick = new Brick;
			brick->setup(posicionesGrid[brickGrandeCentroPos].pos, 9, bricks_local.size(), 0, 0, ofGetElapsedTimeMillis());
			bricks_local.push_back(*brick);
			bricks_local.back().addAnimation(posicionesGrid[brickGrandeCentroPos].pos, Brick::ANIMACION_BRICK::CREACION, duracionCreacion);
			bricks_local.back().setScaleGlobal(5.0f);
			bricks_local.back().startAnimation();
		}
	}

	if (estado == ESTADO_CREAR)
	{
		if (ofGetElapsedTimeMillis() - timeIni > durationIntro + durationLlegada + duracionFade + duracionCreacion)
		{
			estado = ESTADO_REDUCIR;
			bricks_local.back().addAnimation(posicionesGrid[brickGrandeCentroPos].pos, Brick::ANIMACION_BRICK::RESCALE, duracionScale );
			bricks_local.back().startAnimation();
		}
	}

	if (estado == ESTADO_REDUCIR)
	{
		if (ofGetElapsedTimeMillis() - timeIni > durationIntro + durationLlegada + duracionFade + duracionCreacion + duracionScale)
		{
			estado = ESTADO_COLOCACION;
			int pos = secuenciaPosiciones[siguientePosicion];
			bricks_local.back().addAnimation(posicionesGrid[pos].pos, Brick::ANIMACION_BRICK::DUMP_BRIDGE, 0);
			bricks_local.back().startAnimation();
		}
	}

}


void scene_05_pillar_dump::brickArrival(int idPillar, int tipo, int pos)
{
	cout << "brick_arrival: " << idPillar << "," << tipo << "," << pos << endl;
	int posGrid = 9 * idPillar + pos;
	ofVec2f initPos;
	initPos.x = posGrid * 56;
	initPos.y = ANCHO_BRICK / 2.0f;
	ofVec2f dest;
	if (tipo == 9)
		dest = getNextBrickAzul();
	else
		dest = getNextBrickVarios();


	Brick *brick = new Brick;
	brick->setup(initPos, tipo, bricks_local.size(), 0, 0, ofGetElapsedTimeMillis());
	bricks_local.push_back(*brick);
	bricks_local.back().addAnimation(dest, Brick::ANIMACION_BRICK::DUMP_BRIDGE, 0);
	bricks_local.back().startAnimation();
}


void scene_05_pillar_dump::draw() {
	ofBackground(0);
	
	if (estado == ESTADO_INTRO)
		player.draw(0, 0, BRIDGE_WIDTH, BRIDGE_HEIGHT);

	if (estado != ESTADO_INTRO)
		drawDump();

	ofSetColor(255);
	
	ofDrawBitmapString(percentage, 20, 50);

	ofDrawBitmapString(ofToString(ofGetFrameRate(), 0) + " FPS", 20, 20);

}


void scene_05_pillar_dump::drawDump() {
	
	//primero los que no son azules
	for (int i = 0; i < bricks_local.size(); i++)
		if (bricks_local[i].getTipo() != 9)
			bricks_local[i].draw();

	//encima los que son azules
	for (int i = 0; i < bricks_local.size(); i++)
		if (bricks_local[i].getTipo() == 9)
			bricks_local[i].draw();


}

ofVec2f scene_05_pillar_dump::getNextBrickVarios()
{

	int posIni = brickGrandeCentroPos - ((84 * 2) + 2);
	int linea = floor((float)variosPosActual / (float)BRICK_GRANDE_DIM);
	int columna = variosPosActual - (linea * BRICK_GRANDE_DIM);
	int posReal = posIni + (linea * 84) + columna;

	//cout << "posicion destino brick = " << posReal << "(posIni = " << posIni << ", linea =" << linea << ", columna = " << columna << endl;

	variosPosActual++;
	if (variosPosActual == BRICK_GRANDE_DIM * BRICK_GRANDE_DIM)
		variosPosActual = 0;

	return posicionesGrid[posReal].pos;
}

ofVec2f scene_05_pillar_dump::getNextBrickAzul()
{

	int posIni = brickGrandeCentroPos - ((84 * 2) + 2);
	int linea = floor((float)azulPosActual / (float)BRICK_GRANDE_DIM);
	int columna = azulPosActual - (linea * BRICK_GRANDE_DIM);
	int posReal = posIni + (linea * 84) + columna;

	//cout << "posicion destino brick = " << posReal << "(posIni = " << posIni << ", linea =" << linea << ", columna = " << columna << endl;

	azulPosActual++;
	if (azulPosActual == BRICK_GRANDE_DIM * BRICK_GRANDE_DIM)
		azulPosActual = 0;

	return posicionesGrid[posReal].pos;

}