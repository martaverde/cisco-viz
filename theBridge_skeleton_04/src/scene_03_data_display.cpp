


#include "scene_03_data_display.h"
#include "oscManager.h"
#include "Constantes.h"
//#include "ofxEasing.h"

void scene_03_data_display::setup() {

	dataOscManager = oscManager::getInstance();

	type = 6;
	setColors();
	//mode = 0; //featured
	family = 0; //secondary

	//mode = dataOscManager->getFamily();
    //path = dataOscManager->getPath();
	
	highlights = "3,5//6,8";

	videos.clear();

	textoCifra.allocate(BRIDGE_WIDTH, BRIDGE_HEIGHT, GL_RGBA);
	textoParrafo.allocate(BRIDGE_WIDTH, BRIDGE_HEIGHT, GL_RGBA);
	brickTime.allocate(BRIDGE_WIDTH, BRIDGE_HEIGHT, GL_RGBA);
	//getFamily
	if(family == 0) dir.listDir("/videos/data_display/behavior/featured/");
	if(family == 1) dir.listDir("/videos/data_display/behavior/secondary/");
	
	//get state type 

	dir.sort();

	if (dir.size() > 0) {
		videos.assign(dir.size(), ofxHapPlayer());
		for (int i = 0; i < (int)dir.size(); i++) {
			videos[i].load(dir.getPath(i));
			videos[i].play();
			videos[i].setLoopState(OF_LOOP_NONE);
			cout << "video cargado: " << dir.getPath(i) << endl;
		}
	}

	
	number = "9000";
	what = "participants";
	parrafo = "In the past 2 hours over 9000 participants have learned that Security is Foundational.";


	 spacingHighlight = 30;
	 numberFeaturedSize = 330;
	 whatFeaturedSize = 331;
	 copyFeaturedandSecondarySize = 130;


	//typeSize = 330;
	ciscoSans.load("fonts/CiscoSansTTBold.ttf", numberFeaturedSize, true, true);
	ciscoSans.setLineHeight(34.0f);
	ciscoSans.setLetterSpacing(1.035);
	ciscoSansLight.load("fonts/CiscoSansTTLight.ttf", whatFeaturedSize, true, true);
	ciscoSansLight.setLineHeight(34.0f);
	ciscoSansLight.setLetterSpacing(1.035);
	
	

	//vainas parrafo
	for (int i = 0; i<1; i++) {
		ofxParagraph* p = new ofxParagraph();
		//p->setColor(ofColor(255), frontColor);
	//	p->drawBorder(ofColor::fromHex(0x777777));
	//	p->drawWordBoundaries();
		paragraphs.push_back(p);
	}

	//int pWidth = ofGetWidth() - 1200;
	int pWidth = BRIDGE_WIDTH;
	int pLeading = copyFeaturedandSecondarySize*.65;
	paragraphs[0]->setFont(ofxSmartFont::add("fonts/CiscoSansTTBold.ttf", copyFeaturedandSecondarySize, "cisco-bold"));

	for (int i = 0; i<paragraphs.size(); i++) {
		paragraphs[0]->setWidth(pWidth);
		//paragraphs[0]->setSpacing(copyFeaturedandSecondarySize*.7);
		paragraphs[0]->setSpacing(155/2);
		paragraphs[0]->setLeading(pLeading);
		paragraphs[0]->setColor(ofColor(255), frontColor);
	//	paragraphs[0]->setHighLight(6, 7);
		paragraphs[0]->setText(parrafo);
		paragraphs[0]->setPosition(BRIDGE_WIDTH / 2 - paragraphs[0]->getWidth() / 2, BRIDGE_HEIGHT / 3 - copyFeaturedandSecondarySize);
		paragraphs[0]->setAlignment(ofxParagraph::ALIGN_CENTER);
	}



	for (int i = ancho / 2; i < BRIDGE_WIDTH; i = i + ancho) {
		for (int j = -ancho / 2; j < BRIDGE_HEIGHT; j = j + ancho) {
			ofSetColor(255);
			ofDrawEllipse(i, j, 5, 5);
			ofVec2f tempPoint = ofVec2f(i, j);
			gridPositions.push_back(tempPoint);
			ofDrawBitmapString(gridPositions.size(), i, j);
		}
	}

	posYTextoCifra = BRIDGE_HEIGHT / 2;
	posYFinalTextoCifra = BRIDGE_HEIGHT / 4;
	posYfinalTextoCifra2 = -BRIDGE_HEIGHT / 2 - numberFeaturedSize;
	catchUpSpeed = 0.08f;

	//posici�n fija de  n�mero de claim principal. El digito aumenta y desplaza la caja de texto
	rect = ciscoSans.getStringBoundingBox(number, 0, 0);
	rect2 = ciscoSansLight.getStringBoundingBox(what, 0, 0);
   // rectangleComplete = rect.width + rect2.width + numberFeaturedSize / 2;
	rectangleComplete = rect.width + rect2.width + spacingHighlight;

	//==== motion array load 
	buffer = ofBufferFromFile("motionArray.txt");
	positions = ofToString(buffer);
	result = ofSplitString(positions, ",");
	
	initTime =  ofGetElapsedTimeMillis() + 7500;
	easedPos = BRIDGE_HEIGHT / 1.5;

	cout << "data display setup end" << endl;

}

void scene_03_data_display::update(){

	//std::cout << ofGetElapsedTimeMillis() - initTime << endl;

	number = ofToString(int(ofRandom(500, 999)));

	for (int i = 0; i < videos.size(); i++) {
		videos[i].update();	
	}
	if (videos.size() > 0)
	{
		percentage = videos[0].getPosition();
		duration = videos[0].getDuration();
		framesTotal = duration * ofGetFrameRate();
		frameCount = ofMap(percentage, 0, 1, 0, framesTotal);
	}

	//if (videos[0].getIsMovieDone()) //data->sendStateIsFinished();
	
	
	//texto sube 
	/*
	if (frameCount >= 8 * ofGetFrameRate()) {
		posYTextoCifra = catchUpSpeed * posYFinalTextoCifra + (1 - catchUpSpeed) * posYTextoCifra;
	}

	if (frameCount >= 11 * ofGetFrameRate()) {
		posYTextoCifra = catchUpSpeed*0.7 * posYfinalTextoCifra2 + (1 - catchUpSpeed*0.7) * posYTextoCifra;
	}
	*/
//	if (frameCount >= 8 * ofGetFrameRate()) {
		//initTime = ofGetElapsedTimeMillis();
	
	
	if (ofGetElapsedTimeMillis() - initTime > 0 && ofGetElapsedTimeMillis() - initTime < 3000 ) {
		
		newPos = ofMap(ofGetElapsedTimeMillis() - initTime, 0, 3000, 0, 180, true);
		easedPos = ofToFloat(result[int(newPos)]);
		easedPos = ofMap(easedPos, 0, 1, BRIDGE_HEIGHT /1.5, -300);
		//posYTextoCifra = easedPos;

		//std::cout << "HOLI" << endl;

	}
	std::cout << easedPos << endl;

		


	textoCifra.begin();
		ofClear(0,0);
		ofPushMatrix();
		ofTranslate(videos[0].getWidth() / 2 - rectangleComplete / 2, easedPos);
			ofSetColor(255);
			ciscoSans.drawString(number, 0, 120);
			ofSetColor(backColor);
		//	ciscoSansLight.drawString(what, rect.width + numberFeaturedSize/2, 120);
			ciscoSansLight.drawString(what, rect.width + spacingHighlight, 120);
		ofPopMatrix();
	textoCifra.end();
	cout << "data 2" << endl;

	textoParrafo.begin();
		ofClear(0, 0);
		paragraphs[0]->draw();
		//highlights =  ->getHighlight();
		// split string from highlights
		


		vector <string> partes = ofSplitString(highlights, "//", true, true);
		
		for (int i = 0; i < partes.size(); i++) {
			vector <string> index = ofSplitString(partes[i], ",", true, true);
			for (int j = 0; j < index.size(); j++) {
				//std::cout << index[j] << endl;
				paragraphs[0]->drawHighLight(ofToInt(index[0]), ofToInt(index[1]));
			}
		}
		

	textoParrafo.end();

	cout << "data 3" << endl;

	//pintar brick inicio en ofGetWidth()/2, ofGetHeight()/4, escalado + 3
	//brickTime.begin();
	//	ofClear(0, 0);
	//	
	//	ofSetColor(255);
	//	newBrick(type);

	//	for (int i = 0; i < groupOfBricks.size(); i++) {
	//			groupOfBricks[i].draw();
	//	}

	//brickTime.end();

	cout << "data 4" << endl;

}

void scene_03_data_display::draw() {
	ofBackground(0);
	cout << "draw1" << endl;
	if (family == 0) drawFeatured();
	if (family == 1) drawSecondary();
	/*
	ofSetColor(frontColor);
	ofDrawRectangle(ofGetWidth() / 2, ofGetHeight() / 2, 100, 100);

	ofSetColor(backColor);
	ofDrawRectangle(ofGetWidth() / 2 + 100, ofGetHeight() / 2, 100, 100);
	*/
	//entre 5 y 8 segundos inicia colocacion brick
	if (percentage > 0.98) {
		brickTime.draw(0, 0);
	}

	//drawGrid();
	
	// Draw the FPS display
	ofSetColor(255);
	ofDrawBitmapString(ofToString(ofGetFrameRate(), 0) + " FPS", 20, 20);
	ofDrawBitmapString(percentage, 20, 400);
}




void scene_03_data_display::drawFeatured() {
	if (dir.size() > 0) {
		for (int i = 0; i < (int)dir.size(); i++) {
			videos[0].draw(0, 0);
			if (frameCount > 8 * ofGetFrameRate() && frameCount < 13 * ofGetFrameRate()) {
				textoCifra.draw(0, 0);
			}
			
			videos[1].draw(0, 0);
			if (frameCount > 15 * ofGetFrameRate() && frameCount < 26 * ofGetFrameRate()) {
				textoParrafo.draw(0, 0);
			}
			videos[2].draw(0, 0);
		}

		/*
		//para featured 8 - 13 seg
		if (frameCount > 8 * ofGetFrameRate() && frameCount < 13 * ofGetFrameRate()) {
			ofSetColor(255);
			ofDrawBitmapString(number, ofGetWidth() / 2, ofGetHeight() / 2);
		}

		//para featured 8 - 13 seg
		if (frameCount > 15 * ofGetFrameRate() && frameCount < 25 * ofGetFrameRate()) {
			ofSetColor(255);
			ofDrawBitmapString(parrafo, ofGetWidth() / 2, ofGetHeight() / 2);
		}
		*/
	}
}

void scene_03_data_display::drawSecondary() {
	if (dir.size() > 0) {
		for (int i = 0; i < (int)dir.size(); i++) {
			videos[0].draw(0, 0);
			if (frameCount > 5 * ofGetFrameRate() && frameCount < 15 * ofGetFrameRate()) {
				textoParrafo.draw(0, 0);
			}
			//Texto parrafo HERE
			videos[1].draw(0, 0);
		}
		/*
		//para secondary 5 - 15 seg
		if (frameCount > 5 * ofGetFrameRate() && frameCount < 15 * ofGetFrameRate()) {
			ofSetColor(255);
			ofDrawBitmapString(parrafo, ofGetWidth() / 2, ofGetHeight() / 2);
		}
		*/
	}
}


ofColor scene_03_data_display::setColors() {
	green = ofColor(107, 189, 82);
	red = ofColor(221, 42, 39);
	yellow = ofColor(242, 166, 40);
	blue = ofColor(27, 168, 223);

	darkGreen = ofColor(34, 92, 58);
	darkRed = ofColor(150, 25, 33);
	darkBlue = ofColor(22, 73, 108);
	darkYellow = ofColor(221, 42, 39);


	//security
	//data
	//clouds


	if (type == 0 || type == 1 || type == 2) {
		backColor = yellow;
		frontColor = darkYellow;
	}

	//connections
	//collaboration
	//conversations

	if (type == 3 || type == 4 || type == 5) {
		backColor = red;
		frontColor = darkRed;
	}
	//identity
	//behaviour
	//learnings

	if (type == 6 || type == 7 || type == 8) {
		backColor = green;
		frontColor = darkGreen;
	}

	if (type == 9) {
		backColor = blue;
		frontColor = darkBlue;
	}


	return backColor;
	return frontColor;
}


void scene_03_data_display::newBrick(int tipoBrick) {
	Brick tempBrick;
	int in = 3;
	int out = 1;

//indexPosition = buscaPosicionNuevoBrick();
//ofVec2f initPos = posicionesGrid[indexPosition].pos;
	//  std::cout << indexPosition << initPos << endl;
	//BUSCAR POSICI�N INICIAL CUADRADA CON EL V�DEO
//	ofVec2f initPos = gridPositions[50];
	tempBrick.setup(ofVec2f(BRIDGE_WIDTH/2, BRIDGE_HEIGHT/4), tipoBrick, groupOfBricks.size(), in, out, ofGetElapsedTimeMillis());
	groupOfBricks.push_back(tempBrick);

	// std::cout << "Type of new brick: " << tempTypeBrick << endl;
	// std::cout << "Number of bricks: " << groupOfBricks.size() << endl;

//	brickCounter++;
	//indexPosition = filaSwap[counter];
	//shuffle (filaSwap.begin(), filaSwap.end(), std::default_random_engine(seed));
//	timestamp = ofGetTimestampString();


//string brickIsNew = ofToString(groupOfBricks.size()) + "," + ofToString(tipoBrick) + "," + ofToString(indexPosition);
//	std::cout << brickIsNew << endl;
	//data->sendNewBrick(brickIsNew);

}

void scene_03_data_display::drawGrid() {
	for (int i = 0; i < BRIDGE_WIDTH; i = i + 56) {
		ofSetColor(255);
		ofDrawLine(i, 0, i, BRIDGE_HEIGHT);
	}
	for (int j = 0; j < BRIDGE_HEIGHT; j = j + 56) {
		ofDrawLine(0, j, BRIDGE_WIDTH, j);
	}


}