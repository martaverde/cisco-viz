#include "scene_14_shut_down.h"
#include "Brick.h"
#include "Constantes.h"
//bricks con el alpha al 50% 
//duracion 1800 seg 


void scene_14_shut_down::setup() {
	dataOscManager = oscManager::getInstance();
	sceneDuration = 1800000;

	bricks = dataOscManager->getBricks();
	//recibir array de bricks colocados hasta el momento 
}

void scene_14_shut_down::update() {
	
	dataOscManager->update();

	percentage = ofMap(ofGetElapsedTimeMillis(), 0, sceneDuration, 0, 1);
	dataOscManager->sendStatePercentage(percentage);

	if (ofGetElapsedTimeMillis() > sceneDuration) {
		dataOscManager->sendStateIsFinished();
	}
}

void scene_14_shut_down::draw() {

	ofBackground(0);

	

	ofSetColor(255);

	ofDrawBitmapString(percentage, 20, 200);
	ofDrawBitmapString("SHUT_DOWN", 20, 300);
	// Draw the FPS display
	ofSetColor(255);
	ofDrawBitmapString(ofToString(ofGetFrameRate(), 0) + " FPS", 20, 20);

}

