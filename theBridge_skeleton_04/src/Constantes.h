#pragma once

#ifndef __Constantes__
#define __Constantes__

#define BRIDGE_WIDTH				4704
#define BRIDGE_HEIGHT				1344
#define BRIDGE_NUM_POSICIONES_GRID  1332
#define ANCHO_BRICK					56.0f

struct GRID_POSICIONES
{
	ofVec2f pos;
	int		tipoBrick; // -1 si no tiene ning�n brick
};

#endif