
#ifndef __example_Simple__ZeroScene__
#define __example_Simple__ZeroScenee__

#include "ofxScene.h"
#include "ofxHapPlayer.h"
#include "oscManager.h"

class scene_12_final : public ofxScene {
public:
	void setup();
	void update();
	void draw();

	oscManager *dataOscManager;
	float timer;
	float percentage;
	float sceneDuration;
	ofxHapPlayer player;

};

#endif /* defined(__example_Simple__FirstScene__) */