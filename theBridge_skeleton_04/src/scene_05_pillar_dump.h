
#ifndef __scene_05_pillar_dump__
#define __scene_05_pillar_dump__

#include "ofxScene.h"
#include "ofxHapPlayer.h"
#include "oscManager.h"
#include "ofxOsc.h"
#include "Brick.h"
#include "Constantes.h"

#define PORT 33333
#define NUM_MSG_STRINGS 20
#define BRICK_GRANDE_DIM 5
#define VIDEO_PATH "videos/discharge/Discharge.mov"
#define DURACION_INTRO 10 * 1000
#define DURACION_LLEGADA 20 * 1000
#define DURACION_COLOCACION 5 * 1000

class scene_05_pillar_dump : public ofxScene {
public:
    void setup			( );
    void update			( );
    void draw			( );
	void inicializa		( vector<Brick> *bricks, GRID_POSICIONES *pos, int *secuenciaPosiciones);
	void setSiguiente	( int i );
	void drawDump		( );


private:

	void			brickArrival		( int idPillar, int tipo, int pos );
	ofVec2f			getNextBrickVarios	( );
	ofVec2f			getNextBrickAzul	( );
	void			updateStatus		( );

	vector<Brick>   *bricks_global;
	GRID_POSICIONES	*posicionesGrid;
	int				*secuenciaPosiciones;

	enum			ESTADO { ESTADO_INTRO, ESTADO_LLEGADA, ESTADO_AGRUPAR, ESTADO_CREAR, ESTADO_REDUCIR, ESTADO_COLOCACION};
	ESTADO			estado;

	vector<Brick>   bricks_local;
	double			timeIni;
	ofxOscReceiver	receiver;
	ofTrueTypeFont	font;
	oscManager		*dataOscManager;
	ofxHapPlayer	player;
	float			percentage;
	float			sceneDuration;
	
	int				durationIntro = DURACION_INTRO; //intro video
	int				durationLlegada = DURACION_LLEGADA; //llegada
	int				durationColocacion = DURACION_COLOCACION; //colocacion

	//string copy;

	bool			bFirstTime = true;
	//bool			brickGrandeVarios[BRICK_GRANDE_DIM * BRICK_GRANDE_DIM];
	int				variosPosActual;
	//bool			brickGrandeAzules[BRICK_GRANDE_DIM * BRICK_GRANDE_DIM];
	int				azulPosActual;
	int 			brickGrandeCentroPos;
	int				siguientePosicion;
	//ofFbo			fboBricksAzules;

};

#endif /* defined(__example_Simple__FirstScene__) */
