
#ifndef __scene_08_recap__
#define __scene_08_recap__

#include "ofxScene.h"
#include "ofxHapPlayer.h"
#include "recapBlock.h"
#include "oscManager.h"
#include "Constantes.h"

class scene_08_recap : public ofxScene {
	
public:
		void setup();
		void update();
		void draw();
		void parseaRecap(string churro);
		void addRecap(int tipo, string texto);
	
	private:
		double timeIni;
		float percentage;
		string copy;
		ofImage plantilla;

		ofTrueTypeFont ciscoSansBold;
		string dataString;

		string testString;

		float timeNow;

		int tempTypeBlock;

		struct LINEA {
			int posY;
			int posXInicial;				//donde va el primer recap
			int posXSiguiente;				//donde colocar el siguiente recap
			float posXAnimacion;			//donde esta la x inicial durante la animación
			float speed;
			vector <recapBlock> recapBlocks;
		};

		vector <LINEA> lineas;
		int ancho;
		int lineaRecapActual = 0;

		string recap;

		vector <string> stringGroup;

	    int recapType;
		float counter;
		float sceneDuration;

	
	    ofxHapPlayer video;
		string actualVideo;

		float timer;
		float timerActual;
		int DURACION_ESCENA;
		oscManager *dataOscManager;


};

#endif /* defined(__example_Simple__FirstScene__) */
