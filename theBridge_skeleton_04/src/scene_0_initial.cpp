
#include "scene_0_initial.h"
#include "oscManager.h"
#include "Constantes.h"
#include "ofMain.h"

void scene_0_initial::setup() {
	dataOscManager = oscManager::getInstance();
	timer = 0;
	//miliseconds
	sceneDuration = 10000;
	fondo.load("mapping_bridge.png");
}

void scene_0_initial::update() {
	dataOscManager->update();
	timer = timer + ofGetElapsedTimeMillis();

	percentage = ofMap(ofGetElapsedTimeMillis(), 0, sceneDuration, 0, 1);
	percentage = ofClamp(percentage, 0, sceneDuration);
	
	dataOscManager->sendStatePercentage(percentage);

	if (timer > sceneDuration) {
		dataOscManager->sendStateIsFinished();
	}

	//mondongo = "/videos/advertise/cisco_0.mov";
}

void scene_0_initial::draw() {
	ofBackground(0);

	ofSetColor(255);
	fondo.draw(0, 0, BRIDGE_WIDTH, BRIDGE_HEIGHT);

	ofDrawBitmapString("STANDBY SCREEN", 20, 20);

	ofDrawBitmapString(percentage, 20, 40);
}

