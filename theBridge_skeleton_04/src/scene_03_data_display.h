

#ifndef __scene_03_data_display__
#define __scene_03_data_display__

#include "ofxScene.h"
#include "ofxHapPlayer.h"
#include "ofxParagraph.h"
#include "oscManager.h"
#include "Brick.h"

class scene_03_data_display : public ofxScene {
public:
    void setup();
    void update();
    void draw();

	oscManager *dataOscManager;

	vector <ofxHapPlayer> videos;
	int currentVideo;
	ofDirectory dir;
	string path;

	float percentage;
	int mode;
	float duration;
	float framesTotal;
	float frameCount;

	void drawFeatured();
	void drawSecondary();

	//color stuff
	ofColor backColor;
	ofColor frontColor;
	
	ofColor green;
	ofColor red;
	ofColor yellow;
	ofColor blue;
	
	ofColor darkGreen;
	ofColor darkRed;
	ofColor darkBlue;
	ofColor darkYellow;

	int type; //tipo de claim/video/bkla

	ofColor setColors();

	int family; //si es featured o secondary
	//text stuff
	string number;
	string what;
	string parrafo;
	int pFontSize;
	ofTrueTypeFont ciscoSans, ciscoSansLight, ciscoSans2;
	int typeSize;

	vector<ofxParagraph*> paragraphs;
	ofxParagraph paragraph;

	ofFbo textoCifra;
	ofFbo textoParrafo;
	ofRectangle rect;
	ofRectangle rect2;
	int rectangleComplete;

	float posYTextoCifra;
	float posYFinalTextoCifra;
	float catchUpSpeed;
	float posYfinalTextoCifra2;
	string highlights;
	ofBuffer buffer;
	vector <string> result;
	string positions;
	float initTime;
	float newPos;
	float easedPos;


	int numberFeaturedSize;
	int whatFeaturedSize;
	int copyFeaturedandSecondarySize;
	int spacingHighlight;
	

	//bricks

	ofFbo brickTime;
	void newBrick(int tipoBrick);

	vector <Brick> groupOfBricks;

	int ancho = 56;
	vector <ofVec2f> gridPositions;
	int indexPosition;
	void drawGrid();






};

#endif 
