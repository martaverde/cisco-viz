#include "ofMain.h"
#include "ofApp.h"
#include "Constantes.h"

//========================================================================
int main( ){

	ofGLFWWindowSettings settings;

	//ventana de 1920*3  x 1080 
	settings.width = 1920*3;
	settings.height = BRIDGE_HEIGHT;

//	settings.width = BRIDGE_WIDTH;
//	settings.height = BRIDGE_HEIGHT;

	settings.decorated = false;
	settings.windowMode = OF_WINDOW;
	
	ofCreateWindow(settings);
	ofRunApp(new ofApp());

}
