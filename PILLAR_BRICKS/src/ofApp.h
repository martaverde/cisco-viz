#pragma once
#include "ofMain.h"
#include "Brick.h"
#include "ofxXmlSettings.h"
#include "ofxKinectV2OSC.h"
#include "ofxOsc.h"
#include "ofxNDI.h"
#include "ofxHapPlayer.h"
#include "KinectPillar.h"
#include "ofxGui.h"
#include "Temporizador.h" 

//#define NUM_FILAS				12
//#define NUM_COLUMNAS			9
//#define NUM_POSICIONES_GRID		NUM_FILAS * NUM_COLUMNAS
#define INTERVALO_PING			1000
#define PANTALLA_LED_WIDTH		504
#define PANTALLA_LED_HEIGHT		672
#define POSICIONES_PERMANENTES	paramDumpNumBricks
#define DUMP_FASE1_DELAY		10
#define DUMP_FASE2_SUBIR		5
#define DUMP_FASE3_REORDENAR	3
#define NDI_WIDTH				252
#define NDI_HEIGHT				336


class ofApp : public ofBaseApp {

public:
	void setup();
	void update();
	void draw();

	void keyPressed		(int key );
	void keyReleased	(int key );
	void mouseMoved		(int x, int y );
	void mouseDragged	(int x, int y, int button );
	void mousePressed	(int x, int y, int button );
	void mouseReleased	(int x, int y, int button );
	void mouseEntered	(int x, int y );
	void mouseExited	(int x, int y );
	void windowResized	(int w, int h );
	void dragEvent		(ofDragInfo dragInfo );
	void gotMessage		(ofMessage msg );

private:

	enum TIPO_AUDIO			{ BRICK_START, BRICK_STOP };
	enum TIPO_POSICIONES	{ FRONT, BACK };

	void updateDump						( );
	void updateNDI						( );
	void sendInit						( );
	void inicializa						( );
	void setupOSC						( );
	void setupGui						( );
	void updateOSC						( );
	void drawGrid						( );
	void newBrick						( int tipo );
	int  buscaPosicionNuevoBrickArriba	( );
	int  buscaPosicionNuevoBrickAbajo	(TIPO_POSICIONES tipo);
	void setupKinectSkeleton			( );
	void updateKinectSkeleton			( );
	void drawKinectSkeleton				( );
	void resetGrid						( );
	void drawStandBy					( );
	void drawVideo						( );
	void drawInteractivo				( );
	void drawDump						( );
	void updateBricks					( string churro );
	void sendUpdateBricks				( );
	bool soyYo							( ofxOscMessage &msg);
	void startInteraction				( );
	void endInteraction					( );
	void logOSCMessage					( ofxOscMessage &msg );
	void startPivotVideo				( string path );
	void startDump						( );
	//void startDumpTimer					( );
	void sendBrickArrival				( int tipoBrick, int posicion );
	void updateVideo					( );
	void procesaNDI						( );
	void sendAudio						( TIPO_AUDIO tipo );
	void brickEventStart				( int &e );
	void brickEventStop					( int &e );
	void posicionaVentana				(	);
	void loadFondo						( );

	ofxXmlSettings	xml;

	ofxOscSender	oscSenderSM;
	ofxOscSender	oscSenderCM;
	ofxOscSender	oscSenderBridge1;
	ofxOscSender	oscSenderBridge2;
	ofxOscSender	oscSenderAudio;

	ofxOscReceiver  oscReceiver;

	ofxPanel		gui;
	ofxToggle		paramIgnorePivotal;
	ofxIntSlider	paramDumpDelay;
	ofxIntSlider	paramKinectAlpha;
	ofxIntSlider	paramDumpNumBricks;
	ofxIntSlider	paramNumBricksProvocaDump;
	ofxIntSlider	paramFondoMin;
	ofxIntSlider	paramFondoMax;
	ofxIntSlider	paramFondoFila;

	bool			bGuiVisible = false;

	int				indexPosition;
	//ofVec2f			initPos;
	vector <Brick>	groupOfBricksFront;
	vector <Brick>	groupOfBricksBack;
	vector <Brick>	bricksInteraccionFront;
	vector <Brick>	bricksInteraccionBack;

	int				ancho;


	GRID_POSICIONES posicionesGridFront[NUM_FILAS*NUM_COLUMNAS];
	GRID_POSICIONES posicionesGridBack[NUM_FILAS*NUM_COLUMNAS];
	int				brickCounter = 0;
	int				brickCounterArriba = 0;

	int				posicionesArriba[9]; //un random donde coloar los bricks arriba

	ofFbo pillarFront;
	ofFbo pillarBack;

	// KINECT STUFF

	ofxKinectV2OSC kinect;
	Skeleton* skeleton;
	vector<Skeleton>* skeletons;
	BodyRenderer renderer;
	ofTrueTypeFont smallFont, largeFont;

	enum ESTADOS_GLOBAL { ESTADO_OFFLINE, ESTADO_STANDBY, ESTADO_DUMP};
	ESTADOS_GLOBAL estadoGlobal = ESTADO_OFFLINE;

	enum ESTADOS_INTERACTIVO { INTERACTIVO_OFF, INTERACTIVO_INICIO, INTERACTIVO_BAJANDO };
	ESTADOS_INTERACTIVO estadoInteractivo;

	enum ESTADOS_DUMP { DUMP_INICIO, DUMP_REORDENANDO};
	ESTADOS_DUMP estadoDump;



	double lastPing = 0;

	int IdMachine = 0;			//en que m�quina se est� ejecutando
	int IdInstance = 0;			//que instancia de pillar soy

	//NDI
	int senderWidth;
	int senderHeight;
	ofxNDIreceiver ndiReceiverFront; // NDI receiver
	ofxNDIreceiver ndiReceiverBack;  // NDI receiver
	ofImage ndiImageFront;			 // Image to receive
	ofImage ndiImageBack;			 // Image to receive
	ofPixels ndiPixels;
	unsigned char *ndiChars;
	ofFbo	fboNDIback;
	ofFbo	fboNDIfront;

	// VIDEO STUFF
	ofxHapPlayer videoPlayer;
	bool ndiNeedsUpdate = true;
	double lastNDIupdate = 0;
	bool bNDIFrontready = false;
	bool bNDIBackready = false;

	bool bPosicionDerecha = true;
	bool bVideoActivo = false;
	
	struct SECUENCIA {
		int posiciones[108];
		int posicionActual;
	};
	vector<SECUENCIA> secuenciasPosicionesFront;
	vector<SECUENCIA> secuenciasPosicionesBack;
	vector<SECUENCIA> secuenciasPosicionesBackHandicap;
	int secuenciaPosicionesFrontActiva = 0;
	int secuenciaPosicionesBackActiva = 0;
	int secuenciaPosicionesBackHandicapActiva = 0;

	int posicionesPemanentesFront[100]; //maaaal
	int posicionesPemanentesBack[100]; //maaaal

	double timeDumpStart;
	//double timeDumpTimerStart;
	KinectPillar kinectPillarFront;
	KinectPillar kinectPillarBack;

	double	lastWindowRelocation;

	Temporizador tempInteractivoEnded;
	ofImage		imgFondo;
	ofFbo		 fboFondo;

	int			lastBrickForDumpFront;
	int			lastBrickForDumpBack;

};

