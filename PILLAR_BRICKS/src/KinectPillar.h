#pragma once

#include "ofxGui.h"
#include "Brick.h"

#define NUM_FILAS				12
#define NUM_COLUMNAS			9
#define NUM_POSICIONES_GRID		NUM_FILAS * NUM_COLUMNAS

class KinectPillar
{
	
	public:
		void setup			( int width, int height, vector<Brick> *bricks, GRID_POSICIONES *posiconesGrid );
		void update			( ofImage &_img );
		void draw			( float x, float y);
		void save			( );
		void setGuiVisible	(bool visible );

		ofxIntSlider	paramBinPuntosThreshold;
		ofxIntSlider	paramBinPuntosMin;
		ofxIntSlider	paramBinPuntosMax;
		ofxIntSlider	paramBinAlphaMax;
		ofxIntSlider	paramBinAlphaMin;
		ofxIntSlider	paramBricksAlphaKinect;
		ofxFloatSlider  paramBricksAlphaDamp;
		ofxIntSlider    paramThersholdGente;
		ofxFloatSlider  paramAlphaTransicionGente;

	private:
		
		ofFbo			fboKinect;
		float			width;
		float			height;
		vector<Brick>	*bricks;
		ofImage			imgKinect;
		ofxPanel		gui;


		struct BIN {
			float		alpha;
			ofRectangle r;

		};
		
		BIN			bins[NUM_FILAS][NUM_COLUMNAS];
		BIN			bins3[NUM_FILAS*3][NUM_COLUMNAS*3];

		GRID_POSICIONES *posicionesGrid;

		bool bGuiVisible = false;
		bool bHayAlguien = false;

};