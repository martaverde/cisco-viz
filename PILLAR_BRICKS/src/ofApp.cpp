
#include "ofApp.h"
#include <algorithm>
#include <numeric>
#include <iostream>
#include <array>
#include <random>
#include <chrono>
#include "oscManager.h"

 // To-do
 /*
 Filas/columnas diferenciar
 lógica de colocación con montañitas
 eliminar posiciones que no salen en brick
 */


//--------------------------------------------------------------
void ofApp::setup(){
	
    ofBackground(0);
    ofEnableSmoothing();
    //ofEnableAntiAliasing();
	ofSetCircleResolution(100);
	//ofDisableAntiAliasing();

	xml.loadFile("mySettings.xml");

    ancho = 56;
	ofSetWindowPosition(1920 + 1280, 0);
	
	//ofSetWindowPosition(1920, 0);
	ofSeedRandom(1000);

	pillarFront.allocate(PANTALLA_LED_WIDTH, PANTALLA_LED_HEIGHT, GL_RGBA);
	pillarBack.allocate(PANTALLA_LED_WIDTH, PANTALLA_LED_HEIGHT, GL_RGBA);
	
	ndiPixels.allocate(NDI_WIDTH, NDI_HEIGHT, OF_IMAGE_COLOR_ALPHA);
	ndiImageBack.allocate(NDI_WIDTH, NDI_HEIGHT, OF_IMAGE_COLOR_ALPHA);
	ndiImageFront.allocate(NDI_WIDTH, NDI_HEIGHT, OF_IMAGE_COLOR_ALPHA);
	fboNDIback.allocate(NDI_WIDTH, NDI_HEIGHT, GL_RGBA);
	fboNDIfront.allocate(NDI_WIDTH, NDI_HEIGHT, GL_RGBA);
	//ndiChars = new unsigned char[NDI_WIDTH * NDI_HEIGHT * 4];
	//senderWidth = NDI_WIDTH;
	//senderHeight = NDI_HEIGHT;

	for (int fila = 0; fila < NUM_FILAS ; fila++) {
		for (int col = 0; col < NUM_COLUMNAS; col++) {
			ofVec2f tempPoint = ofVec2f((ancho/2) + ancho * col, (ancho/2) + ancho*fila);
			posicionesGridFront[fila*NUM_COLUMNAS+col].pos = tempPoint;
			posicionesGridBack[fila*NUM_COLUMNAS + col].pos = tempPoint;
		}
	}
	
	resetGrid();

	setupOSC();
	
	setupGui();

	estadoGlobal = ESTADOS_GLOBAL::ESTADO_STANDBY;
	
	cout << NDIlib_version() << " (http:\\NDI.NewTek.com)" << endl;

	ndiNeedsUpdate = true;

	sendInit();
	
	if (IdMachine <= 6)
		IdInstance = IdMachine;
	else
		IdInstance = 0;

	//ojo
	//IdInstance = 1;

	cout << "Soy la maquina: " << IdMachine << ", Instancia: " << IdInstance << endl;

	secuenciaPosicionesFrontActiva = ofRandom(0, secuenciasPosicionesFront.size()-1);
	secuenciaPosicionesBackActiva = ofRandom(0, secuenciasPosicionesBack.size()-1);

	for (int i = 0; i <50; i++)
	{
		int pos = buscaPosicionNuevoBrickAbajo(TIPO_POSICIONES::FRONT);
		ofVec2f initPos = posicionesGridFront[pos].pos;
		int tipo = ofRandom(10);
		if (tipo == 10)
			tipo = 9;
		posicionesGridFront[pos].tipoBrick = tipo;

		Brick *brick = new Brick;
		brick->setup(initPos, tipo, groupOfBricksFront.size(), 0, 0, ofGetElapsedTimeMillis());
		groupOfBricksFront.push_back(*brick);
		groupOfBricksFront.back().addAnimation(initPos, Brick::ANIMACION_BRICK::CREACION, 0);
		groupOfBricksFront.back().startAnimation();
		groupOfBricksFront.back().startInside();

		pos = buscaPosicionNuevoBrickAbajo(TIPO_POSICIONES::BACK);
		initPos = posicionesGridBack[pos].pos;
		posicionesGridBack[pos].tipoBrick = tipo;

		Brick *brick2 = new Brick;
		brick2->setup(initPos, tipo, groupOfBricksBack.size(), 0, 0, ofGetElapsedTimeMillis());
		groupOfBricksBack.push_back(*brick2);
		groupOfBricksBack.back().addAnimation(initPos, Brick::ANIMACION_BRICK::CREACION, 0);
		groupOfBricksBack.back().startAnimation();
		groupOfBricksBack.back().startInside();

	}

	kinectPillarBack.setup(PANTALLA_LED_WIDTH, PANTALLA_LED_HEIGHT, &groupOfBricksBack, posicionesGridBack);
	kinectPillarFront.setup(PANTALLA_LED_WIDTH, PANTALLA_LED_HEIGHT, &groupOfBricksFront, posicionesGridFront);
	
	ofAddListener(Brick::evBrickStart, this, &ofApp::brickEventStart);
	ofAddListener(Brick::evBrickStop, this, &ofApp::brickEventStop);

	tempInteractivoEnded.start(1000);
	fboFondo.allocate(PANTALLA_LED_WIDTH, PANTALLA_LED_HEIGHT, GL_RGBA);
	loadFondo();

	lastBrickForDumpBack = groupOfBricksBack.size();
	lastBrickForDumpFront = groupOfBricksFront.size();

}


void ofApp::loadFondo()
{
	int i = ofRandom(0, 40);
	i = 1;
	string nombre = "pillar_grid_" + ofToString(i) + ".png";
	imgFondo.loadImage("pillar_grids/" + nombre);
	
	fboFondo.begin();
	ofClear(0, 255);
	int contador = 0;
	int alpha = 0;
	
	for (int x = 0; x < NUM_COLUMNAS; x++)
	{
		for (int y = 0; y < NUM_FILAS; y++)
		{
			ofSetColor(y*paramFondoFila + ofRandom(paramFondoMin, paramFondoMax));
			ofRectangle r;
			//cout << y * NUM_COLUMNAS + x << endl;
			r.setFromCenter(posicionesGridFront[y * NUM_COLUMNAS + x].pos, ancho, ancho);
			ofDrawRectangle(r);
		}
	}
	fboFondo.end();

}

void ofApp::setupGui()
{

	gui.setup("pillar");
	gui.add(paramIgnorePivotal.setup("ignore pivotal",true));
	gui.add(paramDumpDelay.setup("dump delay", 10, 1, 30));
	gui.add(paramKinectAlpha.setup("kinect alpha", 10, 1, 255));
	gui.add(paramDumpNumBricks.setup("Dump num bricks stay", 35, 1, 60));
	gui.add(paramNumBricksProvocaDump.setup("num bricks provoca dump", 70, 40, 108));
	gui.add(paramFondoMin.setup("nfondo Min", 10, 0, 255));
	gui.add(paramFondoMax.setup("nfondo Max", 10, 0, 255));
	gui.add(paramFondoFila.setup("nfondo fila", 1, 0, 10));
	gui.loadFromFile("gui.xml");

}

void ofApp::updateNDI()
{
	//if (!ndiImageFront.isAllocated())
	//	ndiImageFront.allocate(PANTALLA_LED_WIDTH, PANTALLA_LED_HEIGHT, OF_IMAGE_COLOR_ALPHA);

	//if (!ndiImageBack.isAllocated())
	//	ndiImageBack.allocate(PANTALLA_LED_WIDTH, PANTALLA_LED_HEIGHT, OF_IMAGE_COLOR_ALPHA);

	bNDIFrontready = false;
	bNDIBackready = false;

	int nsenders = ndiReceiverFront.GetSenderCount();
	char name[256];
	if (nsenders > 0) {
		//cout << "Number of NDI senders found: " << nsenders << endl;
		for (int i = 0; i < nsenders; i++) {
			ndiReceiverFront.GetSenderName(name, 256, i);
			string nombre(name);
			//cout << "    Sender " << i << " [" << nombre << "]" << endl;
			if (nombre.find("KINECT" + ofToString(IdInstance) + "_FRONT") != string::npos)
			{
				if (ndiReceiverFront.GetSenderIndex() != i || ofGetElapsedTimeMillis() < 20000)
				{
					ndiReceiverFront.SetSenderIndex(i);
					cout << "Asociado al NDI front " << nombre << endl;
				}
				bNDIFrontready = true;
				break;
			}
		}

		for (int i = 0; i < nsenders; i++) {
			ndiReceiverBack.GetSenderName(name, 256, i);
			string nombre(name);
			if (nombre.find("KINECT" + ofToString(IdInstance) + "_BACK") != string::npos)
			{
				if (ndiReceiverBack.GetSenderIndex() != i || ofGetElapsedTimeMillis() < 20000)
				{
					ndiReceiverBack.SetSenderIndex(i);
					cout << "Asociado al NDI back " << nombre << endl;
				}
				bNDIBackready = true;
				break;
			}
		}
	}
	else
	{
		//cout << "found 0 senderes,  NENG" <<  endl;
	}
	if (bNDIFrontready == false || bNDIBackready == false)
		ndiNeedsUpdate = true;
	else
		ndiNeedsUpdate = false;


}

void ofApp::setupOSC()
{

	int LOCAL_PORT = xml.getValue("LOCAL_PORT", 0);
	string REMOTE_IP_SM = xml.getValue("REMOTE_IP_SM", "");
	int REMOTE_PORT_SM = xml.getValue("REMOTE_PORT_SM", 0);


	string REMOTE_IP_CM = xml.getValue("REMOTE_IP_CM", "");
	int REMOTE_PORT_CM = xml.getValue("REMOTE_PORT_CM", 0);

	string REMOTE_IP_BRIDGE1 = xml.getValue("REMOTE_IP_BRIDGE1", "");
	int REMOTE_PORT_BRIDGE1 = xml.getValue("REMOTE_PORT_BRIDGE1", 0);

	string REMOTE_IP_BRIDGE2 = xml.getValue("REMOTE_IP_BRIDGE2", "");
	int REMOTE_PORT_BRIDGE2 = xml.getValue("REMOTE_PORT_BRIDGE2", 0);

	int PORT_AUDIO = xml.getValue("AUDIO_PORT", 0);

	cout << "Escuchando en puerto " << LOCAL_PORT << "\n";
	cout << "Enviando a SM1 : " << REMOTE_IP_SM << ":" << REMOTE_PORT_SM << "\n";

	cout << "Enviando a CM : " << REMOTE_IP_CM << ":" << REMOTE_PORT_CM << "\n";
	cout << "Enviando a BRIDGE1 : " << REMOTE_IP_BRIDGE1 << ":" << REMOTE_PORT_BRIDGE1 << "\n";
	cout << "Enviando a BRIDGE2 : " << REMOTE_IP_BRIDGE2 << ":" << REMOTE_PORT_BRIDGE2 << "\n";
	cout << "Enviando audio a localhost:" << PORT_AUDIO << "\n";

	// Setup Receiver
	oscReceiver.setup(LOCAL_PORT);
	
	// Setup Sender
	oscSenderSM.setup(REMOTE_IP_SM, REMOTE_PORT_SM);
	oscSenderCM.setup(REMOTE_IP_CM, REMOTE_PORT_CM);
	oscSenderBridge1.setup(REMOTE_IP_BRIDGE1, REMOTE_PORT_BRIDGE1);
	oscSenderBridge2.setup(REMOTE_IP_BRIDGE2, REMOTE_PORT_BRIDGE2);
	oscSenderAudio.setup("localhost", PORT_AUDIO);

}


//void ofApp::setupVideo() {
//	path = "/videos/";
//	dir.listDir(path);
//	
//	dir.sort();
//
//	if (dir.size()) {
//		videos.assign(dir.size(), ofxHapPlayer());
//	}
//
//	for (int i = 0; i < (int)dir.size(); i++) {
//		videos[i].load(dir.getPath(i));
//		videos[i].setLoopState(OF_LOOP_NONE);
//	}
//
//	videoLayer.allocate(PANTALLA_LED_WIDTH, PANTALLA_LED_HEIGHT, GL_RGBA);
//
//}

//--------------------------------------------------------------
void ofApp::inicializa()
{
	ofxOscMessage m;
	m.setAddress("/get_update");
	m.addIntArg(IdInstance);
	estadoGlobal = ESTADOS_GLOBAL::ESTADO_STANDBY;

	ndiNeedsUpdate = true;
}

//--------------------------------------------------------------
void ofApp::sendInit()
{
	//tengo que averiguar que máquina soy y enviar un init a la CM
	string my_ip;
	system("ipfirst.cmd");
	ofFile file("my.ip");
	file >> my_ip;
	
	IdMachine = 0;
	
	for (int i = 1; i < 10; i++)
	{
		if (xml.getValue("MAQUINA" + ofToString(i), "") == my_ip)
			IdMachine = i;
	}
	
	//mi maquina
	if (IdMachine == 9)
		IdMachine = 1;
	cout << "My IP: " << my_ip << ", soy la maquina: " << IdMachine << ", Instancia: " << IdInstance << endl;

	//ofxOscMessage m;
	//m.setAddress("/init " + ofToString(IdMachine));
	//oscSenderCM.sendMessage(m);

}

//--------------------------------------------------------------
void ofApp::resetGrid() {

	for (int i = 0; i < NUM_POSICIONES_GRID; i++)
	{
		posicionesGridFront[i].tipoBrick = -1;
		posicionesGridBack[i].tipoBrick = -1;
	}
	int numPosiciones = xml.getNumTags("SECUENCIA_FRONT");

	secuenciasPosicionesFront.clear();

	if (numPosiciones > 0)
	{
		for (int i = 0; i < numPosiciones; i++)
		{
			string churro = xml.getValue("SECUENCIA_FRONT", "", i);
			if (churro == "")
				cout << "ERROR: secuencia de posiciones Front vacia" << endl;
			else
			{
				vector<string> p = ofSplitString(churro, ",");
				if (p.size() != 108)
					cout << "ERROR: la secuencia Front " << i << " no tiene 108 posiciones" << endl;
				else
				{
					SECUENCIA *s = new SECUENCIA;
					for (int a = 0; a < p.size(); a++)
						s->posiciones[a] = ofToInt(p[a]);
		
					s->posicionActual = 0;
					secuenciasPosicionesFront.push_back(*s);
				}
			}

		}
	}
	else
		cout << "ERROR: no hay sencuencias de posiciones Front en el xml" << endl;

	cout << "hay " << secuenciasPosicionesFront.size() << " secuencias front" << endl;


	numPosiciones = xml.getNumTags("SECUENCIA_BACK");

	secuenciasPosicionesBack.clear();

	if (numPosiciones > 0)
	{
		for (int i = 0; i < numPosiciones; i++)
		{
			string churro = xml.getValue("SECUENCIA_BACK", "", i);
			if (churro == "")
				cout << "ERROR: secuencia de posiciones Back vacia" << endl;
			else
			{
				vector<string> p = ofSplitString(churro, ",");
				if (p.size() != 108)
					cout << "ERROR: la secuencia Back " << i << " no tiene 108 posiciones" << endl;
				else
				{
					SECUENCIA *s = new SECUENCIA;
					for (int a = 0; a < p.size(); a++)
						s->posiciones[a] = ofToInt(p[a]);

					s->posicionActual = 0;
					secuenciasPosicionesBack.push_back(*s);
				}
			}

		}
	}
	else
		cout << "ERROR: no hay sencuencias de posiciones Back en el xml" << endl;

	cout << "hay " << secuenciasPosicionesBack.size() << " secuencias back" << endl;


	numPosiciones = xml.getNumTags("SECUENCIA_BACK_HANDICAP");
	secuenciasPosicionesBackHandicap.clear();

	if (numPosiciones > 0)
	{
		for (int i = 0; i < numPosiciones; i++)
		{
			string churro = xml.getValue("SECUENCIA_BACK_HANDICAP", "", i);
			if (churro == "")
				cout << "ERROR: secuencia de posiciones Back Handicap vacia" << endl;
			else
			{
				vector<string> p = ofSplitString(churro, ",");
				if (p.size() != 108)
					cout << "ERROR: la secuencia Back Handicap " << i << " no tiene 108 posiciones" << endl;
				else
				{
					SECUENCIA *s = new SECUENCIA;
					for (int a = 0; a < p.size(); a++)
						s->posiciones[a] = ofToInt(p[a]);

					s->posicionActual = 0;
					secuenciasPosicionesBackHandicap.push_back(*s);
				}
			}

		}
	}
	else
		cout << "ERROR: no hay sencuencias de posiciones Back Handicap en el xml" << endl;

	cout << "hay " << secuenciasPosicionesBackHandicap.size() << " secuencias handicap" << endl;

}

//--------------------------------------------------------------
//
void ofApp::update(){
	
	kinectPillarFront.paramBinPuntosThreshold = kinectPillarBack.paramBinPuntosThreshold;
	kinectPillarFront.paramBinPuntosMin = kinectPillarBack.paramBinPuntosMin;
	kinectPillarFront.paramBinPuntosMax = kinectPillarBack.paramBinPuntosMax;
	kinectPillarFront.paramBinAlphaMax = kinectPillarBack.paramBinAlphaMax;
	kinectPillarFront.paramBinAlphaMin = kinectPillarBack.paramBinAlphaMin;
	kinectPillarFront.paramBricksAlphaKinect = kinectPillarBack.paramBricksAlphaKinect;
	kinectPillarFront.paramBricksAlphaDamp = kinectPillarBack.paramBricksAlphaDamp;
	kinectPillarFront.paramThersholdGente = kinectPillarBack.paramThersholdGente;
	kinectPillarFront.paramAlphaTransicionGente = kinectPillarBack.paramAlphaTransicionGente;


	if (bNDIBackready  && estadoInteractivo != ESTADOS_INTERACTIVO::INTERACTIVO_INICIO)
		kinectPillarBack.update( ndiImageBack );

	if (bNDIFrontready)
		kinectPillarFront.update(ndiImageFront);


	/*if (bNDIBackready && estadoGlobal == ESTADOS_GLOBAL::ESTADO_STANDBY)
		kinectPillarBack.update(ndiImageBack);

	if (bNDIFrontready && estadoGlobal == ESTADOS_GLOBAL::ESTADO_STANDBY)
		kinectPillarFront.update(ndiImageFront);
*/

	updateOSC();
	
	if (bVideoActivo)
		updateVideo();

    for(int i = 0; i < groupOfBricksFront.size(); i++){
        groupOfBricksFront[i].update();
    }

	for (int i = 0; i < groupOfBricksBack.size(); i++) {
		groupOfBricksBack[i].update();
	}

	pillarFront.begin();
		ofClear(0, 0);

		for (int i = 0; i < groupOfBricksFront.size(); i++) {
			if (groupOfBricksFront[i].getEstado() != Brick::ESTADO_BRICK::MOVING)
				groupOfBricksFront[i].draw();
		}

		for (int i = 0; i < groupOfBricksFront.size(); i++) {
			if (groupOfBricksFront[i].getEstado() == Brick::ESTADO_BRICK::MOVING)
				groupOfBricksFront[i].draw();
		}

	pillarFront.end();

	pillarBack.begin();
		ofClear(0, 0);
		
		for (int i = 0; i < groupOfBricksBack.size(); i++) {
			if (groupOfBricksBack[i].getEstado() != Brick::ESTADO_BRICK::MOVING)
				groupOfBricksBack[i].draw();
		}

		for (int i = 0; i < groupOfBricksBack.size(); i++) {
			if (groupOfBricksBack[i].getEstado() == Brick::ESTADO_BRICK::MOVING)
				groupOfBricksBack[i].draw();
		}

	pillarBack.end();

	if (ofGetElapsedTimeMillis() - lastNDIupdate > 5000)
	{
		updateNDI();
		lastNDIupdate = ofGetElapsedTimeMillis();
	}

	if (estadoGlobal == ESTADOS_GLOBAL::ESTADO_DUMP)
		updateDump();

	//if (ofGetElapsedTimeMillis() - lastWindowRelocation > 60000)
	//{
	//	lastWindowRelocation = ofGetElapsedTimeMillis();
	//	posicionaVentana();
	//}
}


void ofApp::posicionaVentana()
{
	if (bPosicionDerecha)
		ofSetWindowPosition(1920 + 1280, 0);
	//ofSetWindowPosition(1920, 0);
	else
		ofSetWindowPosition(0, 0);
}

//--------------------------------------------------------------
//
void ofApp::updateOSC()
{

	// check for waiting messages
	while (oscReceiver.hasWaitingMessages()) {
		// get the next message
		ofxOscMessage m;
		oscReceiver.getNextMessage(m);
		logOSCMessage(m);
		// Mensajes desde SM

		if (m.getAddress() == "/new_brick") {
			if (soyYo(m))
			{
				int tipoBrick = m.getArgAsInt32(1);
				newBrick(tipoBrick);
			}
		}
		else if (m.getAddress() == "/start_interaction")
		{
			if (soyYo(m))
				startInteraction();
		}
		else if (m.getAddress() == "/end_interaction")
		{
			if (soyYo(m))
				endInteraction();
		}
		else if (m.getAddress() == "/bricks_update")
		{
			if (soyYo(m))
				updateBricks(m.getArgAsString(1));
		}
		else if (m.getAddress() == "/pivot_video")
		{
			if (soyYo(m) && paramIgnorePivotal == false)
				startPivotVideo(m.getArgAsString(1));
		}
		else if (m.getAddress() == "/dump")
		{
			if (soyYo(m))
				startDump();
		}
		// Mensajes desde CM
		if (m.getAddress() == "/set")
		{
			//int machine = m.getArgAsInt(0);
			//int instance = m.getArgAsInt(1);
			//if (machine == IdMachine)
			//{
			//	cout << "CM -> SET instance " << instance << endl;
			//	//ojo
			//	IdInstance = instance;
			//	inicializa();
			//}
		}

	}

	if (ofGetElapsedTimeMillis() - lastPing > INTERVALO_PING)
	{
		lastPing = ofGetElapsedTimeMillis();
		ofxOscMessage m;
		m.setAddress("/ping " + ofToString(IdMachine) + " " + ofToString(IdInstance));
		oscSenderCM.sendMessage(m);
	}

}

void ofApp::updateVideo() {
	
	videoPlayer.update();
	if (bVideoActivo && videoPlayer.getIsMovieDone())
	{
		videoPlayer.close();
		bVideoActivo = false;
		cout << "fin video" << endl;
	}

	//videoLayer.begin();
	//	ofClear(0, 0);	
	//	if (videos[currentVideo].getIsMovieDone())
	//		bVideoActivo = false;
	//	else
	//		videos[currentVideo].draw(0,0);
	//videoLayer.end();
}

//--------------------------------------------------------------
//
void ofApp::startPivotVideo(string path)
{
	cout << "video player: " << path << endl;
	videoPlayer.load(path);
	videoPlayer.setLoopState(OF_LOOP_NONE);
	videoPlayer.play();
	bVideoActivo = true;
}

//--------------------------------------------------------------
//
//void ofApp::startDumpTimer()
//{
//	estadoGlobal = ESTADOS_GLOBAL::ESTADO_DUMP;
//	timeDumpTimerStart = ofGetElapsedTimeMillis();
//	estadoDump = ESTADOS_DUMP::DUMP_DELAY_INICIAL;
//}

//--------------------------------------------------------------
//
void ofApp::startDump()
{

	//if (!tempInteractivoEnded.isDone())
	//{
	//	cout << "se ha lanzado un dump pero aun está terminando la interactividad" << endl;
	//	return;
	//}
	//if (estadoGlobal == ESTADOS_GLOBAL::ESTADO_INTERACTIVO)
	//{
	//	cout << "se ha llamado a start dump con una interaccion a medias." << endl;
	//	//endInteraction();
	//	return;
	//}
	if (estadoGlobal == ESTADOS_GLOBAL::ESTADO_DUMP)
	{
		cout << "se ha llamado a start Dump, pero ya estaba en dump" << endl;
		return;
	}
	cout << "dump start" << endl;
	vector<int> posiblesFront;
	vector<int> posiblesBack;

	////hay que elegir 50 posiciones que no harán dump
	//for (int i = 0 ; i < NUM_POSICIONES_GRID ; i++)
	//{
	//	if (posicionesGridFront[i].tipoBrick != -1)
	//		posiblesFront.push_back(i);
	//	if (posicionesGridBack[i].tipoBrick != -1)
	//		posiblesBack.push_back(i);
	//}
	//cout << "hay " << posiblesFront.size() << "posibles Front" << endl;
	//cout << "hay " << posiblesBack.size() << "posibles Back" << endl;

	int cuantasFront = lastBrickForDumpFront-brickCounterArriba;
	int cuantasBack = lastBrickForDumpBack - brickCounterArriba;
	if (cuantasFront < paramDumpNumBricks || cuantasBack <paramDumpNumBricks)
	{
		cout << "Error en dump, no hay mas de " << paramDumpNumBricks << " bricks para dumpear NENG" << endl;
		return;
	}

	for (int i = 0; i < groupOfBricksFront.size(); i++)
		groupOfBricksFront[i].stopInside();

	for (int i = 0; i < groupOfBricksBack.size(); i++)
		groupOfBricksBack[i].stopInside();

	timeDumpStart = ofGetElapsedTimeMillis();

	//azules
	for (int i = 0; i < lastBrickForDumpFront; i++)
	{
		if (groupOfBricksFront[i].getTipo() == 9)
		{
			ofVec2f posBrick = groupOfBricksFront[i].getPos();
			groupOfBricksFront[i].addAnimation(posBrick, Brick::ANIMACION_BRICK::DELAY, DUMP_FASE1_DELAY * 1000 + (i - POSICIONES_PERMANENTES) * 100);
			groupOfBricksFront[i].addAnimation(ofVec2f(posBrick.x, -56), Brick::ANIMACION_BRICK::DUMP_PILLAR, 800);
			groupOfBricksFront[i].setAlphaTarget(255, 0.1f);
			groupOfBricksFront[i].startAnimation();
			groupOfBricksFront[i].setDumpState(1); //dumping hacia arriba
		}
	}
	for (int i = 0; i < lastBrickForDumpBack; i++)
	{
		if (groupOfBricksBack[i].getTipo() == 9)
		{
			ofVec2f posBrick = groupOfBricksBack[i].getPos();
			groupOfBricksBack[i].addAnimation(posBrick, Brick::ANIMACION_BRICK::DELAY, DUMP_FASE1_DELAY * 1000 + (i - POSICIONES_PERMANENTES) * 100);
			groupOfBricksBack[i].addAnimation(ofVec2f(posBrick.x, -56), Brick::ANIMACION_BRICK::DUMP_PILLAR, 800);
			groupOfBricksBack[i].setAlphaTarget(255, 0.1f);
			groupOfBricksBack[i].startAnimation();
			groupOfBricksBack[i].setDumpState(1); //dumping hacia arriba
		}
	}

	//los que no se quedan tienen que ir hacia arriba
	for (int i = 0; i < lastBrickForDumpFront - paramDumpNumBricks; i++)
	{
		if (groupOfBricksFront[i].getTipo() != 9)
		{
			ofVec2f posBrick = groupOfBricksFront[i].getPos();
			groupOfBricksFront[i].addAnimation(posBrick, Brick::ANIMACION_BRICK::DELAY, DUMP_FASE1_DELAY * 1000 + (i - POSICIONES_PERMANENTES) * 100);
			groupOfBricksFront[i].addAnimation(ofVec2f(posBrick.x, -56), Brick::ANIMACION_BRICK::DUMP_PILLAR, 800);
			groupOfBricksFront[i].setAlphaTarget(255, 0.1f);
			groupOfBricksFront[i].startAnimation();
			groupOfBricksFront[i].setDumpState(1); //dumping hacia arriba

		}
	}
	for (int i = 0; i < lastBrickForDumpBack - paramDumpNumBricks; i++)
	{
		if (groupOfBricksBack[i].getTipo() != 9)
		{
			ofVec2f posBrick = groupOfBricksBack[i].getPos();
			groupOfBricksBack[i].addAnimation(posBrick, Brick::ANIMACION_BRICK::DELAY, DUMP_FASE1_DELAY * 1000 + (i - POSICIONES_PERMANENTES) * 100);
			groupOfBricksBack[i].addAnimation(ofVec2f(posBrick.x, -56), Brick::ANIMACION_BRICK::DUMP_PILLAR, 800);
			groupOfBricksBack[i].setAlphaTarget(255, 0.1f);
			groupOfBricksBack[i].startAnimation();
			groupOfBricksBack[i].setDumpState(1); //dumping hacia arriba
		}
	}

	//añado todos los azules


	estadoGlobal = ESTADOS_GLOBAL::ESTADO_DUMP;
	estadoDump = ESTADOS_DUMP::DUMP_INICIO;

}

//--------------------------------------------------------------
//
void ofApp::updateDump()
{
	
	if (estadoDump == ESTADOS_DUMP::DUMP_INICIO)
	{
		//los que salgan por arriba, se borran y envia el mensaje al bridge para que aparezcan
		//cout << "front" << endl;
		for (int i = 0; i < groupOfBricksFront.size(); i++)
		{
			if (groupOfBricksFront[i].getPos().y < 0 && groupOfBricksFront[i].getDumpState()==1)
			{
				sendBrickArrival(groupOfBricksFront[i].getTipo(), groupOfBricksFront[i].getPos().x / ancho);
				groupOfBricksFront[i].setDumpState(2);
				//groupOfBricksFront.erase(groupOfBricksFront.begin() + i);
				//if (i > 0) i--;
			}
		}
		//cout << "back" << endl;
		for (int i = 0; i < groupOfBricksBack.size(); i++)
		{
			if (groupOfBricksBack[i].getPos().y < 0)
			{
				groupOfBricksBack[i].setDumpState(2);
				//groupOfBricksBack.erase(groupOfBricksBack.begin() + i);
				//if (i > 0) i--;
			}
		}

		//cuando pase el timpo de delay+subida hay que recolocar los bricks que se han quedado
		if (ofGetElapsedTimeMillis() - timeDumpStart > (DUMP_FASE1_DELAY + DUMP_FASE2_SUBIR) * 1000)
		{
			for (int i = 9; i < NUM_POSICIONES_GRID; i++)
			{ 
				posicionesGridFront[i].tipoBrick = -1;
				posicionesGridBack[i].tipoBrick = -1;
			}
			estadoDump = ESTADOS_DUMP::DUMP_REORDENANDO;
			secuenciaPosicionesFrontActiva++;
			if (secuenciaPosicionesFrontActiva == secuenciasPosicionesFront.size())
				secuenciaPosicionesFrontActiva = 0;
			secuenciasPosicionesFront[secuenciaPosicionesFrontActiva].posicionActual = 0;
			
			secuenciaPosicionesBackActiva++;
			if (secuenciaPosicionesBackActiva == secuenciasPosicionesBack.size())
				secuenciaPosicionesBackActiva = 0;
			secuenciasPosicionesBack[secuenciaPosicionesBackActiva].posicionActual = 0;
			
			secuenciaPosicionesBackHandicapActiva++;
			if (secuenciaPosicionesBackHandicapActiva == secuenciasPosicionesBackHandicap.size())
				secuenciaPosicionesBackHandicapActiva = 0;
			secuenciasPosicionesBackHandicap[secuenciaPosicionesBackHandicapActiva].posicionActual = 0;

			int frontBorrados = 0;
			int backBorrados = 0;
			vector< Brick >::iterator it = groupOfBricksFront.begin();
			while (it != groupOfBricksFront.end()) {
				float y = (*it).getPos().y;
				if (y < 0) {
					frontBorrados++;
					it = groupOfBricksFront.erase(it);
				}
				else ++it;
			}
			vector< Brick >::iterator it2 = groupOfBricksBack.begin();
			while (it2 != groupOfBricksBack.end()) {
				float y = (*it2).getPos().y;
				if (y < 0) {
					backBorrados++;
					it2 = groupOfBricksBack.erase(it2);
				}
				else ++it2;
			}

			for (int i = 0; i < lastBrickForDumpFront-frontBorrados ; i++)
			{
				int nuevaPos = buscaPosicionNuevoBrickAbajo(TIPO_POSICIONES::FRONT);
				groupOfBricksFront[i].addAnimation(posicionesGridFront[nuevaPos].pos, Brick::ANIMACION_BRICK::ABAJO_PILLAR, 500);
				groupOfBricksFront[i].startAnimation();
				posicionesGridFront[nuevaPos].tipoBrick = groupOfBricksFront[i].getTipo();
			}

			for (int i = 0 ; i < lastBrickForDumpBack - backBorrados; i++)
			{
				int nuevaPos = buscaPosicionNuevoBrickAbajo(TIPO_POSICIONES::BACK);
				groupOfBricksBack[i].addAnimation(posicionesGridFront[nuevaPos].pos, Brick::ANIMACION_BRICK::ABAJO_PILLAR, 500);
				groupOfBricksBack[i].startAnimation();
				posicionesGridBack[nuevaPos].tipoBrick = groupOfBricksBack[i].getTipo();
			}
		}
	}
	
	if (estadoDump == ESTADOS_DUMP::DUMP_REORDENANDO)
	{
		//hay que eliminar los bricks que se han ido y enviar el mensaje al BRIDGE para que aparezcan
		
		if (ofGetElapsedTimeMillis() - timeDumpStart > (DUMP_FASE1_DELAY + DUMP_FASE2_SUBIR + DUMP_FASE3_REORDENAR) * 1000)
		{
			estadoGlobal = ESTADOS_GLOBAL::ESTADO_STANDBY;
			for (int i = 0; i < groupOfBricksFront.size(); i++)
				groupOfBricksFront[i].startInside();

			for (int i = 0; i < groupOfBricksBack.size(); i++)
				groupOfBricksBack[i].startInside();

			lastBrickForDumpFront = groupOfBricksFront.size();
			lastBrickForDumpBack = groupOfBricksBack.size();

			sendUpdateBricks();

		}
	}

}

void ofApp::sendBrickArrival(int tipoBrick, int posicion)
{
	ofxOscMessage m;
	m.setAddress("/brick_arrival");
	//ojo
	m.addIntArg(IdInstance);
	//m.addIntArg(ofRandom(1,7));
	m.addIntArg(tipoBrick);
	m.addIntArg(posicion);
	oscSenderBridge1.sendMessage(m);
	oscSenderBridge2.sendMessage(m);

}

//--------------------------------------------------------------
//
void ofApp::startInteraction()
{
	cout << "startInteraction" << endl;
	//if (estadoGlobal == ESTADOS_GLOBAL::ESTADO_DUMP)
	//{
	//	cout << "no se puede iniciar interaccion porque esta haciendo un Dump" << endl;
	//	return;
	//}
	if (estadoInteractivo == ESTADOS_INTERACTIVO::INTERACTIVO_INICIO)
	{
		cout << "no se puede iniciar interaccion porque ya está en modo interaccion" << endl;
		return;
	}

	for (int i = 0; i < groupOfBricksBack.size(); i++)
		groupOfBricksBack[i].setAlphaTarget(50, 0.1f);


	//estadoGlobal = ESTADOS_GLOBAL::ESTADO_INTERACTIVO;
	estadoInteractivo = ESTADOS_INTERACTIVO::INTERACTIVO_INICIO;
	brickCounterArriba = 0;
	

	//el primer brick va en el centro, el resto van en random

	//vector<int> arriba;
	//for (int i = 0; i <= 3; i++)
	//	arriba.push_back(i);
	//for (int i = 5; i <= 8; i++)
	//	arriba.push_back(i);

	//unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

	//shuffle(arriba.begin(), arriba.end(), std::default_random_engine(seed));

	//for (int i = 1; i <= 8; i++)
	//{
	//	posicionesArriba[i] = arriba[i-1];
	//}
	//posicionesArriba[0] = 4;

	posicionesArriba[0] = 4;
	posicionesArriba[1] = 5;
	posicionesArriba[2] = 3;
	posicionesArriba[3] = 6;
	posicionesArriba[4] = 2;
	posicionesArriba[5] = 7;
	posicionesArriba[6] = 1;
	posicionesArriba[7] = 8;
	posicionesArriba[8] = 0;

	lastBrickForDumpFront = groupOfBricksFront.size();
	lastBrickForDumpBack = groupOfBricksBack.size();


}

//--------------------------------------------------------------
//
void ofApp::endInteraction()
{
	if (estadoInteractivo != ESTADOS_INTERACTIVO::INTERACTIVO_INICIO)
		return;

	estadoInteractivo = ESTADOS_INTERACTIVO::INTERACTIVO_BAJANDO;
	//hay que decirle a los bricks que hay arriba, que tienen que bajar
	int contador1 = 0;
	int contador2 = 0;
	
	//hasta que no pasen 6 segundos, no se puede hacer un damp

	tempInteractivoEnded.start(6000);

	for (int i = 0; i < 9; i++)
	{
		if (posicionesGridFront[i].tipoBrick != -1)
		{
			int newPos = buscaPosicionNuevoBrickAbajo(TIPO_POSICIONES::FRONT);
			posicionesGridFront[newPos].tipoBrick = posicionesGridFront[i].tipoBrick;
			posicionesGridFront[i].tipoBrick = -1;
			for (int br = 0; br < groupOfBricksFront.size(); br++)
			{
				if (groupOfBricksFront[br].getPos() == posicionesGridFront[i].pos)
				{
					groupOfBricksFront[br].addAnimation(posicionesGridFront[newPos].pos, Brick::ANIMACION_BRICK::DELAY, contador1 * 300);
					groupOfBricksFront[br].addAnimation(posicionesGridFront[newPos].pos, Brick::ANIMACION_BRICK::ABAJO_PILLAR, 0);
					groupOfBricksFront[br].startAnimation();
					contador1++;
					//break;
				}
			}
		}
		if (posicionesGridBack[i].tipoBrick != -1)
		{
			int newPos = buscaPosicionNuevoBrickAbajo(TIPO_POSICIONES::BACK);
			posicionesGridBack[newPos].tipoBrick = posicionesGridBack[i].tipoBrick;
			posicionesGridBack[i].tipoBrick = -1;
			for (int br = 0; br < groupOfBricksBack.size(); br++)
			{
				if (groupOfBricksBack[br].getPos() == posicionesGridBack[i].pos)
				{
					groupOfBricksBack[br].addAnimation(posicionesGridBack[newPos].pos, Brick::ANIMACION_BRICK::DELAY, contador2 * 300);
					groupOfBricksBack[br].addAnimation(posicionesGridBack[newPos].pos, Brick::ANIMACION_BRICK::ABAJO_PILLAR, 0);
					groupOfBricksBack[br].startAnimation();
					contador2++;
					//break;
				}
			}

		}
	}

	//se han bajado los bricks, hay que actualizar el estado
	sendUpdateBricks();

	//estadoGlobal = ESTADOS_GLOBAL::ESTADO_STANDBY;
	estadoInteractivo = ESTADOS_INTERACTIVO::INTERACTIVO_OFF;
	for (int i = 0; i < groupOfBricksBack.size(); i++)
		groupOfBricksBack[i].setAlphaTarget(255, 0.01f);

	lastBrickForDumpFront = groupOfBricksFront.size();
	lastBrickForDumpBack = groupOfBricksBack.size();

	brickCounterArriba = 0;

}

//--------------------------------------------------------------
// soy yo?
//
bool ofApp::soyYo(ofxOscMessage &m) {

	int instance = m.getArgAsInt32(0);
	return (instance == IdInstance);
}

//--------------------------------------------------------------
// 
void ofApp::logOSCMessage(ofxOscMessage &m) {

	string msg_string;
	msg_string = m.getAddress();
	msg_string += ": ";
	for (int i = 0; i < m.getNumArgs(); i++) {
		// get the argument type
		msg_string += m.getArgTypeName(i);
		msg_string += ":";
		// display the argument - make sure we get the right type
		if (m.getArgType(i) == OFXOSC_TYPE_INT32) {
			msg_string += ofToString(m.getArgAsInt32(i));
		}
		else if (m.getArgType(i) == OFXOSC_TYPE_FLOAT) {
			msg_string += ofToString(m.getArgAsFloat(i));
		}
		else if (m.getArgType(i) == OFXOSC_TYPE_STRING) {
			msg_string += m.getArgAsString(i);
		}
		else {
			msg_string += "unknown";
		}
	}

	cout << "OSC: " << msg_string << endl;

}

//--------------------------------------------------------------
// le ha pedido a la SM el estado de los bricks y el SM nos ha contestado
//
void ofApp::updateBricks(string churro) {

	vector<string> tipos = ofSplitString(churro, ",");
	if (tipos.size() != NUM_POSICIONES_GRID)
		cout << "ERROR: ha llegado un update_bricks de la SM que no tiene " << NUM_POSICIONES_GRID << " bricks" << endl;
	else
	{
		for (int i = 0; i < NUM_POSICIONES_GRID; i++)
		{
			posicionesGridFront[i].tipoBrick = ofToInt(tipos[i]);
			posicionesGridBack[i].tipoBrick = ofToInt(tipos[i]);
		}

		secuenciasPosicionesFront[secuenciaPosicionesFrontActiva].posicionActual = 0;
		secuenciasPosicionesBack[secuenciaPosicionesBackActiva].posicionActual = 0;
		secuenciasPosicionesBackHandicap[secuenciaPosicionesBackHandicapActiva].posicionActual = 0;
		for (int i = 0; i < NUM_POSICIONES_GRID; i++)
		{
			if (posicionesGridFront[i].tipoBrick != -1)
			{
				secuenciasPosicionesFront[secuenciaPosicionesFrontActiva].posicionActual++;
				secuenciasPosicionesBack[secuenciaPosicionesBackActiva].posicionActual++;
				secuenciasPosicionesBackHandicap[secuenciaPosicionesBackHandicapActiva].posicionActual++;
			}
		}
	}

}

//--------------------------------------------------------------
// envia el estado de los bricks a la SM
//
void ofApp::sendUpdateBricks() {
	
	string churro;
	churro = churro + ofToString(posicionesGridBack[0].tipoBrick);
	for (int i = 1; i < NUM_POSICIONES_GRID; i++)
		churro = churro + "," +ofToString(posicionesGridBack[i].tipoBrick);

	ofxOscMessage m;
	m.setAddress("/bricks_update");
	m.addIntArg(IdInstance);
	m.addStringArg(churro);
	oscSenderSM.sendMessage(m);


}

//--------------------------------------------------------------
void ofApp::draw(){

	ofBackground(0);
	ofSetColor(255);

	procesaNDI();

	//switch (estadoGlobal)
	//{
	//	
	//	case ESTADOS_GLOBAL::ESTADO_STANDBY:
	//		drawStandBy();
	//		break;
	//	
	//	case ESTADOS_GLOBAL::ESTADO_INTERACTIVO:
	//		drawInteractivo();
	//		break;
	//	
	//	case ESTADOS_GLOBAL::ESTADO_DUMP:
	//		drawDump();
	//		break;

	//	//case ESTADOS_GLOBAL::ESTADO_PIVOTAL:
	//	//	drawVideo();
	//	//	break;

	//}

	//ofSetColor(255,255,255,127);

	ofSetColor(255);

	//ofEnableBlendMode(OF_BLENDMODE_MULTIPLY);
	//ofSetColor(255,255,255, paramKinectAlpha);
	ofSetColor(255);
	//imgFondo.draw(0, 0);
	//imgFondo.draw(PANTALLA_LED_WIDTH, 0);
	fboFondo.draw(0, 0);
	fboFondo.draw(PANTALLA_LED_WIDTH, 0);
	if (estadoInteractivo != ESTADOS_INTERACTIVO::INTERACTIVO_INICIO)
	{
		kinectPillarBack.draw(0, 0);
	}
	kinectPillarFront.draw(PANTALLA_LED_WIDTH, 0);


	ofEnableBlendMode(OF_BLENDMODE_ALPHA);
	
	ofSetColor(255,paramKinectAlpha);
	pillarFront.draw(PANTALLA_LED_WIDTH, 0);
	pillarBack.draw(0, 0);

	//ofSetColor(255, 127);
	//if (bNDIFrontready)
	//	ndiImageBack.draw(PANTALLA_LED_WIDTH*2, 0);
	//
	//kinectPillar.draw(PANTALLA_LED_WIDTH * 2, 0);

	if (bNDIFrontready)
		ndiImageFront.draw(PANTALLA_LED_WIDTH*3, 0);

	if (bVideoActivo)
		drawVideo();

	if (bGuiVisible)
		gui.draw();

	ofSetColor(255, 0, 0);
	//ofDrawLine(PANTALLA_LED_WIDTH, 0, PANTALLA_LED_WIDTH, PANTALLA_LED_HEIGHT);
	ofDrawLine(PANTALLA_LED_WIDTH * 3, 0, PANTALLA_LED_WIDTH * 3, PANTALLA_LED_HEIGHT);
	string e;
	if (estadoGlobal == ESTADOS_GLOBAL::ESTADO_DUMP)
		e = "DUMP";
	if (estadoGlobal == ESTADOS_GLOBAL::ESTADO_STANDBY)
		e = "STAND_BY";
	if (estadoGlobal == ESTADOS_GLOBAL::ESTADO_OFFLINE)
		e = "OFFLINE";

	
	string fps = "Estado = " + e+ ", FPS: " + ofToString(ofGetFrameRate());
	//ofDrawBitmapString(fps, 10, 10);

	string who = "Maq = " + ofToString(IdMachine) + ", Inst: " + ofToString(IdInstance);
	//ofDrawBitmapString(who, (float)PANTALLA_LED_WIDTH*0.6f, 10);

}

//--------------------------------------------------------------
//
void ofApp::procesaNDI()
{

	ndiReceiverBack.ReceiveImage(fboNDIback);
	ndiReceiverFront.ReceiveImage(fboNDIfront);
	//ndiReceiverFront.ReceiveImage(ndiImageFront);

	if (bNDIBackready)
		fboNDIback.readToPixels(ndiImageBack.getPixelsRef());

	if (bNDIFrontready)
		fboNDIfront.readToPixels(ndiImageFront.getPixelsRef());

	//ndiImageBack.setFromPixels(ndiPixels);

	//unsigned int width = (unsigned int)NDI_WIDTH;
	//unsigned int height = (unsigned int)NDI_HEIGHT;
	//if (ndiReceiverBack.ReceiveImage(ndiChars, width, height)) {
	//	if (width != NDI_WIDTH || height != NDI_HEIGHT) {
	//		// Update sender dimensions
	//		senderWidth = width;
	//		senderHeight = height;
	//		// Reallocate the receiving buffer
	//		delete ndiChars;
	//		ndiChars = new unsigned char[senderWidth*senderHeight * 4];
	//		// Reallocate the image we use for display
	//		ndiImageBack.allocate(senderWidth, senderHeight, OF_IMAGE_COLOR_ALPHA);
	//	}
	//	else {
	//		// Update the display image
	//		ndiImageBack.getPixels().setFromExternalPixels(ndiChars, senderWidth, senderHeight, 4);
	//		ndiImageBack.update();
	//	}
	//}

	//ndiReceiverBack.ReceiveImage(ndiPixels);
	//ndiImageBack.allocate(ndiPixels.getWidth(), ndiPixels.getHeight(), OF_IMAGE_COLOR_ALPHA);
	//ndiImageBack.setFromPixels(ndiPixels);

}

//--------------------------------------------------------------
//
void ofApp::drawStandBy()
{

}

//--------------------------------------------------------------
//
void ofApp::drawVideo()
{
	ofSetColor(255);
	videoPlayer.draw(PANTALLA_LED_WIDTH, 0);
}


//--------------------------------------------------------------
//
void ofApp::drawInteractivo()
{

}


//--------------------------------------------------------------
//
void ofApp::drawDump()
{

}


//--------------------------------------------------------------
//
void ofApp::keyPressed(int key){
	if (key == 'a') newBrick(ofRandom(10));

	if (key == 's')
	{
		kinectPillarFront.save();
		kinectPillarBack.save();
		gui.saveToFile("gui.xml");

	}

	if (key == 'i')
		startInteraction();

	if (key == 'b')
		endInteraction();

	if (key == 'd')
		startDump();

	if (key == 'v')
	{
		bPosicionDerecha = !bPosicionDerecha;
		posicionaVentana();
	}

	if (key == '1')
		for (int i = 0; i < 100; i++)
			sendBrickArrival(ofRandom(10), ofRandom(9));

	if (key == 'u')
	{
		ofxOscMessage m;
		m.setAddress("/get_update");
		m.addIntArg(IdInstance);
		oscSenderSM.sendMessage(m);
	}

	if (key == '0')
		startPivotVideo("videos/advertise/ad_cisco_pillar_1.mov");

	if (key == ' ')
	{
		bGuiVisible = !bGuiVisible;
		kinectPillarBack.setGuiVisible(bGuiVisible);
	}

	if (key == 'o')
		sendAudio(TIPO_AUDIO::BRICK_START);
	if (key == 'p')
		sendAudio(TIPO_AUDIO::BRICK_STOP);
	if (key == 'f')
		loadFondo();
}

void ofApp::sendAudio(TIPO_AUDIO tipo)
{
	ofxOscMessage m;
	if (tipo == TIPO_AUDIO::BRICK_START)
		m.setAddress("/start");
	if (tipo == TIPO_AUDIO::BRICK_STOP)
		m.setAddress("/stop");
	oscSenderAudio.sendMessage(m);
	//cout << "enviado audio " << m.getAddress() << endl;
}

void ofApp::brickEventStart(int &e)
{
	sendAudio(TIPO_AUDIO::BRICK_START);
}

void ofApp::brickEventStop(int &e)
{
	sendAudio(TIPO_AUDIO::BRICK_STOP);
}
/*
void ofApp::move(ofVec2f position){
    groupOfBricks[groupOfBricks.size()-1].zenoToPoint(position);
}
*/


void ofApp::setupKinectSkeleton() {
	smallFont.loadFont("selena.otf", 16); //http://openfontlibrary.org/en/font/selena
	largeFont.loadFont("selena.otf", 48);

	kinect.setup(12345, smallFont);
	skeletons = kinect.getSkeletons();
	renderer.setup(skeletons, largeFont);
}

void ofApp::updateKinectSkeleton() {
	kinect.update();

}

void ofApp::drawKinectSkeleton() {
	kinect.drawDebug();
	ofPushMatrix();
	ofTranslate(-pillarFront.getWidth() / 2, 0);
	renderer.draw();
	ofPopMatrix();
	//Print out commands and text

	string commands = "COMMANDS\n\n";
	commands.append("d = debug\n");
	commands.append("j = joints\n");
	commands.append("b = bones\n");
	commands.append("h = hands\n");
	commands.append("r = ranges\n");
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {
	


}

//--------------------------------------------------------
//
void ofApp::newBrick( int tipoBrick ){

	if (estadoInteractivo != ESTADOS_INTERACTIVO::INTERACTIVO_INICIO)
		return;

    Brick tempBrick1;
	Brick tempBrick2;
	int in = 2;
   // int in = 1;
    int out = 1;
    
    indexPosition = buscaPosicionNuevoBrickArriba();
	// por si nos hemos quedado sin huecos
	if (indexPosition == -1)
		return;
    ofVec2f destPosFront = posicionesGridFront[indexPosition].pos;
	ofVec2f destPosBack = posicionesGridBack[indexPosition].pos;
	posicionesGridFront[indexPosition].tipoBrick = tipoBrick;
	posicionesGridBack[indexPosition].tipoBrick = tipoBrick;
	//  std::cout << indexPosition << initPos << endl;
    
    //BUSCAR POSICIÓN INICIAL CUADRADA CON EL VÍDEO
    //initPos = gridPositions[50];
	ofVec2f initPos = posicionesGridFront[31].pos;
	ofVec2f initPosTactil;

		//initPosTactil = posicionesGridBack[22].pos;

	tempBrick1.setup(initPos, tipoBrick, groupOfBricksFront.size(), in, out , ofGetElapsedTimeMillis());
	groupOfBricksFront.push_back(tempBrick1);
	groupOfBricksFront.back().addAnimation(initPos, Brick::ANIMACION_BRICK::CREACION, 0);
	groupOfBricksFront.back().addAnimation(destPosFront, Brick::ANIMACION_BRICK::ARRIBA_PILLAR, 0);
	groupOfBricksFront.back().startAnimation();


	if (IdInstance == 5)
	{
		initPosTactil = posicionesGridBack[58].pos;
		initPos = posicionesGridBack[67].pos;
	}
	else
	{
		initPosTactil = posicionesGridBack[22].pos;
	}

	tempBrick2.setup(initPos, tipoBrick, groupOfBricksBack.size(), in, out, ofGetElapsedTimeMillis());
	groupOfBricksBack.push_back(tempBrick2);
	groupOfBricksBack.back().addAnimation(initPosTactil, Brick::ANIMACION_BRICK::CREACION_TACTIL,0);
	groupOfBricksBack.back().addAnimation(destPosBack, Brick::ANIMACION_BRICK::ARRIBA_PILLAR, 0);
	groupOfBricksBack.back().startAnimation();


    // std::cout << "Type of new brick: " << tempTypeBrick << endl;
    // std::cout << "Number of bricks: " << groupOfBricks.size() << endl;
  
    brickCounterArriba++;
	 //indexPosition = filaSwap[counter];
     //shuffle (filaSwap.begin(), filaSwap.end(), std::default_random_engine(seed));
  

	//string brickIsNew = ofToString(groupOfBricksFront.size()) + "," +ofToString(tipoBrick) +","+ ofToString(indexPosition);
	//std::cout << brickIsNew << endl;
	//data->sendNewBrick(brickIsNew);
   
}

//=============================================
//
int ofApp::buscaPosicionNuevoBrickAbajo( TIPO_POSICIONES tipo )
{
	if (tipo == TIPO_POSICIONES::BACK && IdInstance != 5)
	{
		int posActual = secuenciasPosicionesBack[secuenciaPosicionesBackActiva].posicionActual;
		cout << "posicion indice abajo back = " << posActual << endl;
		secuenciasPosicionesBack[secuenciaPosicionesBackActiva].posicionActual++;
		if (secuenciasPosicionesBack[secuenciaPosicionesBackActiva].posicionActual >= 108)
		{
			cout << "ERROR: pidiendo una posicion > 108" << endl;
			secuenciasPosicionesBack[secuenciaPosicionesBackActiva].posicionActual = 107;
		}
		//cout << "nueva pos abajo Back index= " << posActual << endl;
		int pos = secuenciasPosicionesBack[secuenciaPosicionesBackActiva].posiciones[posActual];
		if (pos == -1)
			return 107;
		else
			return pos;
	}

	//handicap
	if (tipo == TIPO_POSICIONES::BACK && IdInstance == 5)
	{
		int posActual = secuenciasPosicionesBackHandicap[secuenciaPosicionesBackHandicapActiva].posicionActual;
		secuenciasPosicionesBackHandicap[secuenciaPosicionesBackHandicapActiva].posicionActual++;
		if (secuenciasPosicionesBackHandicap[secuenciaPosicionesBackHandicapActiva].posicionActual >= 108)
		{
			cout << "ERROR: pidiendo una posicion > 108" << endl;
			secuenciasPosicionesBackHandicap[secuenciaPosicionesBackHandicapActiva].posicionActual = 107;
		}
		//cout << "nueva pos abajo Back handicap index= " << posActual << endl;

		int pos = secuenciasPosicionesBackHandicap[secuenciaPosicionesBackHandicapActiva].posiciones[posActual];
		if (pos == -1)
			return 107;
		else
			return pos;
	}


	if (tipo == TIPO_POSICIONES::FRONT)
	{
		int posActual = secuenciasPosicionesFront[secuenciaPosicionesFrontActiva].posicionActual;
		cout << "posicion indice abajo front = " << posActual << endl;
		secuenciasPosicionesFront[secuenciaPosicionesFrontActiva].posicionActual++;
		if (secuenciasPosicionesFront[secuenciaPosicionesFrontActiva].posicionActual >= 108)
		{
			cout << "ERROR: pidiendo una posicion > 108" << endl;
			secuenciasPosicionesFront[secuenciaPosicionesFrontActiva].posicionActual = 107;
		}
		//cout << "nueva pos abajo Front index= " << posActual << endl;

		int pos = secuenciasPosicionesFront[secuenciaPosicionesFrontActiva].posiciones[posActual];
		if (pos == -1)
			return 107;
		else
			return pos;
	}

	cout << "no ha encontrado posicion abajo... no deberia ocurrir NENG " << endl;
	return 0;

}

//=============================================
//
int ofApp::buscaPosicionNuevoBrickArriba()
{
	//for (int i = 0; i < NUM_POSICIONES_GRID; i++)
	//	if (posicionesGrid[i].tipoBrick == -1)
	//		return i;

	if (brickCounterArriba >= 0 && brickCounterArriba <= 8)
		return posicionesArriba[brickCounterArriba];
	else
	{
		cout << "ERROR, ya hay 9 bricks arriba, neng!!!" << endl;
		return -1;
	}
}


//=============================================
void ofApp::drawGrid(){
   // ofSetColor(255, 100);
   // plantilla.draw(0,0);
    
 //   for(int i = 0; i < PANTALLA_LED_WIDTH; i = i + ancho){
 //       ofSetColor(255);
 //       ofDrawLine(i, 0, i, PANTALLA_LED_HEIGHT);
 //   }
 //   
 //   for(int j = 0; j < PANTALLA_LED_HEIGHT; j = j + ancho){
 //       ofDrawLine(0,j, PANTALLA_LED_WIDTH, j);
 //   }
	//for (int i = 0; i < NUM_POSICIONES_GRID; i++)
	//	ofDrawBitmapString(ofToString(i).c_str(), posicionesGrid[i].pos);

}
//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
   
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
   
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
