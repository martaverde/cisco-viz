#include "KinectPillar.h"

void KinectPillar::setup(int width, int height, vector<Brick> *bricks, GRID_POSICIONES *_posicionesGrid)
{
	this->width = width;
	this->height = height;
	this->bricks = bricks;
	this->posicionesGrid = _posicionesGrid;

	gui.setup("kinect","",300,10);
	gui.add(paramBinPuntosThreshold.setup("k bin threshold", 10, 1, 336));
	gui.add(paramBinPuntosMin.setup("k bin pt min", 10, 1, 336));
	gui.add(paramBinPuntosMax.setup("k bin pt max", 10, 1, 336));
	gui.add(paramBinAlphaMin.setup("k bin alpha min", 10, 0, 255));
	gui.add(paramBinAlphaMax.setup("k bin alpha max", 10, 0, 255));
	gui.add(paramBricksAlphaKinect.setup("bricks alpha kinect", 100, 1, 255));
	gui.add(paramBricksAlphaDamp.setup("bricks alpha damp", 0.1f, 0.01f, 0.9f));
	gui.add(paramThersholdGente.setup("threshold gente", 1, 1, 300));
	gui.add(paramAlphaTransicionGente.setup("alpha damp Gente" , 0.1f, 0.01f, 0.9f));

	gui.loadFromFile("kinect.xml");
	
	float ancho = (float)width / (float)NUM_COLUMNAS;
	float alto = (float)height / (float)NUM_FILAS;

	for (int i = 0; i < NUM_COLUMNAS; i++)
		for (int k = 0; k < NUM_FILAS; k++)
		{
			ofRectangle r;
			r.x = i * ancho;
			r.y = k * alto;
			r.width = ancho;
			r.height = alto;
			bins[k][i].r = r;
		}


	for (int i = 0; i < NUM_COLUMNAS*3; i++)
		for (int k = 0; k < NUM_FILAS*3; k++)
		{
			ofRectangle r;
			r.x = i * (ancho/3.0f);
			r.y = k * (alto/3.0f);
			r.width = (ancho/3.0f);
			r.height = (alto/3.0f);
			bins3[k][i].r = r;
		}

	fboKinect.allocate(width, height, GL_RGBA);
	//imgKinect.allocate(width/2, height/2, OF_IMAGE_COLOR_ALPHA);

}


void KinectPillar::update( ofImage &_img)
{
	imgKinect = _img;
	
	bHayAlguien = false;
	int contadorGente = 0;

	for (int i = 0; i < NUM_COLUMNAS; i++)
		for (int k = 0; k < NUM_FILAS; k++)
			bins[k][i].alpha = 0;

	for (int i = 0; i < NUM_COLUMNAS*3; i++)
		for (int k = 0; k < NUM_FILAS*3; k++)
			bins3[k][i].alpha = 0;


	for (int y = 0 ; y < imgKinect.getHeight() ; y=y+2)
		for (int x = 0; x < imgKinect.getWidth(); x=x+2)
		{
			if (imgKinect.getColor(x, y).r > 10 || imgKinect.getColor(x, y).g > 10 || imgKinect.getColor(x, y).b > 10)
			{
				int binDestinoX = ofMap(x, 0, imgKinect.getWidth()- 1, 0, NUM_COLUMNAS, true);
				int binDestinoY = ofMap(y, 0, imgKinect.getHeight() - 1, 0, NUM_FILAS, true);
				if (binDestinoY >= 0 && binDestinoY < NUM_FILAS && binDestinoX >= 0 && binDestinoX < NUM_COLUMNAS)
					bins[binDestinoY][binDestinoX].alpha += 1;
				
				int binDestinoX3 = ofMap(x, 0, imgKinect.getWidth() - 1, 0, NUM_COLUMNAS*3, true);
				int binDestinoY3 = ofMap(y, 0, imgKinect.getHeight() - 1, 0, NUM_FILAS*3, true);
				if (binDestinoY3 >= 0 && binDestinoY3 < NUM_FILAS*3 && binDestinoX3 >= 0 && binDestinoX3 < NUM_COLUMNAS*3)
					bins3[binDestinoY3][binDestinoX3].alpha += 1;
				//cout << "kinect width = " << imgKinect->getWidth() << ", x = " << x << "," << binDestinoX << "," << binDestinoY << " = bin" << endl;
			}
		}

	fboKinect.begin();
		ofClear(0, 0);
		//for (int x = 0 ; x < NUM_COLUMNAS ; x++)
		//	for (int y = 0; y < NUM_FILAS; y++)
		//	{
		//		if (bins[y][x].alpha > 20)
		//			ofDrawRectangle(bins[y][x].r);
		//	}
		for (int x = 0; x < NUM_COLUMNAS*3; x++)
			for (int y = 0; y < NUM_FILAS*3; y++)
			{
				if (bins3[y][x].alpha > paramBinPuntosThreshold)
				{
					ofSetColor(255, 255, 255,ofMap(bins3[y][x].alpha,paramBinPuntosMin, paramBinPuntosMax, paramBinAlphaMin, paramBinAlphaMax,true));
					ofDrawRectangle(bins3[y][x].r);
					contadorGente++;
					//cout << bins3[y][x].r << endl;
				}
			}
		//ofSetColor(255, 0, 0, 200);
		//imgKinect.draw(0, 0, width, height);
	fboKinect.end();

	if (contadorGente > paramThersholdGente)
		bHayAlguien = true;

	for (int i = 0; i < bricks->size(); i++)
	{
		if (bHayAlguien)
			bricks->at(i).setAlphaTarget(paramBricksAlphaKinect, paramBricksAlphaDamp);
		else
			bricks->at(i).setAlphaTarget(255, paramAlphaTransicionGente);
	}

	for (int x = 0; x < NUM_COLUMNAS; x++)
		for (int y = 0; y < NUM_FILAS; y++)
		{
			if (bins[y][x].alpha > paramBinPuntosThreshold)
			{
				//hay que buscar el brick que est� en esa posicion
				ofVec2f pos = posicionesGrid[y*NUM_COLUMNAS + x].pos;
				for (int i = 0; i < bricks->size(); i++)
				{
					if (bricks->at(i).getPos() == pos)
						bricks->at(i).setAlphaTarget(255.0, paramBricksAlphaDamp);
				}
			}
		}

}

void KinectPillar::draw(float x , float y)
{
	//ofSetColor(255);
	fboKinect.draw(x, y);
	
	if (bGuiVisible)
		gui.draw();
}

void KinectPillar::save()
{
	gui.saveToFile("kinect.xml");
}

void KinectPillar::setGuiVisible(bool visible)
{
	bGuiVisible = visible;
}