#pragma once

#include "ofxOsc.h"
#include "ofxXmlSettings.h"


//#define LOCAL_PORT 12345
//#define REMOTE_IP "localhost"
//#define REMOTE_PORT 20001

class oscManager{

public:

	~oscManager(void);

    void	update();
	bool	isNewBrick( int &tipoBrick );
    // Receive OSC
    void receiveMessages();
    
   // Send OSC
    void sendResponse(string response);
    void sendStatePercentage(float percent);
    void sendStateIsFinished();


	static oscManager * getInstance() {
		if (myInstance.get() == nullptr) {
			myInstance = std::unique_ptr<oscManager>(new oscManager);
			return static_cast <oscManager *>(myInstance.get());
		}


		return myInstance.get();
	}

	static oscManager & get_fucking_instance() {
		return *getInstance();
	}

private:
	oscManager(void);
	static std::unique_ptr<oscManager> myInstance;
	int				tipoBrick;
	bool			bNuevoBrick = false;

	ofxOscReceiver	receiver;
	ofxOscSender	senderSM;
	ofxOscSender	senderCM;

	string			otherMsg;
};
