//
//  Brick.hpp
//  cisco_v1
//
//  Created by Marta Verde on 14/12/18.
//

//Settings Needed:

/*
 
Type of brick - > color front and back

animation:
Initial position
Final position
Cuando se está animando (boolean)
Brick id con settings guardados (guardar en XML?) (posicion FINAL)
*/


#ifndef Brick_h
#define Brick_h

#define DURACION_INSIDE 2000
#define SCALE_FBO 4.0f

#include "ofMain.h"
#include "ofxEasing2.h"

struct GRID_POSICIONES
{
	ofVec2f pos;
	int		tipoBrick; // -1 si no tiene ningún brick
};

class Brick {
    
	public:
		static ofEvent<int> evBrickStart;
		static ofEvent<int> evBrickStop;

		enum ANIMACION_BRICK { NONE, DELAY, CREACION, CREACION_TACTIL, ARRIBA_PILLAR, ABAJO_PILLAR, DUMP_PILLAR, DUMP_BRIDGE, FADE_INSIDE, RESCALE, GROW_STATUS};
		enum ESTADO_BRICK { STATIC, MOVING };
		enum ESTADO_INSIDE { INSIDE_OFF, INSIDE_IN, INSIDE_STANDBY, INSIDE_OUT};
		enum ANIMACIONES_INSIDE { ANIMACION_INSIDE_LEFT, ANIMACION_INSIDE_RIGHT, ANIMACION_INSIDE_UP, ANIMACION_INSIDE_DOWN};

		Brick();
		void			setup			( ofVec2f _Pos, int _type, int _id, int _animationTypeIn, int _animationTypeOut, float _currentTime );
		void			setScaleGlobal	( float _scale );
		void			update			( );
		void			draw			( );
		void			addAnimation	( ofVec2f _dest, ANIMACION_BRICK _tipoAnimacion, float duracion );
		void			addAnimationPart(ANIMACION_BRICK tipo, float duration, ofVec2f posIni, ofVec2f posFin);
		void			startAnimation	( );
		ofVec2f			getPos			( );
		ESTADO_BRICK	getEstado		( );
		int				getTipo			( );
		void			stopInside		( ); //se llama para que cuando llegue al centro el icono, no anime más
		void			startInside		( );
		void			drawInside		( );
		void			setTipo			( int tipo );
		void			setAlphaTarget	( float alpha, float dampAlpha) ;
		int				getDumpState	( );
		void			setDumpState	( int _dump );

	private:
		
		void			setupInterior	( );
		void			mueve			( );
		//void			createAnimation ( );
		void			updateInside	( );

		ESTADO_BRICK		estadoBrick;
		
		ESTADO_INSIDE		estadoInside;
		double				timeInsideStart;
		ANIMACIONES_INSIDE	tipoInside;

		bool				bNeedStopInside;
		ofVec2f				posInside;
		float				scaleInside;
		float				insideDelay;

		struct ANIMACION {
			double			timeIni;
			float			duration;
			ANIMACION_BRICK	tipo;
			ofVec2f			posIni;
			ofVec2f			posFin;
			float			scaleIni;
		};

		vector<ANIMACION>	animaciones;	//por donde tiene que pasar durante la animación

		//bool movingBricks;
    
		ofVec2f posBrick;
		float  dim;
		
		ofColor backColor;
		ofColor frontColor;
    
		int brick_id;
		int type;
    
		ofColor green ;
		ofColor red ;
		ofColor yellow ;
		ofColor blue;
    
		ofColor darkGreen;
		ofColor darkRed;
		ofColor darkBlue;
		ofColor darkYellow;
    
		void simpleTriangle(float x, float y, float w, float h);
		void simpleHexagon(float x, float y, float gs);
		void simpleDiamond(float x, float y, float width, float height);
    
		//ofFbo singleBrick;
		ofFbo fboScaled;
		float scaleFactorGlobal;
    
		float alphaTarget = 255.0f;
		float alphaActual = 255.0f;
		float alphaDamp = 0.1f;

		bool bAnimateInside = false;
		int dumpState = false;
};

#endif



