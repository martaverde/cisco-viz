#include "oscManager.h"

//--------------------------------------------------------------
/*
void oscManager::setup(int localPort, string remoteIp, int remotePort){
    
    // Setup Receiver
    receiver.setup(localPort);
    
    // Setup Sender
    sender.setup(remoteIp, remotePort);
    
    startState = false;
}*/


std::unique_ptr<oscManager> oscManager::myInstance = std::unique_ptr<oscManager>(nullptr);


oscManager::oscManager(void) {
	 XML.loadFile("mySettings.xml");

	 int LOCAL_PORT = XML.getValue("LOCAL_PORT",0);
	 string REMOTE_IP_SM = XML.getValue("REMOTE_IP_SM","");
	 int REMOTE_PORT_SM = XML.getValue("REMOTE_PORT_SM",0);

	 cout << "listening for osc messages on port " << LOCAL_PORT << "\n";
	// Setup Receiver
	receiver.setup(LOCAL_PORT);

	// Setup Sender
	sender.setup(REMOTE_IP, REMOTE_PORT);

}


oscManager::~oscManager(void) {

}

//--------------------------------------------------------------
void oscManager::update(){
    // Receive messages
    receiveMessages();
}

//--------------------------------------------------------------
void oscManager::receiveMessages(){
    
    // check for waiting messages
    while(receiver.hasWaitingMessages()){
        // get the next message
        ofxOscMessage m;
        receiver.getNextMessage(m);
        
        if(m.getAddress() == "/new_brick"){
            tipoBrick = m.getArgAsInt32(0);
			bNuevoBrick = true;
        }
        else if(m.getAddress() == "/state_name"){
            //stateName = m.getArgAsString(0);
            //sendResponse("ok_next_state");
        }
        else{
            string msg_string;
            msg_string = m.getAddress();
            msg_string += ": ";
            for(int i = 0; i < m.getNumArgs(); i++){
                // get the argument type
                msg_string += m.getArgTypeName(i);
                msg_string += ":";
                // display the argument - make sure we get the right type
                if(m.getArgType(i) == OFXOSC_TYPE_INT32){
                    msg_string += ofToString(m.getArgAsInt32(i));
                }
                else if(m.getArgType(i) == OFXOSC_TYPE_FLOAT){
                    msg_string += ofToString(m.getArgAsFloat(i));
                }
                else if(m.getArgType(i) == OFXOSC_TYPE_STRING){
                    msg_string += m.getArgAsString(i);
                }
                else{
                    msg_string += "unknown";
                }
            }
            otherMsg = msg_string;
        }
    }
    
}


//--------------------------------------------------------------
//devuelve true si hay un nuevo brick
//
bool oscManager::isNewBrick( int &_tipoBrick ) {
	
	if (bNuevoBrick)
	{
		_tipoBrick = tipoBrick;
		bNuevoBrick = false;
		return true;
	}
	
	return false;

}

//--------------------------------------------------------------
void oscManager::sendResponse(string response){
    ofxOscMessage m;
    m.setAddress("/response");
    m.addStringArg(response);
    sender.sendMessage(m);
}

//--------------------------------------------------------------
void oscManager::sendStatePercentage(float percent){
    ofxOscMessage m;
    m.setAddress("/state_percentage");
    m.addFloatArg(percent);
    sender.sendMessage(m);
}

//--------------------------------------------------------------
void oscManager::sendStateIsFinished(){
    ofxOscMessage m;
    m.setAddress("/state_finished");
    sender.sendMessage(m);
}

