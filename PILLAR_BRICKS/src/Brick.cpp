//
//  Brick.cpp
//  cisco_v1
//
//  Created by Marta Verde on 14/12/18.
//

#include "Brick.h"

ofEvent<int> Brick::evBrickStart = ofEvent<int>();
ofEvent<int> Brick::evBrickStop = ofEvent<int>();

/*
 to-do
 definir animaciones 8 TIPOS in 8 tipos OUT
 get timestamp de colocación OK
 definir tiempos variables random de rangos de animación (de 2 waits) 
 añadir initPoint de animación para poder cargar en el RECAP. VACIO O POSICIONADO.
*/

Brick::Brick(){
   
}

void Brick::setup(ofVec2f _Pos, int _type, int _id, int _animationTypeIn, int _animationTypeOut, float _currentTime){
  
	posBrick.x =  _Pos.x;
	posBrick.y =  _Pos.y;
    dim = 56;
    type = _type;
    brick_id = _id;
   
    green = ofColor(107, 189, 82);
    red = ofColor(221, 42, 39);
    yellow = ofColor(242, 166, 40);
    blue = ofColor(27, 168, 223);
    
    darkGreen = ofColor(34, 92, 58);
    darkRed = ofColor(150, 25, 33);
    darkBlue = ofColor(22, 73, 108);
    darkYellow = ofColor(221, 42, 39);
    
    //singleBrick.allocate(dim , dim , GL_RGBA32F);
	fboScaled.allocate(dim * SCALE_FBO, dim * SCALE_FBO, GL_RGBA32F);

    scaleFactorGlobal = 1.0f;
	scaleInside = 1.0f;
	estadoBrick = ESTADO_BRICK::STATIC;
	 
	setupInterior();
	insideDelay = ofRandom(0, 1000);
	 
	estadoInside = ESTADO_INSIDE::INSIDE_OFF;
	alphaActual = 255;
	alphaTarget = 255;
	alphaDamp = 1.0f;
}
void Brick::setScaleGlobal( float _scale )
{
	scaleFactorGlobal = _scale;
	scaleInside = _scale;
}

void Brick::setupInterior()
{

	if (type == 0 || type == 1 || type == 2) {
		backColor = yellow;
		frontColor = darkYellow;
	}

	if (type == 3 || type == 4 || type == 5) {
		backColor = red;
		frontColor = darkRed;
	}

	if (type == 6 || type == 7 || type == 8) {
		backColor = green;
		frontColor = darkGreen;
	}

	if (type == 9) {
		backColor = blue;
		frontColor = darkBlue;
	}

	drawInside();


}


//void Brick::drawInside()
//{
//
//	singleBrick.begin();
//	ofClear(0, 0);
//	ofPushMatrix();
//	//centrado dim/2 dim/2
//	float posX = dim / 2;
//	float posY = dim / 2;
//	ofTranslate(posX, posY);
//
//	//ofScale(scaleFactor, scaleFactor);
//	float scaleFactor = 1.0f;
//	ofSetColor(frontColor, 255 * ofMap(scaleFactor, 1, 3, 1, 0));
//
//	//simpleHexagon(0 + posInside.x, 0 + posInside.y, (dim / 2.85));
//	
//	if (type == 0)   simpleTriangle(0+posInside.x, 5 + posInside.y, (dim / 1.25), (dim / 1.25));
//	if (type == 1)   simpleHexagon(0 + posInside.x, 0 + posInside.y, (dim / 2.85));
//
//	if (type == 2)   simpleDiamond(0 + posInside.x, 0 + posInside.y, (dim / 2) + 10, (dim / 2) + 5);
//	if (type == 3)   simpleHexagon(0 + posInside.x, 0 + posInside.y, (dim / 2.85));
//
//	if (type == 4)   simpleDiamond(0 + posInside.x, 0 + posInside.y, (dim / 2) + 10, (dim / 2) + 5);
//	if (type == 5)   simpleTriangle(0 + posInside.x, 5 + posInside.y, (dim / 1.25), (dim / 1.25));
//
//	if (type == 6)   simpleDiamond(0 + posInside.x, 0 + posInside.y, (dim / 2) + 10, (dim / 2) + 5);
//	if (type == 7)   simpleTriangle(0 + posInside.x, 5 + posInside.y, dim / 1.25, dim / 1.25);
//	if (type == 8)   simpleHexagon(0 + posInside.x, 0 + posInside.y, dim / 2.85);
//	if (type == 9)   ofDrawCircle(0 + posInside.x, 0 + posInside.y, dim / 3.2f);
//	
//	ofPopMatrix();
//	singleBrick.end();
//}


void Brick::drawInside()
{

	fboScaled.begin();
		ofClear(backColor.r,backColor.g,backColor.b, 255);
		//ofClear(0, 255);
		ofPushMatrix();
		//centrado dim/2 dim/2
		float posX = dim/2;
		float posY = dim/2;
		posX = posX * SCALE_FBO;
		posY = posY * SCALE_FBO;
		ofTranslate(posX, posY);

		//ofScale(scaleFactor, scaleFactor);
		float scaleFactor = 1.0f;
		ofSetColor(frontColor, 255 * ofMap(scaleFactor, 1, 3, 1, 0));

		//simpleHexagon(0 + posInside.x, 0 + posInside.y, (dim / 2.85));
		float dimS = dim * SCALE_FBO;
		if (type == 0)   simpleTriangle(0 + posInside.x, (dimS / 10.0f) + posInside.y, (dimS / 1.25), (dimS / 1.25));
		if (type == 1)   simpleHexagon(0 + posInside.x, 0 + posInside.y, (dimS / 2.85));

		if (type == 2)   simpleDiamond(0 + posInside.x, 0 + posInside.y, (dimS / 2) + (dimS / 5.0f), (dimS / 2) + (dimS / 10.0f));
		if (type == 3)   simpleHexagon(0 + posInside.x, 0 + posInside.y, (dimS / 2.85));

		if (type == 4)   simpleDiamond(0 + posInside.x, 0 + posInside.y, (dimS / 2) + (dimS / 5.0f), (dimS / 2) + (dimS / 10.0f));
		if (type == 5)   simpleTriangle(0 + posInside.x, (dimS / 10.0f) + posInside.y, (dimS / 1.25), (dimS / 1.25));

		if (type == 6)   simpleDiamond(0 + posInside.x, 0 + posInside.y, (dimS / 2) + (dimS / 5.0f), (dimS / 2) + (dimS / 10.0f));
		if (type == 7)   simpleTriangle(0 + posInside.x, (dimS/10.0f) + posInside.y, dimS / 1.25, dimS / 1.25);
		if (type == 8)   simpleHexagon(0 + posInside.x, 0 + posInside.y, dimS / 2.85);
		if (type == 9)   ofDrawCircle(0 + posInside.x, 0 + posInside.y, dimS / 3.3f);

		ofPopMatrix();
	fboScaled.end();

	//singleBrick.begin();
	//	ofClear(0, 0);
	//	ofSetColor(255);
	//	fboScaled.draw(0, 0, dim, dim);
	//singleBrick.end();

}


void Brick::startInside() {
	
	bNeedStopInside = false;
	timeInsideStart = ofGetElapsedTimeMillis();
	tipoInside = (Brick::ANIMACIONES_INSIDE)(int)ofRandom(4);
	estadoInside = ESTADO_INSIDE::INSIDE_STANDBY;
	bAnimateInside = true;

}

void Brick::updateInside() {
	
	ofVec2f ini;
	ofVec2f fin;
	float tActual = ofGetElapsedTimeMillis() - timeInsideStart;

	if (tActual >= DURACION_INSIDE + insideDelay)
	{
		timeInsideStart = ofGetElapsedTimeMillis();
		if (estadoInside == ESTADO_INSIDE::INSIDE_IN)
		{
			estadoInside = ESTADO_INSIDE::INSIDE_STANDBY;
			return;
		}
		if (estadoInside == ESTADO_INSIDE::INSIDE_OUT)
		{
			estadoInside = ESTADO_INSIDE::INSIDE_IN;
			tipoInside = (Brick::ANIMACIONES_INSIDE)(int)ofRandom(4);
			return;
		}
		if (estadoInside == ESTADO_INSIDE::INSIDE_STANDBY && bNeedStopInside == false)
		{
			estadoInside = ESTADO_INSIDE::INSIDE_OUT;
			tipoInside = (Brick::ANIMACIONES_INSIDE)(int)ofRandom(4);
			return;
		}
		// hay que parar la aninacion cuando llegue al centro (standby)
		if (estadoInside == ESTADO_INSIDE::INSIDE_STANDBY && bNeedStopInside == true)
		{
			estadoInside = ESTADO_INSIDE::INSIDE_OFF;
			bAnimateInside = false;
			return;
		}
	}
	float scale = dim * SCALE_FBO;
	if (estadoInside == ESTADO_INSIDE::INSIDE_IN)
	{
		if (tipoInside == ANIMACIONES_INSIDE::ANIMACION_INSIDE_LEFT)
			ini = ofVec2f(-scale * 2, 0);
		if (tipoInside == ANIMACIONES_INSIDE::ANIMACION_INSIDE_RIGHT)
			ini = ofVec2f(scale * 2, 0);
		if (tipoInside == ANIMACIONES_INSIDE::ANIMACION_INSIDE_UP)
			ini = ofVec2f(0, -scale * 2);
		if (tipoInside == ANIMACIONES_INSIDE::ANIMACION_INSIDE_DOWN)
			ini = ofVec2f(0 , scale * 2);
		
		fin = ofVec2f(0, 0);

		posInside.x = ofxeasing2::map(tActual, 0, DURACION_INSIDE + insideDelay, ini.x, fin.x, &ofxeasing2::circ::easeOut);
		posInside.y = ofxeasing2::map(tActual, 0, DURACION_INSIDE + insideDelay, ini.y, fin.y, &ofxeasing2::circ::easeOut);

	}

	if (estadoInside == ESTADO_INSIDE::INSIDE_OUT)
	{
		ini = ofVec2f(0, 0);

		if (tipoInside == ANIMACIONES_INSIDE::ANIMACION_INSIDE_LEFT)
			fin = ofVec2f(-scale * 2, 0);
		if (tipoInside == ANIMACIONES_INSIDE::ANIMACION_INSIDE_RIGHT)
			fin = ofVec2f(scale * 2, 0);
		if (tipoInside == ANIMACIONES_INSIDE::ANIMACION_INSIDE_UP)
			fin = ofVec2f(0, -scale * 2);
		if (tipoInside == ANIMACIONES_INSIDE::ANIMACION_INSIDE_DOWN)
			fin = ofVec2f(0, scale * 2);
		posInside.x = ofxeasing2::map(tActual, 0, DURACION_INSIDE + insideDelay, ini.x, fin.x, &ofxeasing2::circ::easeIn);
		posInside.y = ofxeasing2::map(tActual, 0, DURACION_INSIDE + insideDelay, ini.y, fin.y, &ofxeasing2::circ::easeIn);
	}

}

void Brick::stopInside() {

	bNeedStopInside = true;
}


void Brick::update() {

	alphaActual = (alphaDamp * alphaTarget) + (1.0 - alphaDamp) * alphaActual;
	//cout << alphaActual << endl;
	if (estadoBrick == ESTADO_BRICK::MOVING)
		mueve();
	
	if (bAnimateInside)
		updateInside();
	
	if (estadoBrick == ESTADO_BRICK::MOVING)
	{
		if (animaciones.size() > 0)
			if (animaciones.front().tipo == ANIMACION_BRICK::DELAY)
				updateInside();
	}

	drawInside();

	/*
	if (type == 0 || type == 1 || type == 2) {
		backColor = yellow;
		frontColor = darkYellow;
	}

	if (type == 3 || type == 4 || type == 5) {
		backColor = red;
		frontColor = darkRed;
	}

	if (type == 6 || type == 7 || type == 8) {
		backColor = green;
		frontColor = darkGreen;
	}

	if (type == 9) {
		backColor = blue;
		frontColor = darkBlue;
	}
	
	//ICON FBO
	singleBrick.begin();
		ofClear(0, 0);
		ofPushMatrix();
		//centrado dim/2 dim/2
		posX = dim / 2;
		posY = dim / 2;
		ofTranslate(posX, posY);

		ofScale(scaleFactor, scaleFactor);
		ofSetColor(frontColor, 255 * ofMap(scaleFactor, 1, 3, 1, 0));

		if (type == 0)   simpleTriangle(0, 5, (dim / 1.25), (dim / 1.25));
		if (type == 1)   simpleHexagon(0, 0, (dim / 2.85));

		if (type == 2)   simpleDiamond(0, 0, (dim / 2) + 10, (dim / 2) + 5);
		if (type == 3)   simpleHexagon(0, 0, (dim / 2.85));

		if (type == 4)   simpleDiamond(0, 0, (dim / 2) + 10, (dim / 2) + 5);
		if (type == 5)   simpleTriangle(0, 5, (dim / 1.25), (dim / 1.25));

		if (type == 6)   simpleDiamond(0, 0, (dim / 2) + 10, (dim / 2) + 5);
		if (type == 7)   simpleTriangle(0, 5, dim / 1.25, dim / 1.25);
		if (type == 8)   simpleHexagon(0, 0, dim / 2.85);
		if (type == 9)   ofDrawEllipse(0, 0, dim / 1.6, dim / 1.6);

		ofSetColor(255);
		//ofDrawBitmapString(brick_id, 0, 0);

		ofPopMatrix();
	singleBrick.end();
	*/
}

void Brick::mueve()
{
	if (animaciones.size() == 0)
		return;

	float animationStart = animaciones.front().timeIni;
	ofVec2f dest = animaciones.front().posFin;
	ofVec2f iniPointAnimation = animaciones.front().posIni;
	float animationDuration = animaciones.front().duration;

	ofVec2f newPos;
	if (animaciones.front().tipo == ANIMACION_BRICK::CREACION_TACTIL)
	{
		if (iniPointAnimation.x == dest.x)
		{
			newPos.y = ofxeasing2::map(ofGetElapsedTimeMillis() - animationStart, 0, animationDuration, iniPointAnimation.y, dest.y, &ofxeasing2::circ::easeOut);
			//cout << "timeIni = " << animationStart << ", actual= " << ofGetElapsedTimeMillis() << ", relativo = " << ofGetElapsedTimeMillis() - animationStart << "Valor = " << newPos.y << endl;
			newPos.x = posBrick.x;
		}
		if (iniPointAnimation.y == dest.y)
		{
			newPos.x = ofxeasing2::map(ofGetElapsedTimeMillis() - animationStart, 0, animationDuration, iniPointAnimation.x, dest.x, &ofxeasing2::circ::easeOut);
			newPos.y = posBrick.y;
		}
	}
	else
	{
		if (iniPointAnimation.x == dest.x)
		{
			newPos.y = ofxeasing2::map(ofGetElapsedTimeMillis() - animationStart, 0, animationDuration, iniPointAnimation.y, dest.y, &ofxeasing2::circ::easeInOut);
			//cout << "timeIni = " << animationStart << ", actual= " << ofGetElapsedTimeMillis() << ", relativo = " << ofGetElapsedTimeMillis() - animationStart << "Valor = " << newPos.y << endl;
			newPos.x = posBrick.x;
		}
		if (iniPointAnimation.y == dest.y)
		{
			newPos.x = ofxeasing2::map(ofGetElapsedTimeMillis() - animationStart, 0, animationDuration, iniPointAnimation.x, dest.x, &ofxeasing2::circ::easeInOut);
			newPos.y = posBrick.y;
		}
	}


	posBrick = newPos;

	//if (posBrick == dest)

	if (ofGetElapsedTimeMillis() - animationStart >= animationDuration)
	{
		//cout << "terminada animacion de tipo " << animaciones.front().tipo << endl;
		posBrick = dest;
		animaciones.erase(animaciones.begin());
		int v = 1;
		if (animaciones.size() == 0)
		{
			estadoBrick = ESTADO_BRICK::STATIC;
			ofNotifyEvent(evBrickStop,v);
		}
		else
		{
			animaciones.front().timeIni = ofGetElapsedTimeMillis();
			animaciones.front().posIni = dest;  //ojo
			//ofNotifyEvent(evBrickStart, v);

		}
	}
}

ofVec2f Brick::getPos() {
	return posBrick;
}

Brick::ESTADO_BRICK Brick::getEstado() {

	return estadoBrick;

}

int Brick::getTipo() {

	return type;

}

void Brick::setTipo(int _tipo) {

	type = _tipo;

}

void Brick::draw(){
    
	ofSetRectMode(OF_RECTMODE_CENTER);
 //   if (estadoBrick == ESTADO_BRICK::MOVING)
	//	ofSetColor(backColor,255);
	//else
		ofSetColor(backColor, alphaActual);
	float offsetY = 0;
	float offsetYinterior = 0;
	


	float posYinterior = posBrick.y;

	if (estadoBrick == ESTADO_BRICK::MOVING)
	{
		float animationStart = animaciones.front().timeIni;
		ANIMACION_BRICK tipoAnimacion = animaciones.front().tipo;
		ofVec2f dest = animaciones.front().posFin;
		ofVec2f iniPointAnimation = animaciones.front().posIni;
		float animationDuration = animaciones.front().duration;
		float scaleIni = animaciones.front().scaleIni;

		float actual = ofGetElapsedTimeMillis() - animationStart;
		
		if (tipoAnimacion == ANIMACION_BRICK::ARRIBA_PILLAR)
		{
			if (iniPointAnimation.y != dest.y)
			{
				float size = 10;
//				if (actual > animationDuration * 0.3f)
//					offsetYinterior = ofxeasing::map(actual, animationDuration * 0.3f, animationDuration, 40, 0, &ofxeasing::cubic::easeInOut);
				//cout << "offset interior:" << offsetYinterior << ",start = " << animationStart << ", actual=" << actual << ", duration=" << animationDuration << endl;
				//offsetY = ofxeasing::map(actual, 0, animationDuration, 0, size, &ofxeasing::quad::easeIn);
				//if (actual > animationDuration * 0.3f)
				//	offsetY = ofxeasing2::map(actual, animationDuration * 0.3f, animationDuration, size, 0, &ofxeasing2::quad::easeIn);
					//offsetY = size - offsetY;
				
				//offsetYinterior = ofxeasing::map(actual, animationDuration * 0.3f, animationDuration, size, 0, &ofxeasing::quad::easeInOut);
				//posYinterior = ofxeasing2::map(actual, 0, animationDuration, iniPointAnimation.y, dest.y, &ofxeasing2::linear::easeInOut);

			}
			//ofDrawRectangle(posBrick.x, posBrick.y, dim, dim );
			//ofDrawRectangle(posBrick.x, posBrick.y + (offsetY / 2.0f), dim, dim + offsetY);
			//ofDrawRectangle(posBrick.x, posBrick.y, dim, dim);
			ofSetColor(255);
			//singleBrick.draw(posBrick.x, posBrick.y + offsetYinterior);
			//fboScaled.draw(posBrick.x, posYinterior,dim,dim);
			fboScaled.draw(posBrick.x, posBrick.y, dim, dim);
		}
		else if (tipoAnimacion == ANIMACION_BRICK::ABAJO_PILLAR)
		{
			ofVec2f medio = iniPointAnimation.getInterpolated(posBrick, 0.5f);
			//ofVec2f medio = posBrick + (posBrick - iniPointAnimation) / 2.0f;
			if (iniPointAnimation.y != posBrick.y)
			{
				//if (actual > animationDuration * 0.1f)
				//	offsetYinterior = ofxeasing2::map(actual, animationDuration * 0.1f, animationDuration, 40, 0, &ofxeasing2::cubic::easeIn);

				if (actual > animationDuration * 0.8f)
				{
					float posY = ofMap(actual, animationDuration * 0.8f, animationDuration, iniPointAnimation.y, posBrick.y, true);
					//ofDrawRectangle(medio.x, posBrick.y - dist/2.0f, dim, (posBrick.distance(iniPointAnimation) + dim) - dist);
					ofSetRectMode(OF_RECTMODE_CORNER);
					ofRectangle r;
					r.x = posBrick.x - dim/2;
					r.y = posY - dim /2 ;
					r.width=dim;
					r.setHeight((posBrick.y - posY));
					ofDrawRectangle(r);
					ofSetRectMode(OF_RECTMODE_CENTER);

				}
				else
					ofDrawRectangle(medio.x, medio.y, dim, posBrick.distance(iniPointAnimation) + dim);
			}
			else
			{
				if (actual > animationDuration * 0.8f)
				{
					//float posX = ofMap(actual, animationDuration * 0.5f, animationDuration, iniPointAnimation.x, posBrick.x, true);
					float posX = ofxeasing2::map(actual, animationDuration * 0.8f, animationDuration, iniPointAnimation.x, posBrick.x, &ofxeasing2::circ::easeOut);
					//ofDrawRectangle(medio.x, posBrick.y - dist/2.0f, dim, (posBrick.distance(iniPointAnimation) + dim) - dist);
					ofSetRectMode(OF_RECTMODE_CORNER);
					ofRectangle r;
					r.x = posX - dim / 2;
					r.y = posBrick.y - dim / 2;
					//r.y = posBrick.y;
					r.width = (posBrick.x - posX);
					r.setHeight(dim);
					ofDrawRectangle(r);
					ofSetRectMode(OF_RECTMODE_CENTER);
					//ofDrawRectangle(r);
				}
				else
					ofDrawRectangle(medio.x, medio.y, posBrick.distance(iniPointAnimation) + dim, dim);
			}

			//ofDrawRectangle(posBrick.x, posBrick.y, dim, dim);
			ofSetColor(255);
			//fboScaled.draw(posBrick.x, posBrick.y + offsetYinterior,dim,dim);
			fboScaled.draw(posBrick.x, posBrick.y, dim, dim);

		}
		else if (tipoAnimacion == ANIMACION_BRICK::CREACION)
		{
			////if (actual < 500)
			//{
			//	float scale = ofxeasing2::map_clamp(actual, 0, animationDuration, 0, 1, &ofxeasing2::elastic::easeOut);
			//	scale = scale * scaleFactorGlobal;
			//	ofDrawRectangle(posBrick.x, posBrick.y, dim * scale, dim * scale);
			//}
			//if (actual > 500)
			//{
			//	//ofDrawRectangle(posBrick.x, posBrick.y, dim , dim );
			//	float scaleIn = ofxeasing2::map_clamp(actual, 500, animationDuration, 0, 1, &ofxeasing2::elastic::easeOut);
			//	ofSetColor(255);
			//	fboScaled.draw(posBrick.x, posBrick.y,scaleIn * scaleFactorGlobal * dim, scaleIn * scaleFactorGlobal * dim);
			//}
			
			float scale = ofxeasing2::map(actual, 0, animationDuration, 0, 1, &ofxeasing2::elastic::easeOut);
			scale = scale * scaleFactorGlobal;
			ofDrawRectangle(posBrick.x, posBrick.y, dim * scale, dim * scale);
			if (actual > 1000)
			{
				float scaleIn = ofxeasing2::map_clamp(actual, 1000, animationDuration-50, 0, 1, &ofxeasing2::circ::easeInOut);
				ofSetColor(255);
				fboScaled.draw(posBrick.x, posBrick.y, scaleIn * scaleFactorGlobal * dim, scaleIn * scaleFactorGlobal * dim);
			}
		}
		else if (tipoAnimacion == ANIMACION_BRICK::GROW_STATUS)
		{
			ofDrawRectangle(posBrick.x, posBrick.y, dim , dim );
			float scaleIn = ofxeasing2::map(actual, 0, animationDuration, 0, 1, &ofxeasing2::circ::easeOut);
			ofSetColor(255);
			fboScaled.draw(posBrick.x, posBrick.y, scaleIn * scaleFactorGlobal * dim, scaleIn * scaleFactorGlobal * dim);
		}
		else if (tipoAnimacion == ANIMACION_BRICK::DELAY)
		{
			//ofDrawRectangle(posBrick.x, posBrick.y, dim, dim);
			ofSetColor(255);
			fboScaled.draw(posBrick.x, posBrick.y, scaleFactorGlobal * dim, scaleFactorGlobal * dim);
		}
		else if (tipoAnimacion == ANIMACION_BRICK::DUMP_PILLAR)
		{
			ofVec2f medio = iniPointAnimation.getInterpolated(posBrick, 0.5f);
			if (actual > animationDuration * 0.1f)
				offsetYinterior = ofxeasing2::map(actual, animationDuration * 0.1f, animationDuration, 40, 0, &ofxeasing2::cubic::easeIn);

			if (actual > animationDuration * 0.6f)
			{
				//float posY = ofMap(actual, animationDuration * 0.6f, animationDuration, iniPointAnimation.y, posBrick.y, true);
				float posY = ofxeasing2::map(actual, animationDuration * 0.6f, animationDuration, iniPointAnimation.y, posBrick.y, &ofxeasing2::cubic::easeIn);
				//ofDrawRectangle(medio.x, posBrick.y - dist/2.0f, dim, (posBrick.distance(iniPointAnimation) + dim) - dist);
				ofSetRectMode(OF_RECTMODE_CORNER);
				ofRectangle r;
				r.x = posBrick.x - dim / 2;
				r.y = posY - dim / 2;
				r.width = dim;
				r.setHeight(-(posBrick.y + posY));
				ofDrawRectangle(r);
				ofSetRectMode(OF_RECTMODE_CENTER);
			}
			else
				ofDrawRectangle(medio.x, medio.y, dim, posBrick.distance(iniPointAnimation) + dim);


			ofDrawRectangle(posBrick.x, posBrick.y, dim, dim);
			ofSetColor(255);
			fboScaled.draw(posBrick.x, posBrick.y + offsetYinterior,dim,dim);
		}
		else if (tipoAnimacion == ANIMACION_BRICK::FADE_INSIDE)
		{
			scaleInside = ofxeasing2::map(actual, 0, animationDuration, 1, 0, &ofxeasing2::elastic::easeOut);
			ofDrawRectangle(posBrick.x, posBrick.y, dim, dim);
			ofSetColor(255);
			fboScaled.draw(posBrick.x, posBrick.y, scaleInside * dim, scaleInside * dim);
		}
		else if (tipoAnimacion == ANIMACION_BRICK::RESCALE)
		{
			scaleFactorGlobal = ofxeasing2::map_clamp(actual, 0, animationDuration-100, scaleIni, 1.0f, &ofxeasing2::linear::easeOut);
			ofDrawRectangle(posBrick.x, posBrick.y, dim * scaleFactorGlobal, dim * scaleFactorGlobal);
			scaleInside = scaleFactorGlobal;
			ofSetColor(255);
			fboScaled.draw(posBrick.x, posBrick.y , scaleInside * dim, scaleInside * dim);
		}
	}
	else
	{
		ofEnableBlendMode(OF_BLENDMODE_DISABLED);
		ofDrawRectangle(posBrick.x, posBrick.y, dim * scaleFactorGlobal, dim* scaleFactorGlobal);
		ofEnableBlendMode(OF_BLENDMODE_ALPHA);
		ofSetColor(255, 255, 255, alphaActual);
		float sc = ofMap(alphaActual, 50, 255, 0, 1, true);
		//float sc = 1.0f;
		fboScaled.draw(posBrick.x, posBrick.y + offsetYinterior, scaleInside * dim * sc, scaleInside * dim *sc);

		//ofSetColor(255, 255, 255, alphaActual);
		//ofEnableBlendMode(OF_BLENDMODE_DISABLED);
		//ofDrawRectangle(posBrick.x, posBrick.y, dim * scaleFactorGlobal, dim* scaleFactorGlobal);
		//ofSetColor(255, 255, 255, alphaActual);
		//float sc = ofMap(alphaActual, 50, 255, 0, 1, true);
		//singleBrick.draw(posBrick.x, posBrick.y + offsetYinterior, scaleInside * dim * sc, scaleInside * dim *sc);
	}
	
	ofSetRectMode(OF_RECTMODE_CORNER);

}

int Brick::getDumpState()
{
	return dumpState;
}

void Brick::setDumpState(int _dump)
{
	this->dumpState = _dump;
}

void Brick::simpleDiamond(float x, float y, float width, float height){
    ofBeginShape();
        ofVertex(x-width/2,y);  //left
        ofVertex(x, y + height/2); //bottom
        ofVertex(x+width/2, y); //right
        ofVertex(x, y - height/2);
    ofEndShape();
}

void Brick::simpleHexagon(float x, float y, float gs){
    ofBeginShape();
    for(int i=0;i<6;i++){
        float angle = i * 2 * PI / 6;
        ofVertex(x + gs*cos(angle),y + gs*sin(angle));
    }
    ofEndShape();

/*	ofPolyline pol;
	for(int i=0;i<6;i++){
	    float angle = i * 2 * PI / 6;
	    pol.addVertex(x + gs*cos(angle),y + gs*sin(angle));
	}
	pol.setClosed(true);
	pol.draw()*/;
}

void Brick::simpleTriangle(float x, float y, float w, float h){
    float xArray[3];
    float yArray[3];
    
    float currentAngle = -HALF_PI;
    float wr = w * 0.5;
    float hr = h * 0.5;
    
    for (int i = 0; i < 3; i++) {
        xArray[i] = x + wr*cos(currentAngle);
        yArray[i] = y + hr*sin(currentAngle);
        
        currentAngle = currentAngle + ofDegToRad(360 / 3);
    }
    
    ofDrawTriangle(xArray[0], yArray[0],
                   xArray[1], yArray[1],
                   xArray[2], yArray[2]);
}


void Brick::addAnimation(ofVec2f _dest, ANIMACION_BRICK _tipoAnimacion, float duracion)
{
	//estadoBrick = ESTADO_BRICK::MOVING;
	
	if (_tipoAnimacion == ANIMACION_BRICK::CREACION)
		addAnimationPart(_tipoAnimacion, 1450, posBrick, _dest);

	if (_tipoAnimacion == ANIMACION_BRICK::CREACION_TACTIL)
	{
		addAnimationPart(ANIMACION_BRICK::ARRIBA_PILLAR, 1000, posBrick, _dest);
	}

	if (_tipoAnimacion == ANIMACION_BRICK::GROW_STATUS)
		addAnimationPart(_tipoAnimacion, 900, posBrick, _dest);

	if (_tipoAnimacion == ANIMACION_BRICK::FADE_INSIDE)
		addAnimationPart(_tipoAnimacion, duracion, posBrick, _dest);

	if (_tipoAnimacion == ANIMACION_BRICK::DELAY)
		addAnimationPart(_tipoAnimacion, duracion, posBrick, posBrick);

	if (_tipoAnimacion == ANIMACION_BRICK::RESCALE)
		addAnimationPart(_tipoAnimacion, duracion, posBrick, posBrick);

	if (_tipoAnimacion == ANIMACION_BRICK::ARRIBA_PILLAR)
	{
		if (posBrick.x == _dest.x)
		{
			if (duracion == 0)
				addAnimationPart(_tipoAnimacion, 800, posBrick, _dest);
			else
				addAnimationPart(_tipoAnimacion, duracion, posBrick, _dest);
		}
		else
		{
			float y = (int)ofRandom(2,3) * dim;
			ofVec2f newPos = posBrick - ofVec2f(0, y);

			addAnimationPart(_tipoAnimacion, 500, posBrick, newPos);
			ofVec2f desde = newPos;
			newPos.x = _dest.x;
			addAnimationPart(_tipoAnimacion, 400, desde, newPos);
			//addAnimationPart(_tipoAnimacion, 4000, newPos, _dest);
			addAnimationPart(_tipoAnimacion, 800, newPos, _dest);
		}
	}

	if (_tipoAnimacion == ANIMACION_BRICK::ABAJO_PILLAR)
	{
		if (posBrick.x == _dest.x)
			addAnimationPart(_tipoAnimacion, 800, posBrick, _dest);
		else
		{
			float y = (int)ofRandom(2, 4) * dim;
			ofVec2f newPos = posBrick + ofVec2f(0, y);
			addAnimationPart(_tipoAnimacion, 600, posBrick, newPos);
			ofVec2f desde = newPos;
			newPos.x = _dest.x;
			addAnimationPart(_tipoAnimacion, 500, desde, newPos);
			addAnimationPart(_tipoAnimacion, 800, newPos, _dest);
		}
	}

	if (_tipoAnimacion == ANIMACION_BRICK::DUMP_BRIDGE)
	{
		if (posBrick.x == _dest.x)
			addAnimationPart(ANIMACION_BRICK::ABAJO_PILLAR, 800, posBrick, _dest);
		else
		{
			ofVec2f newPos = posBrick;
			newPos.x = _dest.x;
			addAnimationPart(ANIMACION_BRICK::ABAJO_PILLAR, 600, posBrick, newPos);
			//newPos.y = _dest.y;
			addAnimationPart(ANIMACION_BRICK::ABAJO_PILLAR, 800, newPos, _dest);
		}
	}

	if (_tipoAnimacion == ANIMACION_BRICK::DUMP_PILLAR)
	{
		addAnimationPart(_tipoAnimacion, 800, posBrick, _dest);
	}
}


void Brick::addAnimationPart(ANIMACION_BRICK tipo, float duration, ofVec2f posIni, ofVec2f posFin)
{
	ANIMACION a;
	a.timeIni = -1;
	a.duration = duration;
	a.posIni = posIni;
	a.posFin = posFin;
	a.tipo = tipo;
	a.scaleIni = scaleFactorGlobal;
	animaciones.push_back(a);
}
		
void Brick::startAnimation()
{
	if (estadoBrick == ESTADO_BRICK::MOVING)
		return;
	estadoBrick = ESTADO_BRICK::MOVING;
	animaciones.front().timeIni = ofGetElapsedTimeMillis();
	if (animaciones.front().tipo == ANIMACION_BRICK::ARRIBA_PILLAR || animaciones.front().tipo == ANIMACION_BRICK::ABAJO_PILLAR || animaciones.front().tipo == ANIMACION_BRICK::DUMP_PILLAR)
	{
		int v = 1;
		ofNotifyEvent(evBrickStart, v);
	}
	//cout << "moviendo brick " << brick_id << "time ini = " << animaciones.front().timeIni << endl;
}


void Brick::setAlphaTarget(float alpha, float dampAlpha)
{
	this->alphaTarget = alpha;
	this->alphaDamp = dampAlpha;
}

