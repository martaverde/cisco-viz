#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main( ){
	ofGLFWWindowSettings settings;
	 settings.width = 504 * 4;
     settings.height = 672;
	 //settings.decorated = false;
	 settings.windowMode = OF_WINDOW;
	 ofCreateWindow(settings);
	ofRunApp(new ofApp()); 

}
