#include "oscManager.h"

//--------------------------------------------------------------
void oscManager::setup() {
	XML.loadFile("mySettings.xml");

	LOCAL_PORT = XML.getValue("LOCAL_PORT", 0);
	REMOTE_IP = XML.getValue("REMOTE_IP", "");
	REMOTE_PORT = XML.getValue("REMOTE_PORT", 0);
	MACHINE = XML.getValue("MACHINE", 0);
	cout << "OSCManager listening for osc messages on port " << LOCAL_PORT << "\n";
	cout << "OSCManager sending to SM: " << REMOTE_IP << ":" << REMOTE_PORT << endl;

	// Setup Receiver
	receiver_.setup(LOCAL_PORT);

	// Setup Sender
	sender.setup(REMOTE_IP, REMOTE_PORT);
	startState = false;
	updateBricksPendiente = false;
	dataPath = "/videos/screensaver/salva_welcome.mov";
}


//--------------------------------------------------------------
void oscManager::update(){
    // Receive messages
    receiveMessages();
}

//--------------------------------------------------------------
void oscManager::receiveMessages(){
    
    // check for waiting messages
    while(receiver_.hasWaitingMessages()){
        // get the next message
        ofxOscMessage m;
		receiver_.getNextMessage(m);

		// :: NEXT STATE ::
        if(m.getAddress() == "/state_id"){
            stateId = m.getArgAsInt32(0);
			sendResponse("ok_next_state");
			cout << "osc Manager: STATE ID -> " << stateId << endl;
        }
        else if(m.getAddress() == "/state_type"){
			stateType = m.getArgAsInt32(0);
			cout << "osc Manager: STATE TYPE -> " << stateType << endl;
        }
        else if(m.getAddress() == "/topic_id"){
            topicId = m.getArgAsInt32(0);
			cout << "osc Manager: TOPIC ID -> " << topicId << endl;
        }
        else if(m.getAddress() == "/copy"){
            copy = m.getArgAsString(0);
			cout << "osc Manager: COPY -> " << copy << endl;
        }
		else if (m.getAddress() == "/highlights") {
			highlight = m.getArgAsString(0);
			cout << "osc Manager: HIGHLIGHTS -> " << highlight << endl;
		}
		else if (m.getAddress() == "/fact") {
			factText = m.getArgAsString(0);
			cout << "osc Manager: FACT -> " << factText << endl;
		}
        else if(m.getAddress() == "/path"){
            dataPath = m.getArgAsString(0);
			cout << "osc Manager: PATH -> " << dataPath << endl;
        }

		// :: CHECK CONNECTION ::
        else if(m.getAddress() == "/check_connection"){
            sendResponse("ok_connected");
        }

		// :: START STATE ::
        else if(m.getAddress() == "/start_state"){
            sendResponse("ok_start_state");
            startState = true;
        }

		// :: STATE SPECIFIC ::
		else if (m.getAddress() == "/recap") {
			// :: Recap ::
			recapType = m.getArgAsInt32(0);
			strRecap = m.getArgAsString(1);
			cout << "osc Manager: RECAP -> " << strRecap << endl;
		}
		else if (m.getAddress() == "/keynote") {
			// :: Keynote ::
			keynote = m.getArgAsInt32(0);
		}
		else if (m.getAddress() == "/screensaver") {
			// :: Screensaver ::
			screensaver = m.getArgAsInt32(0);
		}
		else if (m.getAddress() == "/welcome") {
			// :: Welcome ::
			welcome = m.getArgAsInt32(0);
		}
		else if (m.getAddress() == "/advertise") {
			// :: Advertise ::
			advertise = m.getArgAsInt32(0);
		}
		else if (m.getAddress() == "/grow") {
			// :: Grow ::
			grow1 = m.getArgAsInt32(0);
			grow2 = m.getArgAsInt32(1);
		}
		else if (m.getAddress() == "/bricks") {
			bricksString = m.getArgAsString(0);
			cout << "OSC Manager: update bricks" << bricksString << endl;
			vector<string> bricksStringSplit = ofSplitString(bricksString, ",", true, true);
			bricks.clear();
			for (int i = 0; i < bricksStringSplit.size(); i++) {
				bricks.push_back(ofToInt(bricksStringSplit[i]));
			}
			updateBricksPendiente = true;
		}
		else if (m.getAddress() == "/dump") {
			// :: Dump ::
		}
		else if (m.getAddress() == "/final_brick") {
			// :: Final Brick ::
		}
		else if (m.getAddress() == "/shutdown") {
			// :: Shut Down ::
			currentBricksPos = m.getArgAsInt32(0);
		}
		else if (m.getAddress() == "/milestone") {
			// :: Milestone ?
		}
		
		// :: OTHER ::
        else{
            string msg_string;
            msg_string = m.getAddress();
            msg_string += ": ";
            for(int i = 0; i < m.getNumArgs(); i++){
                // get the argument type
                msg_string += m.getArgTypeName(i);
                msg_string += ":";
                // display the argument - make sure we get the right type
                if(m.getArgType(i) == OFXOSC_TYPE_INT32){
                    msg_string += ofToString(m.getArgAsInt32(i));
                }
                else if(m.getArgType(i) == OFXOSC_TYPE_FLOAT){
                    msg_string += ofToString(m.getArgAsFloat(i));
                }
                else if(m.getArgType(i) == OFXOSC_TYPE_STRING){
                    msg_string += m.getArgAsString(i);
                }
                else{
                    msg_string += "unknown";
                }
            }
            otherMsg = msg_string;
        }
    }
}


// ::::: SEND OSC :::::
void oscManager::sendResponse(string response) {
	ofxOscMessage m;
	m.setAddress("/" + ofToString(MACHINE) + "/response");
	m.addStringArg(response);
	sender.sendMessage(m);
}

void oscManager::sendStatePercentage(float percent) {
	ofxOscMessage m;
	m.setAddress("/" + ofToString(MACHINE) + "/state_percentage");
	m.addFloatArg(percent);
	sender.sendMessage(m);
}

void oscManager::sendStateIsFinished() {
	ofxOscMessage m;
	m.setAddress("/" + ofToString(MACHINE) + "/state_finished");
	sender.sendMessage(m);
}
// ::::: SEND OSC :::::


// ::::: GET VARIABLES :::::
// :: NEXT STATE ::
int oscManager::getState() {
	return stateId;
}
int oscManager::getStateType() {
	return stateType;
}
int oscManager::getTopicId() {
	return topicId;
}
string oscManager::getCopy() {
	return copy;
}
string oscManager::getHighlights() {
	return highlight;
}
string oscManager::getFactText() {
	return factText;
}
string oscManager::getPath() {
	return dataPath;
}
// :: STATE SPECIFIC ::
int oscManager::getRecapType() {
	return recapType;
}
string oscManager::getRecapString() {
	return strRecap;
}
int oscManager::getKeynote() {
	return keynote;
}
int oscManager::getScreensaver() {
	return screensaver;
}
int oscManager::getWelcome() {
	return welcome;
}
int oscManager::getAdvertise() {
	return advertise;
}
int oscManager::getGrow1() {
	return grow1;
}
int oscManager::getGrow2() {
	return grow2;
}
vector<int> oscManager::getBricks() {
	return bricks;
}
int oscManager::getCurrentBricksPos() {
	return currentBricksPos;
}
// ::::: GET VARIABLES :::::

