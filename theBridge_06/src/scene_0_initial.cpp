
#include "scene_0_initial.h"
#include "oscManager.h"
#include "Constantes.h"
#include "ofMain.h"

void scene_0_initial::setup() {

}

void scene_0_initial::start() {
	startTime = ofGetElapsedTimeMillis();
	sceneDuration = 2000;
	fondo.load("mapping_bridge.png");
}

void scene_0_initial::update() {
	percentage = ofMap(ofGetElapsedTimeMillis() - startTime, 0, sceneDuration, 0, 1);
	if (percentage > 0.7f) {
		percentage = 0.99f;
	}

	if (ofGetElapsedTimeMillis() - startTime > sceneDuration) {
		startTime = ofGetElapsedTimeMillis();
	}
}

void scene_0_initial::draw(int alpha) {

	ofSetColor(255, alpha);
	fondo.draw(0, 0, BRIDGE_WIDTH, BRIDGE_HEIGHT);

	ofDrawBitmapString("STANDBY SCREEN", 20, 20);

	ofDrawBitmapString(percentage, 20, 40);
}

float scene_0_initial::getPercentage() {
	return percentage;
}

