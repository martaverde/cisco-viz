

#ifndef __scene_data_display__
#define __scene_data_display__

#include "ofxHapPlayer.h"
#include "ofxParagraph.h"
#include "Brick.h"
#include "Constantes.h"

#define ALPHA_FADED 100

class scene_data_display{
	public:
		void setup	( vector<Brick> *bricks, GRID_POSICIONES *pos, int *secuenciaPosiciones );
		void start	( int stateType_, int topicId_, string copyText_, string highlights_, string factText_, string _path,  int siguientePosicion );
		void update	( );
		void draw	( int alpha );

		void	pauseVideo(bool paused);
		void	cueVideo(bool rewind);
		void	sceneFinished(bool _finished);
		bool	finished;
		float	getPercentage();

	private:
		// :: STATE TYPES ::
		void drawFeatured	( int alpha );  // 0
		void drawSecondary	( int alpha ); // 1
		void drawMilestone	( int alpha ); // 2
		void drawBrick		( );

		bool			bBrickCreated;
		int				siguientePosicionBrick;

		// :: BRICKS ::
		void			newBrick(int tipoBrick);
		void			drawGrid();
		vector<Brick>   *bricks_global;
		GRID_POSICIONES	*posicionesGrid;
		int				*secuenciaPosiciones;
		// 
		vector <Brick>	groupOfBricks;
		vector <ofVec2f> gridPositions;
		//ofFbo			brickTime;
		int				ancho = 56;
		int				indexPosition;

		// :: VIDEOS ::
		ofxHapPlayer video1;
		ofxHapPlayer video2;
		ofxHapPlayer video3;
		int currentVideo;
		ofDirectory dir;
		int videosCargados = 0;

		// :: STATE OPTIONS AND DATA RECEIVED ::
		int stateType; //si es featured o secondary
		int topicId;
		string copyText;
		string path;
		string highlights;

		// :: STATE PERCENTAGE ::
		float percentage;
		int mode;
		float duration;
		float framesTotal;
		float frameCount;

		// :: COLORS ::
		ofColor firstColor;
		ofColor secondColor;
		ofColor white;
		ofColor green;
		ofColor red;
		ofColor yellow;
		ofColor blue;
		ofColor darkGreen;
		ofColor darkRed;
		ofColor darkBlue;
		ofColor darkYellow;
		void  setColors();

		// :: COPY IN PARAGRAPHS ::
		string	copyParsed[3];
		string	highlightsLinesToParse[3];
		float heightParagraphs[3];
		int paragraphFontSize[3];

		// :: PARAGRAPH ::
		ofFbo		PARAGRAPH_COPY;
		// :: Fact - Copy ::
		ofFbo		FACT_COPY;
		string		factNumberString;
		string		factTextString;
		ofRectangle	factNumberBox;
		ofRectangle factTextBox;
		int			factBoxTotalWidth;
		int			factNumberFontSize;
		int			factTextFontSize;
		bool		showingFact;
		// :: Fonts ::
		ofTrueTypeFont ciscoSans, ciscoSansLight, ciscoSans2;
		ofTrueTypeFont ciscoSansReferencia;
		vector<ofxParagraph*> paragraphs;
		ofxParagraph paragraph;
		float posYTextoCifra;
		float posYFinalTextoCifra;
		float catchUpSpeed;
		float posYfinalTextoCifra2;
		ofBuffer buffer;
		vector <string> result;
		string positions;
		float initTime;
		float newPos;
		float easedPos;
		int spacingHighlight;

		int brickGrandeCentroPos;

		ofFbo fboText;

};

#endif 
