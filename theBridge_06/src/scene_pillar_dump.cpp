

#include "scene_pillar_dump.h"
#include "oscManager.h"
#include "Constantes.h"

//video 10 segundos
//25 segundos animaci�n colocacion brick 

void scene_pillar_dump::setup( vector<Brick> *bricks, GRID_POSICIONES *pos, int *_secuencia ) {
	
	this->posicionesGrid = pos;
	this->bricks_global = bricks;
	this->secuenciaPosiciones = _secuencia;
	
	sceneDuration = 34000;

	receiver.setup(PORT);
	bFirstTime = false;
	std::cout << "DUMP Listener listening for messages on port " << PORT << endl;
	player.load(PILLAR_DUMP_VIDEO_PATH);
	player.setLoopState(OF_LOOP_NONE);

	//player.stop();
	player.setPosition(0);

	brickGrandeCentroPos = (84 * 5) + (84 / 2);


}

void scene_pillar_dump::start(int _s)
{
	cout << "pillar_dump siguiente posicion = " << _s << endl;
	this->siguientePosicion = _s;
	cout << "video cargado: " << PILLAR_DUMP_VIDEO_PATH << endl;
	player.setPosition(0);
	player.play();

	azulPosActual = 0;
	variosPosActual = 0;
	//brickGrandeCentroPos = posicionesGrid[84 * 5 + 84 / 2].pos;
	timeIni = ofGetElapsedTimeMillis();
	bricks_local.clear();

	estado = ESTADO_INTRO;
	
	//para eliminar posibles mensajes que hayan llegado de los pillars mientras no se est� ejecutando esta escena
	//receiver.setup(PORT);
	
	while (receiver.hasWaitingMessages())
	{
		ofxOscMessage m;
		receiver.getNextMessage(m);
	}
}



void scene_pillar_dump::update() {

	player.update();

	for (int i = 0; i < bricks_local.size(); i++)
		bricks_local[i].update();

	//if (ofGetElapsedTimeMillis() > sceneDuration) {
	//	dataOscManager->sendStateIsFinished();
	//}

	while (receiver.hasWaitingMessages()) {
		ofxOscMessage m;
		receiver.getNextMessage(m);
		if (m.getAddress() == "/brick_arrival") {
			int idPillar = m.getArgAsInt(0);
			int brickType = m.getArgAsInt(1);
			int brickPos = m.getArgAsInt(2);
			brickArrival(idPillar, brickType, brickPos);
		}
	}
	
	updateStatus();

}


void scene_pillar_dump::updateStatus( )
{
	int duracionFade = 1000;
	int duracionCreacion = 1500;
	int duracionScale = 2000;

	if (estado == ESTADO_INTRO)
	{
		if (ofGetElapsedTimeMillis() - timeIni > durationIntro)
			estado = ESTADO_LLEGADA;
	}

	if (estado == ESTADO_LLEGADA)
	{
		//hay que asegurar que llegan 25 bricks azules

		if (ofGetElapsedTimeMillis() - timeIni > durationIntro + durationLlegada)
		{
			estado = ESTADO_AGRUPAR;
			//hay que hacer desaparecer el interior de los bricks azules
			for (int i = 0; i < bricks_local.size(); i++)
			{
				bricks_local[i].addAnimation(bricks_local[i].getPos(), Brick::ANIMACION_BRICK::FADE_INSIDE, duracionFade);
				bricks_local[i].startAnimation();
			}
		}
		else
		{
			//a�ado bricks azules... 25 en durationLlegada milisegundos
			if (ofGetElapsedTimeMillis() - timeIni > durationIntro && ofGetElapsedTimeMillis() - timeIni < durationIntro + (durationLlegada - 4000))
				if (ofGetElapsedTimeMillis() - lastFakeBrickTime > (durationLlegada-4000)/27)
				{
					brickArrival(ofRandom(1, 6), 9, ofRandom(0, 8));
					lastFakeBrickTime = ofGetElapsedTimeMillis();
				}
		}
	}

	if (estado == ESTADO_AGRUPAR)
	{
		if (ofGetElapsedTimeMillis() - timeIni > durationIntro + durationLlegada + duracionFade)
		{
			estado = ESTADO_CREAR;
			bricks_local.clear();
			//hay que crear el brick gordo
			Brick *brick = new Brick;
			brick->setup(posicionesGrid[brickGrandeCentroPos].pos, 9, bricks_global->size(), 0, 0, ofGetElapsedTimeMillis());
			bricks_global->push_back(*brick);
			bricks_global->back().setScaleGlobal(5.0f);
			bricks_global->back().addAnimation(posicionesGrid[brickGrandeCentroPos].pos, Brick::ANIMACION_BRICK::CREACION, duracionCreacion);
			//bricks_global->back().addAnimation(posicionesGrid[brickGrandeCentroPos].pos, Brick::ANIMACION_BRICK::DUMP_BRIDGE, 20);
			//bricks_global->back().addAnimation(posicionesGrid[brickGrandeCentroPos].pos, Brick::ANIMACION_BRICK::DELAY, 20);
			bricks_global->back().startAnimation();
		}
	}

	if (estado == ESTADO_CREAR)
	{
		if (ofGetElapsedTimeMillis() - timeIni > durationIntro + durationLlegada + duracionFade + duracionCreacion)
		{
			estado = ESTADO_REDUCIR;
			bricks_global->back().addAnimation(posicionesGrid[brickGrandeCentroPos].pos, Brick::ANIMACION_BRICK::RESCALE, duracionScale );
			int pos = secuenciaPosiciones[siguientePosicion];
			cout << "posicionGrid =  " << pos << "coordenadas: " << posicionesGrid[pos].pos << endl;
			bricks_global->back().addAnimation(posicionesGrid[pos].pos, Brick::ANIMACION_BRICK::DUMP_BRIDGE, 0);
			bricks_global->back().startAnimation();
		}
	}

	//if (estado == ESTADO_REDUCIR)
	//{
	//	if (ofGetElapsedTimeMillis() - timeIni > durationIntro + durationLlegada + duracionFade + duracionCreacion + duracionScale)
	//	{
	//		estado = ESTADO_COLOCACION;
	//		int pos = secuenciaPosiciones[siguientePosicion];
	//		cout << "posicionGrid =  " << pos << "coordenadas: " << posicionesGrid[pos].pos << endl;
	//		bricks_global->back().addAnimation(posicionesGrid[pos].pos, Brick::ANIMACION_BRICK::DUMP_BRIDGE, 0);
	//		bricks_global->back().startAnimation();
	//	}
	//}

}


void scene_pillar_dump::brickArrival(int idPillar, int tipo, int pos)
{
	//cout << "brick_arrival: " << idPillar << "," << tipo << "," << pos << endl;
	int posGrid = 9 * idPillar + pos;
	ofVec2f initPos;
	initPos.x = posGrid * 56;
	initPos.y = ANCHO_BRICK / 2.0f;
	ofVec2f dest;
	if (tipo == 9)
		dest = getNextBrickAzul();
	else
		dest = getNextBrickVarios();


	Brick *brick = new Brick;
	brick->setup(initPos, tipo, bricks_local.size(), 0, 0, ofGetElapsedTimeMillis());
	bricks_local.push_back(*brick);
	bricks_local.back().addAnimation(dest, Brick::ANIMACION_BRICK::DUMP_BRIDGE, 0);
	bricks_local.back().startAnimation();
}


void scene_pillar_dump::draw(int alpha) {
	// ofBackground(0);

	ofSetColor(255, alpha);

	ofPushStyle();
	if (ofGetElapsedTimeMillis() - timeIni > 2000)
		for (int i = 0; i < bricks_global->size(); i++)
			bricks_global->at(i).draw();
	ofPopStyle();

	if (estado == ESTADO_INTRO)
		player.draw(0, 0, BRIDGE_WIDTH, BRIDGE_HEIGHT);

	if (estado != ESTADO_INTRO)
		drawDump();

	/*
	ofSetColor(255);
	ofDrawBitmapString(ofToString(ofGetFrameRate(), 0) + " FPS", 20, 20);
	*/

}


void scene_pillar_dump::drawDump() {
	
	//primero los que no son azules
	for (int i = 0; i < bricks_local.size(); i++)
		if (bricks_local[i].getTipo() != 9)
			bricks_local[i].draw();

	//encima los que son azules
	for (int i = 0; i < bricks_local.size(); i++)
		if (bricks_local[i].getTipo() == 9)
			bricks_local[i].draw();


}

ofVec2f scene_pillar_dump::getNextBrickVarios()
{

	int posIni = brickGrandeCentroPos - ((84 * 2) + 2);
	int linea = floor((float)variosPosActual / (float)BRICK_GRANDE_DIM);
	int columna = variosPosActual - (linea * BRICK_GRANDE_DIM);
	int posReal = posIni + (linea * 84) + columna;

	//cout << "posicion destino brick = " << posReal << "(posIni = " << posIni << ", linea =" << linea << ", columna = " << columna << endl;

	variosPosActual++;
	if (variosPosActual == BRICK_GRANDE_DIM * BRICK_GRANDE_DIM)
		variosPosActual = 0;

	return posicionesGrid[posReal].pos;
}

ofVec2f scene_pillar_dump::getNextBrickAzul()
{

	int posIni = brickGrandeCentroPos - ((84 * 2) + 2);
	int linea = floor((float)azulPosActual / (float)BRICK_GRANDE_DIM);
	int columna = azulPosActual - (linea * BRICK_GRANDE_DIM);
	int posReal = posIni + (linea * 84) + columna;

	//cout << "posicion destino brick = " << posReal << "(posIni = " << posIni << ", linea =" << linea << ", columna = " << columna << endl;

	azulPosActual++;
	if (azulPosActual == BRICK_GRANDE_DIM * BRICK_GRANDE_DIM)
		azulPosActual = 0;

	return posicionesGrid[posReal].pos;

}

void scene_pillar_dump::pauseVideo(bool paused) {
	player.setPaused(paused);
}

void scene_pillar_dump::cueVideo(bool rewind) {
	float currentPosition = player.getPosition();
	if (rewind) {
		currentPosition -= CUE_FOR_TESTING;
	}
	else {
		currentPosition += CUE_FOR_TESTING;
	}
	player.setPosition(currentPosition);
}