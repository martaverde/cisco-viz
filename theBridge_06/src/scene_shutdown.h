
#ifndef __scene_shutdown__
#define __scene_shutdown__

#include "Brick.h"
#include "Constantes.h"

class scene_shutdown {
public:
    void setup(vector<Brick> *bricks, GRID_POSICIONES *pos, int *_secuencia);
	void start( );
    void update();
    void draw(int alpha);
	void keyReleased(int key);

private:

	vector<Brick>   *bricks_global;
	GRID_POSICIONES	*posicionesGrid;
	int				*secuenciaPosiciones;

	float initTime;

	float sceneDuration;
	
	int lastIndex;
};
#endif /* defined(__example_Simple__FirstScene__) */
