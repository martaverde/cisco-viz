
#ifndef __scene_pillar_dump__
#define __scene_pillar_dump__

#include "ofxHapPlayer.h"
#include "ofxOsc.h"
#include "Brick.h"
#include "Constantes.h"

#define PORT 33333
#define NUM_MSG_STRINGS 20
#define BRICK_GRANDE_DIM 5
#define PILLAR_DUMP_VIDEO_PATH		"videos/pillar_dump/discharge_bridge.mov"
#define DURACION_INTRO 10 * 1000
#define DURACION_LLEGADA 18 * 1000
//#define DURACION_LLEGADA 12 * 1000
#define DURACION_COLOCACION 5 * 1000

class scene_pillar_dump {

	public:
		void setup			( vector<Brick> *bricks, GRID_POSICIONES *pos, int *secuenciaPosiciones );
		void start			( int siguienteBrick );
		void update			( );
		void draw			( int alpha );
		void drawDump		( );

		void	pauseVideo(bool paused);
		void	cueVideo(bool rewind);

	private:

		void			brickArrival		( int idPillar, int tipo, int pos );
		ofVec2f			getNextBrickVarios	( );
		ofVec2f			getNextBrickAzul	( );
		void			updateStatus		( );

		vector<Brick>   *bricks_global;
		GRID_POSICIONES	*posicionesGrid;
		int				*secuenciaPosiciones;

		enum			ESTADO { ESTADO_INTRO, ESTADO_LLEGADA, ESTADO_AGRUPAR, ESTADO_CREAR, ESTADO_REDUCIR, ESTADO_COLOCACION};
		ESTADO			estado;

		vector<Brick>   bricks_local;
		double			timeIni;
		ofxOscReceiver	receiver;
		ofTrueTypeFont	font;
		ofxHapPlayer	player;
		float			sceneDuration;
	
		int				durationIntro = DURACION_INTRO; //intro video
		int				durationLlegada = DURACION_LLEGADA; //llegada
		int				durationColocacion = DURACION_COLOCACION; //colocacion

		//string copy;

		bool			bFirstTime = true;
		//bool			brickGrandeVarios[BRICK_GRANDE_DIM * BRICK_GRANDE_DIM];
		int				variosPosActual;
		//bool			brickGrandeAzules[BRICK_GRANDE_DIM * BRICK_GRANDE_DIM];
		int				azulPosActual;
		int 			brickGrandeCentroPos;
		int				siguientePosicion;
		//ofFbo			fboBricksAzules;
		double			lastFakeBrickTime;
};

#endif /* defined(__example_Simple__FirstScene__) */
