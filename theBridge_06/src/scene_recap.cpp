

#include "scene_recap.h"
#include "Constantes.h"
//#define DURACION_ESCENA 100 //segundos

//----> check percentage 


void scene_recap::setup() {
	cout << "escena 8 setup" << endl;
	ofEnableSmoothing();

	minRecapsPerLine = 10;
	nRecapLines = 32;
	standardVariationX = -3.f;

	ancho = 56;
	ciscoSansBold.load("fonts/CiscoSansTTBold.ttf", 28);

	colores[0] = ofColor(107, 189, 82);
	colores[1] = ofColor(221, 42, 39);
	colores[2] = ofColor(242, 166, 40);
	colores[3] = ofColor(27, 168, 223);

}

/*-------------------------------------------------------------------------
Modo 0 hour recap
video 12 segundos
80 de recap

modo 1 daily recap
video 10 segundos
85 de recap
*/
void scene_recap::start(int tipo, string churro)
{
	this->recapType = tipo;
	this->copy = churro;
		
	for (int i = 0; i < lineas.size(); i++)
		lineas[i].recapBlocks.clear();
	
	lineas.clear();

	//salen 12 lineas
	for (int i = 0; i < nRecapLines; i++) {
		LINEA *l = new LINEA;
		l->posY = (ancho / 2) + ancho * i;
		l->posXInicial = 36;
		//l->posXInicial = BRIDGE_WIDTH + ofRandom(0, 500);
		l->posXSiguiente = l->posXInicial;
		l->posXAnimacion = l->posXInicial + BRIDGE_WIDTH;
		//random speed de 0.2 a 0.4
		l->speed = ofRandom(1.0f, 1.2f);
		lineas.push_back(*l);
	}
	
	if (recapType == 0) {
		sceneDuration = 80 * 1000;
		actualVideo = HOUR_RECAP_VIDEO_PATH;
	}

	if (recapType == 1) {
		sceneDuration = 85 * 1000;
		actualVideo = DAILY_RECAP_VIDEO_PATH;
	}

	video.load(actualVideo);
	cout << "video cargado: " << actualVideo << endl;
	video.setLoopState(OF_LOOP_NONE);
	video.play();

	parseaRecap(churro);

	lineaRecapActual = 0;

	maxLongitud = 0;
	for (int i = 0; i < lineas.size(); i++)
	{
		if (lineas[i].posXSiguiente + lineas[i].posXInicial > maxLongitud)
			maxLongitud = lineas[i].posXSiguiente + lineas[i].posXInicial;
	}

	timeIni = ofGetElapsedTimeMillis();
	lastJumpTime = ofGetElapsedTimef();
	timeBetweenJumps = 5000;
	jumpNow = false;
	showingRecapLines = false;
}


void scene_recap::parseaRecap( string churro ) {
	
	cout << "parseando recap: " << endl << churro << endl;

	vector<string> churroLines = ofSplitString(churro, "//");
	bool repeatRecaps = false;
	bool enoughRecaps = false;
	int nRecapsTotal = 0;
	if (churroLines.size() < nRecapLines * minRecapsPerLine) {
		repeatRecaps = true;
	}

	while (!enoughRecaps) {
		for (int i = 0; i < churroLines.size(); i++) {
			vector<string> datos = ofSplitString(churroLines[i], "|");
			if (datos.size() == 2) {
				addRecap(ofToInt(datos[0]), datos[1]);
				//cout << "addRecap: " << datos[0] << " - " << datos[1] << endl;
				nRecapsTotal++;
			}
		}
		if (!repeatRecaps || nRecapsTotal >= nRecapLines * minRecapsPerLine) {
			enoughRecaps = true;
		}
	}
	cout << "FIN parseando recap: " << endl;
}

void scene_recap::addRecap(int tipo, string texto) {
	
	// cout << "a�adiendo recap en linea " << lineaRecapActual << " -> " <<  texto << endl;

	recapBlock *rb = new recapBlock;
	ofVec2f pos = ofVec2f(lineas[lineaRecapActual].posXSiguiente, lineas[lineaRecapActual].posY);
	//ofVec2f pos = ofVec2f( lineas[lineaRecapActual].posXSiguiente, lineas[lineaRecapActual].posY);
	rb->setup(pos, tipo, texto, &ciscoSansBold);
	int width = rb->getBlockWidth();
	lineas[lineaRecapActual].recapBlocks.push_back(*rb);
	lineas[lineaRecapActual].posXSiguiente = lineas[lineaRecapActual].posXSiguiente + width + rb->getBorder();
 
	lineaRecapActual++;
	
	if (lineaRecapActual == lineas.size())
		lineaRecapActual = 0;
}


void scene_recap::update() {

	video.update();

	float now = ofGetElapsedTimeMillis() - timeIni;
	float iniRecap;
	//if (recapType == 0)
	//	iniRecap = 10 * 1000;
	//else
	//	iniRecap = 12 * 1000;
	if (recapType == 0)
		iniRecap = 8 * 1000;
	else
		iniRecap = 8 * 1000;

	if (now > iniRecap) {
		showingRecapLines = true;

		if (ofGetElapsedTimeMillis() - lastJumpTime >= timeBetweenJumps && jumpNow == false) {
			jumpNow = true;
			posYanterior = 0;
			timeStartJump = ofGetElapsedTimeMillis();
			//colorActual++;
			//if (colorActual == 4)
			//	colorActual = 0;

		}
		if (jumpNow) {
			// TODO ::::: ANIMAR MOVIMIENTO HACIA ARRIBA :::::
			float ahora = ofGetElapsedTimeMillis() - timeStartJump;
			//cout << "ahora= " << ahora << endl;
			float posY = ofxeasing2::map(ahora, 0, 500, 0, 56, &ofxeasing2::circ::easeInOut);
			//cout << "easing= " << posY << endl;
			float toca = posY - posYanterior;
			//cout << toca << endl;
			for (int i = 0; i < lineas.size(); i++)
			{
				for (int b = 0; b < lineas[i].recapBlocks.size(); b++) {
					//lineas[i].recapBlocks[b].y -= 56;
					lineas[i].recapBlocks[b].y -= toca;
				}
			}
			posYanterior = posY;
			if (ahora > 500)
			{
				jumpNow = false;
				lastJumpTime = ofGetElapsedTimeMillis();
			}
		}

		if (ofGetElapsedTimeMillis() - lastColorChange > 1000)
		{
			colorActual = ofRandom(0, 4);
			if (colorActual == 4) colorActual = 3;
			lastColorChange = ofGetElapsedTimeMillis();
		}

		/*
		// DESPLAZAMIENTO LATERAL
		for (int i = 0; i < lineas.size(); i++)
		{	
			float variationX = standardVariationX * lineas[i].speed;
			for (int b = 0; b < lineas[i].recapBlocks.size(); b++)
			{
				// Desplazamiento normal hacia la izquierda
				lineas[i].recapBlocks[b].incrementaX(variationX);
				// Cuando sale de la pantalla (izquierda):
				// reasignacion de posicion (a la derecha de todos los blocks)
				int recapBlockWidth = lineas[i].recapBlocks[b].getBorder() * 2 - lineas[i].recapBlocks[b].getBlockWidth();
				if (lineas[i].recapBlocks[b].x < BRIDGE_WIDTH/2 - recapBlockWidth) {
					
				}
			}
			lineas[i].posXSiguiente += variationX;
		*/
	}

}

void scene_recap::draw(int alpha) {
	// ofBackground(0);

	if (showingRecapLines) {
		for (int i = 0; i < lineas.size(); i++) {
			//	ofDrawLine(0, lineas[i].posY, ofGetWidth(), lineas[i].posY);
			for (int b = 0; b < lineas[i].recapBlocks.size(); b++)
				lineas[i].recapBlocks[b].draw();
		}
		// TODO :::::: CHANGE COLORS DYNAMICALLY
		//ofSetColor(0, 0, 0, 255);
		ofSetColor(colores[colorActual]);
		ofDrawRectangle(0, 672, BRIDGE_WIDTH, 672);
	}

	ofSetColor(255, alpha);
	video.draw(0, 0, BRIDGE_WIDTH, BRIDGE_HEIGHT);

	/*
	ofSetColor(255, 0, 0);
	ofDrawBitmapString(copy, 20, 100);
	ofDrawBitmapString("RECAP", 20, 300);
	
	// Draw the FPS display
	ofSetColor(255);
	ofDrawBitmapString(ofToString(ofGetFrameRate(), 0) + " FPS", 20, 20);
	*/
}


void scene_recap::pauseVideo(bool paused) {
	video.setPaused(paused);
}

void scene_recap::cueVideo(bool rewind) {
	float currentPosition = video.getPosition();
	if (rewind) {
		currentPosition -= CUE_FOR_TESTING;
	}
	else {
		currentPosition += CUE_FOR_TESTING;
	}
	video.setPosition(currentPosition);
}
