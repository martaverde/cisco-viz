
#ifndef __scene_01_welcome__
#define __scene_01_welcome__

#include "ofxHapPlayer.h"

class scene_00_videoPlayer{
public:
    void setup();
	void start(int stateId, int stateType, string _path);
    void update();
    void draw(int alpha);

	void	pauseVideo(bool paused);
	void	cueVideo(bool rewind);
	void	sceneFinished(bool _finished);
	float	getPercentage();
   
	ofxHapPlayer video;
	float percentage;
	string path;
	float duration;

	bool finished;

private:
	string stateToPath(int stateId, int stateType);

};

#endif
