//
//  Brick.hpp
//  cisco_v1
//
//  Created by Marta Verde on 14/12/18.
//



//Settings Needed:

/*
 
Type of brick - > color front and back

animation:
Initial position
Final position
 
Cuando se está animando (boolean)
 

Brick id con settings guardados (guardar en XML?) (posicion FINAL)
*/


#ifndef recapBlock_h
#define recapBlock_h

#include <stdio.h>
#include "ofMain.h"
//#include "ofxEasing.h"



class recapBlock {
    
public:
 
    void setup(ofVec2f _Pos, int _type, string _inputString, ofTrueTypeFont *font);
    void update();
    void draw();
    void keyReleased(int key);
    bool movingBricks;
	void incrementaX(float x);

    void zenoToPoint(ofVec2f finalPos);
   
    ofTrueTypeFont *ciscoSansBold;
    
    void interiorAnimation();
    
    float catchUpSpeed;
    ofVec2f initPos, finalPos;
    ofVec2f Pos;
    float x;
    float y;
    float speedY;
    float speedX;
    int dim;
    
    ofColor backColor;
    ofColor frontColor;
    string inputString;
    
    
    float speed;
    
    int border;
    
    float counter;
    int brick_id;
    int type;
    
    ofColor green ;
    ofColor red ;
    ofColor yellow ;
    ofColor blue;
    
    ofColor darkGreen;
    ofColor darkRed;
    ofColor darkBlue;
    ofColor darkYellow;
    int blockWidth;
    void simpleTriangle(float x, float y, float w, float h);
    void simpleHexagon(float x, float y, float gs);
    void simpleDiamond(float x, float y, float width, float height);
    
    float getBlockWidth();
    float getBlockPosition();
	float getBorder();

    float initTime;
    float endPosition;
    float position;
    
    int animationTypeIn;
    int animationTypeOut;
    
private:
};

#endif



