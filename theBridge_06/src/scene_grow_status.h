
#ifndef __scene_grow_status__
#define __scene_grow_status__

#include "ofxHapPlayer.h"
#include "Brick.h"
#include "Constantes.h"

#define GROW_STATUS_VIDEO_PATH		"videos/grow_status/grow_status_bridge.mov"

class scene_grow_status {
public:
    void setup(vector<Brick> *bricks, GRID_POSICIONES *pos, int *_secuencia);
	void start(int grow1, int grow2);
    void update();
    void draw(int alpha);
	void keyReleased(int key);

	void	pauseVideo(bool paused);
	void	cueVideo(bool rewind);

private:

	vector<Brick>   *bricks_global;
	GRID_POSICIONES	*posicionesGrid;
	int				*secuenciaPosiciones;

	//vector<Brick>   bricks_local;

	ofxHapPlayer player;

	float initTime;

	float sceneDuration;

	int grow1;
	int grow2;
	
	float percentage;
	
	int lastIndex;

};
#endif /* defined(__example_Simple__FirstScene__) */
