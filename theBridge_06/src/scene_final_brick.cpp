#include "scene_final_brick.h"
#include "Constantes.h"

void scene_final_brick::setup(vector<Brick> *bricks, GRID_POSICIONES *pos, int *_secuencia) {
	this->posicionesGrid = pos;
	this->bricks_global = bricks;
	this->secuenciaPosiciones = _secuencia;
}

void scene_final_brick::start() {
	
	while (bricks_global->size() < BRIDGE_NUM_POSICIONES_GRID)
	{
		int pos;
		if (bricks_global->size() > 0)
			pos = bricks_global->size() - 1;
		else
			pos = 0;
		int gridPos = secuenciaPosiciones[pos];

		ofVec2f initPos = posicionesGrid[gridPos].pos;
		int tipo = ofRandom(0,10);
		//cout << "addBrick: " << i << ": posicion " << gridPos << "(" << initPos << "),tipo= " << tipo << endl;
		Brick *brick = new Brick;
		brick->setup(initPos, tipo, gridPos, 0, 0, ofGetElapsedTimeMillis());
		bricks_global->push_back(*brick);
		bricks_global->back().startInside();
	}
	
	finalBrickVideo.load(FINAL_BRICK_VIDEO_PATH);
	cout << "video cargado: " << FINAL_BRICK_VIDEO_PATH << endl;
	finalBrickVideo.play();
	finalBrickVideo.setLoopState(OF_LOOP_NONE);
	sceneFinished(false);
}

void scene_final_brick::update() {
	finalBrickVideo.update();
	percentage = finalBrickVideo.getPosition();

	if (finalBrickVideo.getIsMovieDone() == 1) {
		ofLog() << "FINAL BRICK MOVIE DONE";
		sceneFinished(true);
	}
}

void scene_final_brick::draw(int alpha) {
	// TODO: DRAW BRICKS
	
	for (int i = 0; i < bricks_global->size(); i++) {
		bricks_global->at(i).draw();
	}

	if (finalBrickVideo.isLoaded()) {
		ofSetColor(255, 255, 255, alpha);
		finalBrickVideo.draw(0, 0, BRIDGE_WIDTH, BRIDGE_HEIGHT);
	}
}

void scene_final_brick::sceneFinished(bool _finished) {
	finished = _finished;
}

float scene_final_brick::getPercentage() {
	return percentage;
}

void scene_final_brick::pauseVideo(bool paused) {
	finalBrickVideo.setPaused(paused);
}

void scene_final_brick::cueVideo(bool rewind) {
	float currentPosition = finalBrickVideo.getPosition();
	if (rewind) {
		currentPosition -= CUE_FOR_TESTING;
	}
	else {
		currentPosition += CUE_FOR_TESTING;
	}
	finalBrickVideo.setPosition(currentPosition);
}