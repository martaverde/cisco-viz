#pragma once

#include "ofxHapPlayer.h"
#include "Brick.h"
#include "Constantes.h"

#define FINAL_BRICK_VIDEO_PATH		"videos/final_brick/final_brick_bridge.mov"

class scene_final_brick {
public:
	void setup(vector<Brick> *bricks, GRID_POSICIONES *pos, int *_secuencia);
	void start();
	void update();
	void draw(int alpha);

	void	pauseVideo(bool paused);
	void	cueVideo(bool rewind);

	void sceneFinished(bool _finished);
	float getPercentage();

private:
	vector<Brick>   *bricks_global;
	GRID_POSICIONES	*posicionesGrid;
	int				*secuenciaPosiciones;

	ofxHapPlayer finalBrickVideo;
	float percentage;

	bool finished;
};