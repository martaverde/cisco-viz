#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

	ofSetVerticalSync(true);

	oscManager.setup();

	mainFBO.allocate(BRIDGE_WIDTH, BRIDGE_HEIGHT, GL_RGBA);
	fboFondo.allocate(BRIDGE_WIDTH, BRIDGE_HEIGHT, GL_RGBA);

	XMLinit.loadFile("mySettings.xml");
	//ofSetFrameRate(60);
	posX = XMLinit.getValue("WINDOW_POSITION", 0);
	posX = 1920;

	ofEnableAntiAliasing();

	ofSetWindowPosition(posX, 0);
	//ofSetWindowPosition(0, 0);

	setupSecuencia();
	setupGrid();
	
	setupScenes();
	setupOSC();
	setupMachineId();

	//SCENES STUFF

	// :: Previous Scene Init ::
	SHOWING_PREVIOUS_SCENE		= false;
	PREVIOUS_SCENE_ID			= -1;
	PREVIOUS_SCENE_START_TIME	= 0.f;
	OVERLAP_TIME_IN_SECS		= 1.5f;
	// :: Overlapping Scenes Init;
	activeVideoPlayerId			= 1;
	activeDataDisplayId			= 1;
	availableVideoPlayerId		= 0;
	availableDataDisplayId		= 0;
	overlappingVideoPlayer		= false;
	overlappingDataDisplay		= false;
	// :: Current Scene Init ::
	SCENE_ID					= 0;
	SCENE_PERCENTAGE			= 0.f;
	// :: Start Scene ::
	startScene(SCENE_ID);
	showPreviousScene(true);

	bChopeoLED = true;

}

//
//void ofApp::loadFondo()
//{
//
//	fboFondo.begin();
//	ofClear(0, 255);
//	int contador = 0;
//	int alpha = 0;
//
//	for (int x = 0; x < NUM_COLUMNAS; x++)
//	{
//		for (int y = 0; y < NUM_FILAS; y++)
//		{
//			ofSetColor(y*paramFondoFila + ofRandom(paramFondoMin, paramFondoMax));
//			ofRectangle r;
//			//cout << y * NUM_COLUMNAS + x << endl;
//			r.setFromCenter(posicionesGridFront[y * NUM_COLUMNAS + x].pos, ancho, ancho);
//			ofDrawRectangle(r);
//		}
//	}
//	fboFondo.end();
//
//}

//--------------------------------------------------------------
void ofApp::setupScenes() {

	sceneVideoPlayer[0].setup();
	sceneVideoPlayer[1].setup();
	sceneDataDisplay[0].setup(&bricks, posicionesGrid, secuenciaPosiciones);
	sceneDataDisplay[1].setup(&bricks, posicionesGrid, secuenciaPosiciones);
	sceneGrowStatus.setup(&bricks, posicionesGrid, secuenciaPosiciones);
	scenePillarDump.setup(&bricks, posicionesGrid, secuenciaPosiciones);
	sceneRecap.setup();
	sceneShutdown.setup(&bricks, posicionesGrid, secuenciaPosiciones);
	sceneFinalBrick.setup(&bricks, posicionesGrid, secuenciaPosiciones);
	sceneInitial.setup();
}

void ofApp::setupMachineId()
{
	//tengo que averiguar que m�quina soy y enviar un init a la CM
	string my_ip;
	system("ipfirst.cmd");
	ofFile file("my.ip");
	file >> my_ip;

	idMachine = 0;

	for (int i = 1; i <=2; i++)
		if (XMLinit.getValue("MAQUINA" + ofToString(i), "") == my_ip)
			idMachine = i;

	cout << "My IP: " << my_ip << ", soy la maquina: " << idMachine << endl;

}

void ofApp::setupOSC()
{

	string REMOTE_IP_CM = XMLinit.getValue("REMOTE_IP_CM", "");
	int REMOTE_PORT_CM = XMLinit.getValue("REMOTE_PORT_CM", 0);

	cout << "Enviando OSC a CM : " << REMOTE_IP_CM << ":" << REMOTE_PORT_CM << "\n";

	oscSenderCM.setup(REMOTE_IP_CM, REMOTE_PORT_CM);
}

//--------------------------------------------------------------
void ofApp::update(){

	oscManager.update();

	// :: Test Loop ::
	if (KEYBOARD_TEST_MODE) {
		if (sceneVideoPlayer[activeVideoPlayerId].finished) {
			testLoopModeChangeVideo();
		}
	}

	// :: Start Scene ::
	if (oscManager.startState && !KEYBOARD_TEST_MODE) {
		// :: Update Bricks? :: PETA!
		
		if (oscManager.updateBricksPendiente) {
			updateBricks(oscManager.getBricks());
			oscManager.updateBricksPendiente = false;
		}
		

		ofLog() << "START STATE :: STATE N -> " + ofToString(oscManager.getState());
		PREVIOUS_SCENE_ID = SCENE_ID;
		SCENE_ID = stateToScene(oscManager.getState());
		ofLog() << "START SCENE :: SCENE N -> " + ofToString(SCENE_ID);
		startScene(SCENE_ID);
		showPreviousScene(true); // esto tiene que ir despues de startScene SIEMPRE
		oscManager.startState = false;
	}

	// :: Previous Scene ::
	if (SHOWING_PREVIOUS_SCENE) {
		// PREVIOUS_SCENE_ALPHA = ofMap(ofGetElapsedTimef(), PREVIOUS_SCENE_START_TIME, PREVIOUS_SCENE_START_TIME + OVERLAP_TIME_IN_SECS, 255, 0);
		PREVIOUS_SCENE_ALPHA = 255;
		if (ofGetElapsedTimef() > PREVIOUS_SCENE_START_TIME + OVERLAP_TIME_IN_SECS) {
			showPreviousScene(false);
		}
		updateScene(PREVIOUS_SCENE_ID, true);
	}
	// :: Current Scene ::
	updateScene(SCENE_ID, false);

	// :: Draw Scene ::
	mainFBO.begin();
		ofBackground(0);
		fboFondo.draw(0, 0);
		if (SHOWING_PREVIOUS_SCENE) {
			drawScene(PREVIOUS_SCENE_ID, true);
		}
		drawScene(SCENE_ID, false);
		//drawGrid();
		ofSetColor(ofRandom(127, 255));
		ofDrawBitmapString(" FPS: " + ofToString(ofGetFrameRate()), 10, 10);
		if (KEYBOARD_TEST_MODE)
			ofSetColor(255, 0, 0);
		else
			ofSetColor(0, 255, 0);
		ofDrawCircle(BRIDGE_WIDTH - 20, 30, 20);
		ofSetColor(255);
	mainFBO.end();
	
	if (ofGetElapsedTimeMillis() - lastPing > INTERVALO_PING)
	{
		lastPing = ofGetElapsedTimeMillis();
		ofxOscMessage m;
		m.setAddress("/ping " + ofToString(idMachine));
		oscSenderCM.sendMessage(m);
	}

	for (int i = 0; i < bricks.size(); i++)
		bricks[i].update();

	oscManager.sendStatePercentage(SCENE_PERCENTAGE);
}

void ofApp::setupSecuencia()
{
	int numPosiciones = XMLinit.getNumTags("SECUENCIA_BRIDGE");

	if (numPosiciones > 0)
	{
		for (int i = 0; i < numPosiciones; i++)
		{
			string churro = XMLinit.getValue("SECUENCIA_BRIDGE", "", i);
			if (churro == "")
				cout << "ERROR: secuencia de posiciones vacia" << endl;
			else
			{
				vector<string> p = ofSplitString(churro, ",");
				if (p.size() != BRIDGE_NUM_POSICIONES_GRID)
					cout << "ERROR: la secuencia " << i << " no tiene " << BRIDGE_NUM_POSICIONES_GRID << " posiciones" << endl;
				else
				{
					for (int a = 0; a < p.size(); a++)
						secuenciaPosiciones[a] = ofToInt(p[a]);
						//secuenciaPosiciones[a] = (BRIDGE_NUM_POSICIONES_GRID - 1) - ofToInt(p[a]);
				}
			}

		}
	}
	else
		cout << "ERROR: no hay sencuencias de posiciones del Bridge en el xml" << endl;

}

//--------------------------------------------------------------
void ofApp::setupGrid() {

	int contador = 0;
	fboFondo.begin();
		ofClear(0, 255);
		for (int fila = 0; fila < 12; fila++)
			for (int columna = 0; columna < 84; columna++)
			{
				posicionesGrid[contador++].pos = ofVec2f((ANCHO_BRICK / 2.0f) + columna * ANCHO_BRICK, (ANCHO_BRICK / 2.0f) + fila * ANCHO_BRICK);
				ofRectangle r;
				r.setFromCenter(posicionesGrid[contador - 1].pos, ANCHO_BRICK, ANCHO_BRICK);
				ofSetColor(fila * 2 + ofRandom(0, 11));
				ofDrawRectangle(r);
			}
		for (int fila = 12; fila < 18; fila++)
		{
			for (int columna = 0; columna < 18; columna++)
			{
				posicionesGrid[contador++].pos = ofVec2f((ANCHO_BRICK / 2.0f) + columna * ANCHO_BRICK, (ANCHO_BRICK / 2.0f) + fila * ANCHO_BRICK);
				ofRectangle r;
				r.setFromCenter(posicionesGrid[contador - 1].pos, ANCHO_BRICK, ANCHO_BRICK);
				ofSetColor(fila * 2 + ofRandom(0, 11));
				ofDrawRectangle(r);

			}
			for (int columna = 66; columna < 84; columna++)
			{
				posicionesGrid[contador++].pos = ofVec2f((ANCHO_BRICK / 2.0f) + columna * ANCHO_BRICK, (ANCHO_BRICK / 2.0f) + fila * ANCHO_BRICK);
				ofRectangle r;
				r.setFromCenter(posicionesGrid[contador - 1].pos, ANCHO_BRICK, ANCHO_BRICK);
				ofSetColor(fila * 2 + ofRandom(0, 11));
				ofDrawRectangle(r);
			}
		}

		for (int fila = 18; fila < 24; fila++)
		{
			for (int columna = 0; columna < 9; columna++)
			{
				posicionesGrid[contador++].pos = ofVec2f((ANCHO_BRICK / 2.0f) + columna * ANCHO_BRICK, (ANCHO_BRICK / 2.0f) + fila * ANCHO_BRICK);
				ofRectangle r;
				r.setFromCenter(posicionesGrid[contador - 1].pos, ANCHO_BRICK, ANCHO_BRICK);
				ofSetColor(fila * 2 + ofRandom(0, 11));
				ofDrawRectangle(r);
			}
			for (int columna = 75; columna < 84; columna++)
			{
				posicionesGrid[contador++].pos = ofVec2f((ANCHO_BRICK / 2.0f) + columna * ANCHO_BRICK, (ANCHO_BRICK / 2.0f) + fila * ANCHO_BRICK);
				ofRectangle r;
				r.setFromCenter(posicionesGrid[contador - 1].pos, ANCHO_BRICK, ANCHO_BRICK);
				ofSetColor(fila * 2 + ofRandom(0, 11));
				ofDrawRectangle(r);
			}
		}
	fboFondo.end();

	secuenciaPosicionesActual = 0;
	for (int i = 0; i < 500; i++)
	{
		int gridPos = secuenciaPosiciones[secuenciaPosicionesActual];

		ofVec2f initPos = posicionesGrid[gridPos].pos;
		int tipo = ofRandom(0,10);
		//cout << "addBrick: " << i << ": posicion " << gridPos << "(" << initPos << "),tipo= " << tipo << endl;
		Brick *brick = new Brick;
		brick->setup(initPos, tipo, gridPos, 0, 0, ofGetElapsedTimeMillis());
		bricks.push_back(*brick);
		bricks.back().addAnimation(initPos, Brick::ANIMACION_BRICK::CREACION, 0);
		bricks.back().startAnimation();
		bricks.back().startInside();
		secuenciaPosicionesActual++;

	}
}

void ofApp::updateBricks(vector<int> bricksType) {
	bricks.clear();
	secuenciaPosicionesActual = 0;
	cout << "Actualizando " << bricksType.size() << " Bricks" << endl;
	for (int i = 0; i < bricksType.size(); i++) {
		int gridPos = secuenciaPosiciones[secuenciaPosicionesActual];
		posicionesGrid[gridPos].tipoBrick = bricksType[i];
		if (bricksType[i] != -1)
		{
			ofVec2f initPos = posicionesGrid[gridPos].pos;
			Brick *brick = new Brick;
			brick->setup(initPos, bricksType[i], gridPos, 0, 0, ofGetElapsedTimeMillis());
			bricks.push_back(*brick);
			bricks.back().startInside();
		}
		//bricks.back().addAnimation(initPos, Brick::ANIMACION_BRICK::CREACION, 0);
		secuenciaPosicionesActual++;
		if (secuenciaPosicionesActual == BRIDGE_NUM_POSICIONES_GRID)
			secuenciaPosicionesActual = 0;
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	ofBackground(0);
	float aspect = (float)BRIDGE_WIDTH / (float)BRIDGE_HEIGHT;

	//mainFBO.draw(0, 0, 3840, 3840 / aspect);
	if (bChopeoLED)
	{
		//mainFBO.getTexture().drawSubsection(0, 0, 1920, 1080, 0, 0); //trozo 1
		//mainFBO.getTexture().drawSubsection(1920, 0, 1920, 1080, 1920, 0); // trozo 2
		//mainFBO.getTexture().drawSubsection(1920 * 2, 0, 864, 1080, 1920 * 2, 0); // trozo 2
		//mainFBO.getTexture().drawSubsection(1920 * 2 + 864, 0, 504, 264, 0, 1080);
		//mainFBO.getTexture().drawSubsection(1920 * 2 + 864, 264, 504, 264, 4200, 1080);
		//dondedibuja X, dondedibuja Y, tam��oX, tama�oY, texturaX, texturaY
		mainFBO.getTexture().drawSubsection(0, 0, 1848, 1008, 0, 0); //trozo 1
		mainFBO.getTexture().drawSubsection(1920, 0, 1848, 1008, 1848, 0); // trozo 2
		mainFBO.getTexture().drawSubsection(1920 * 2, 0, 1008, 1008, 1848 * 2, 0); // trozo 3
		mainFBO.getTexture().drawSubsection(1920 * 2 + 1008, 336, 504, 336, 0, 1008);
		mainFBO.getTexture().drawSubsection(1920 * 2 + 1008, 0, 504, 336, 4200, 1008);


	}
	else
	{
		mainFBO.draw(0, 0, 1920, 1920 / aspect);

	}

}

//--------------------------------------------------------------
void ofApp::drawGrid() {
	ofBackground(0);
	ofSetColor(255);
	for (int i = 0; i < BRIDGE_NUM_POSICIONES_GRID; i++)
		ofDrawBitmapString(ofToString(i), posicionesGrid[i].pos);
}


// :::::::: SCENES :::::::::
int ofApp::stateToScene(int stateId) {
	int sceneId;
	if (stateId == 0) {
		sceneId = 7;
	}
	else if (stateId == 1 || stateId == 2 || stateId == 6 || stateId == 7 || stateId == 9 || stateId == 10) {
		sceneId = 0;
	}
	else if (stateId == 3) {
		sceneId = 1;
	}
	else if (stateId == 4) {
		sceneId = 2;
	}
	else if (stateId == 5) {
		sceneId = 3;
	}
	else if (stateId == 8) {
		sceneId = 4;
	}
	else if (stateId == 11) {
		sceneId = 5;
	}
	else if (stateId == 12) {
		sceneId = 6;
	}
	return sceneId;
}

void ofApp::startScene(int sceneId) {

	if (KEYBOARD_TEST_MODE == false)
		if (!sanityCheck(sceneId))
		{
			cout << "sanity check ha fallado -> activa screen saver" << endl;
			oscManager.dataPath = "/videos/screensaver/salva_welcome.mov";
			sceneId = 0;
		}
	
	//si llega una escena 7, la cambio por 0
	if (sceneId == 7)
	{
		oscManager.dataPath = "/videos/screensaver/salva_welcome.mov";
		oscManager.stateId = 10;
		oscManager.stateType = 0;
		sceneId = 0;
	}

	cout << "---------------------------------------------" << endl;

	if (sceneId == 0) {
		ofLog() << "Start Video Player :: INDEX " + ofToString(availableVideoPlayerId);
		if (KEYBOARD_TEST_MODE) {
			sceneVideoPlayer[availableVideoPlayerId].start( testState, testStateType, testPath );
		}
		else {
			sceneVideoPlayer[availableVideoPlayerId].start(
				oscManager.getState(),
				oscManager.getStateType(),
				oscManager.getPath()
			);
		}
	}
	else if (sceneId == 1) {
		ofLog() << "Start Data Display :: INDEX " + ofToString(availableDataDisplayId);
		if (KEYBOARD_TEST_MODE) {
			sceneDataDisplay[availableDataDisplayId].start( testStateType, testTopicId, testCopy, testHighlights, testFact, testPath , secuenciaPosicionesActual);
			if (testStateType==0 || testStateType==2)
				secuenciaPosicionesActual++;
		}
		else {
			sceneDataDisplay[availableDataDisplayId].start(
				oscManager.getStateType(),
				oscManager.getTopicId(),
				oscManager.getCopy(),
				"",
				//oscManager.getHighlights(),
				oscManager.getFactText(),
				oscManager.getPath(),
				secuenciaPosicionesActual
			);
			if (oscManager.getStateType() == 0 || oscManager.getStateType() == 2)
				secuenciaPosicionesActual++;


		}
		//sceneDataDisplay.start(
		//	0, //0-1
		//	0,//0-9
		//	"hola que tal esto es un texto cualquiera para ver si funciona",
		//	"2,3//5,6",
		//	"/videos/data_display/security/featured/"
		//);
	}
	else if (sceneId == 2) {
		if (KEYBOARD_TEST_MODE) {
			sceneGrowStatus.start( 100, 400 );
		}
		else {
			sceneGrowStatus.start(
				oscManager.getGrow1(),
				oscManager.getGrow2()
			);
		}
	}
	else if (sceneId == 3) {
		scenePillarDump.start( secuenciaPosicionesActual );
		secuenciaPosicionesActual++;
	}
	else if (sceneId == 4) {
		if (KEYBOARD_TEST_MODE) {
			sceneRecap.start( testStateType, testRecap );
		}
		else {
			sceneRecap.start(
				oscManager.getStateType(),
				oscManager.getCopy()
			);
		}
	}
	else if (sceneId == 5) {
		//sceneShutdown.start( oscManager.getCurrentBricksPos() );
		sceneShutdown.start();
		// sceneShutdown.start(400);
	}
	else if (sceneId == 6) {
		sceneFinalBrick.start();
	}
	else if (sceneId == 7) {
		sceneInitial.start();
	}
}


bool ofApp::sanityCheck(int sceneId)
{
	
	bool resultado = true;
	cout << "******************************" << endl;
	cout << "sanity check escena " << sceneId << endl;
	if (sceneId == 0)
	{
		if (oscManager.getState() < 0)
		{
			cout << "getState < 0" << endl;
			resultado = false;
		}
		if (oscManager.getStateType() < 0)
		{
			cout << "getStatetype < 0" << endl;
			resultado = false;
		}
		//if (oscManager.getPath().length() < 5)
		//{
		//	cout << "path Length < 5" << endl;
		//	resultado = false;
		//}
	}

	if (sceneId == 1)
	{
		if (oscManager.getStateType() < 0)
		{
			cout << "getStatetype < 0" << endl;
			resultado = false;
		}
		if (oscManager.getPath().length() < 5)
		{
			cout << "path Length < 5" << endl;
			resultado = false;
		}
		if (oscManager.getTopicId() < 0)
		{
			cout << "topicId < 0" << endl;
			resultado = false;
		}		
		if (oscManager.getCopy().length() < 5)
		{
			cout << "copy Length < 5" << endl;
			resultado = false;
		}		
		//if (oscManager.getHighlights().length() < 5)
		//{
		//	cout << "get highlights Length < 5" << endl;
		//	resultado = false;
		//}		
		//if (oscManager.getFactText().length() < 5)
		//{
		//	cout << "fact text  Length < 5" << endl;
		//	resultado = false;
		//}
	}

	//grow status
	if (sceneId == 2)
	{
		if (oscManager.getGrow1() <= 0)
		{
			cout << "grow1  <= 0" << endl;
			resultado = false;
		}
		if (oscManager.getGrow1() > bricks.size())
		{
			cout << "FAKE:  grow1:" << oscManager.getGrow1() << " > numero bricks" << endl;
			oscManager.grow1 = (float)bricks.size()*0.5f;
			resultado = true;
		}
		if (oscManager.getGrow2() <= 0)
		{
			cout << "grow2 <=" << endl;
			resultado = false;
		}
		if (oscManager.getGrow2() > bricks.size())
		{
			cout << "FAKE:  grow2:" << oscManager.getGrow2() << " > numero bricks" << endl;
			oscManager.grow2 = bricks.size();
			resultado = true;
		}
	}
		

	if (sceneId == 4)
	{
		if (oscManager.getStateType() < 0)
		{
			cout << "getStatetype < 0" << endl;
			resultado = false;
		}
		if (oscManager.getCopy().length() < 5)
		{
			cout << "copy Length < 5" << endl;
			resultado = false;
		}
	}
	cout << "Fin sanity check escena " << sceneId << ", resultado = " << resultado << endl;
	return resultado;
}



void ofApp::showPreviousScene(bool show) {
	if (PREVIOUS_SCENE_ID != -1)	SHOWING_PREVIOUS_SCENE = show;
	if (show) {
		PREVIOUS_SCENE_START_TIME = ofGetElapsedTimef();
		int activeNow;
		int availableNow;
		if (SCENE_ID == 0) {
			activeNow				= activeVideoPlayerId;
			availableNow			= availableVideoPlayerId;
			activeVideoPlayerId		= availableNow;
			availableVideoPlayerId	= activeNow;
			if (SCENE_ID == PREVIOUS_SCENE_ID) { 
				overlappingVideoPlayer = true; 
			}
		}
		else if (SCENE_ID == 1) {
			activeNow				= activeDataDisplayId;
			availableNow			= availableDataDisplayId;
			activeDataDisplayId		= availableNow;
			availableDataDisplayId	= activeNow;
			if (SCENE_ID == PREVIOUS_SCENE_ID) {
				overlappingDataDisplay = true;
			}
		}
	}
	else {
		if (SCENE_ID == PREVIOUS_SCENE_ID) {
			if (SCENE_ID == 0) {
				overlappingVideoPlayer	= false;
			}
			else if (SCENE_ID == 1) {
				overlappingDataDisplay	= false;
			}
		}
	}
}

void ofApp::updateScene(int sceneId, bool isPrevious) {
	
	if (sceneId == 0) {
		if (isPrevious) {
			sceneVideoPlayer[availableVideoPlayerId].update();
		}
		else {
			sceneVideoPlayer[activeVideoPlayerId].update();
			SCENE_PERCENTAGE = sceneVideoPlayer[activeVideoPlayerId].getPercentage();
		}
	}
	else if (sceneId == 1) {
		if (isPrevious) {
			sceneDataDisplay[availableDataDisplayId].update();
		}
		else {
			sceneDataDisplay[activeDataDisplayId].update();
			SCENE_PERCENTAGE = sceneDataDisplay[activeDataDisplayId].getPercentage();
		}
	}
	else if (sceneId == 2) {
		sceneGrowStatus.update();
	}
	else if (sceneId == 3) {
		scenePillarDump.update();
	}
	else if (sceneId == 4) {
		sceneRecap.update();
	}
	else if (sceneId == 5) {
		sceneShutdown.update();
	}
	else if (sceneId == 6) {
		sceneFinalBrick.update();
	}
	else if (sceneId == 7) {
		sceneInitial.update();
		SCENE_PERCENTAGE = sceneInitial.getPercentage();
	}
}

void ofApp::drawScene(int sceneId, bool isPrevious) {
	int alphaDraw;
	if (isPrevious) {
		alphaDraw = PREVIOUS_SCENE_ALPHA;
	}
	else {
		alphaDraw = 255;
	}
	if (sceneId == 0) {
		if (isPrevious) {
			sceneVideoPlayer[availableVideoPlayerId].draw(PREVIOUS_SCENE_ALPHA);
		}
		else {
			sceneVideoPlayer[activeVideoPlayerId].draw(255);
		}
	}
	else if (sceneId == 1) {
		if (isPrevious) {
			sceneDataDisplay[availableDataDisplayId].draw(PREVIOUS_SCENE_ALPHA);
		}
		else {
			sceneDataDisplay[activeDataDisplayId].draw(255);
		}
	}
	else if (sceneId == 2) {
		sceneGrowStatus.draw(alphaDraw);
	}
	else if (sceneId == 3) {
		scenePillarDump.draw(alphaDraw);
	}
	else if (sceneId == 4) {
		sceneRecap.draw(alphaDraw);
	}
	else if (sceneId == 5) {
		sceneShutdown.draw(alphaDraw);
	}
	else if (sceneId == 6) {
		sceneFinalBrick.draw(alphaDraw);
	}
	else if (sceneId == 7) {
		sceneInitial.draw(alphaDraw);
	}
}
// :::::::: SCENES :::::::::


//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (key == ' ')
		KEYBOARD_TEST_MODE = !KEYBOARD_TEST_MODE;
	
	if (key == '0')
	{
		// sceneVideoPlayer
		SCENE_ID = 0;
		startScene(SCENE_ID);
	}
	if (key == '1')
	{
		// sceneDataDisplay
		SCENE_ID = 1;
		startScene(SCENE_ID);
	}
	if (key == '2')
	{
		// sceneGrowStatus
		SCENE_ID = 2;
		startScene(SCENE_ID);
	}
	if (key == '3')
	{
		// scenePillarDump
		SCENE_ID = 3;
		startScene(SCENE_ID);
	}
	if (key == '4')
	{
		if (KEYBOARD_TEST_MODE) {
			// :: STATE 8 :: RECAP ::
			tRecap_idx = updateTestIdx(tRecap_idx, tRecap_vids);
			testState = 8;
			testStateType = tRecap_idx;
			ofBuffer buffer = ofBufferFromFile("lorem.txt");
			testRecap = buffer.getText();
			testStartScene(4);
		}
		else {
			// sceneRecap
			SCENE_ID = 4;
			startScene(SCENE_ID);
		}
	}
	if (key == '5')
	{
		// sceneShutdown
		SCENE_ID = 5;
		startScene(SCENE_ID);
	}
	if (key == '6')
	{
		// sceneFinalBrick
		SCENE_ID = 6;
		startScene(SCENE_ID);
	}
	if (key == '7')
	{
		// sceneInitial
		SCENE_ID = 7;
		startScene(SCENE_ID);
	}

	if (key == 'c')
		bChopeoLED = !bChopeoLED;


	// :: TEST SCENES ::
	if (KEYBOARD_TEST_MODE) {
		if (key == 'q') {
			// :: STATE 1 :: WELCOME ::
			tWelcome_idx = updateTestIdx(tWelcome_idx, tWelcome_vids);
			testState = 1;
			testStateType = tWelcome_idx;
			testPath = "";
			testStartScene(0);
		}
		if (key == 'w') {
			// :: STATE 2 :: ADVERTISE ::
			tAdvertise_idx = updateTestIdx(tAdvertise_idx, tAdvertise_vids);
			testState = 2;
			testStateType = tAdvertise_idx;
			testPath = "";
			testStartScene(0);
		}
		if (key == 'e') {
			// :: STATE 6 :: CALL TO ACTION ::
			testState = 6;
			testStateType = 0;
			testPath = "";
			testStartScene(0);
		}
		if (key == 'r') {
			// :: STATE 7 :: CLAIM ::
			testState = 7;
			testStateType = 0;
			testPath = "";
			testStartScene(0);
		}
		if (key == 't') {
			// :: STATE 9 :: KEYNOTE ::
			tKeynote_idx = updateTestIdx(tKeynote_idx, tKeynote_vids);
			testState = 9;
			testStateType = tKeynote_idx;
			testPath = "";
			testStartScene(0);
		}
		if (key == 'y') {
			// :: STATE 10 :: SCREENSAVER ::
			tScreensaver_idx = updateTestIdx(tScreensaver_idx, tScreensaver_vids);
			testState = 10;
			testStateType = tScreensaver_idx;
			testPath = "";
			testStartScene(0);
		}
		if (key == 'f') {
			// :: STATE 2 :: DATA DISPLAY :: FEATURED ::
			testState = 3;
			testStateType = 0;
			testTopicId = (int)ofRandom(0, 10);
			if (testTopicId > 9) {
				testTopicId = 9;
			}
			//testCopy = "Every participant connects an average of mmxxx ms|Every participant connects an average of mmxxx ms|Every participant connects an average of mmxxx ms";
			testCopy = "This week alone,|there have been <350 #CLEUR>|posts about Cisco Live on social media";
			//testCopy = "Every participant connects |There were xxx connections|y esta es corta";
			testHighlights = "";
			testFact = "32,536|Online views";
			testPath = "";
			testStartScene(1);
		}
		if (key == 's') {
			// :: STATE 2 :: DATA DISPLAY :: SECONDARY  ::
			testState = 3;
			testStateType = 1;
			testTopicId = (int)ofRandom(0, 10);
			if (testTopicId > 9) {
				testTopicId = 9;
			}
			testCopy = "Every participant connects an average of mmxxx ms|Every participant connects an average of mmxxx ms|Every participant connects an average of mmxxx ms";
			testCopy = "Every participant connects an average of mmxxx ms|There were xxx connections";
			testHighlights = "";
			testFact = "32,536|Online views";
			testPath = "";
			testStartScene(1);
		}
		if (key == 'm') {
			// :: STATE 2 :: DATA DISPLAY :: MILESTONE  ::
			testState = 3;
			testStateType = 2;
			testTopicId = (int)ofRandom(0, 10);
			if (testTopicId > 9) {
				testTopicId = 9;
			}
			testCopy = "Every participant connects an average of mmxxx ms|Every participant connects an average of mmxxx ms|Every participant connects an average of mmxxx ms";
			testCopy = "una linea muy corta";
			testHighlights = "";
			testFact = "32,536|Online views";
			testPath = "";
			testStartScene(1);
		}

		if (key == OF_KEY_LEFT || key == OF_KEY_RIGHT || key == OF_KEY_DOWN || key == OF_KEY_UP) {
			testManageVideos(key);
		}
	}
	
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}


// :: TEST SCENES ::
void ofApp::testStartScene(int sceneId) {
	ofLog() << "START STATE :: STATE N -> " + ofToString(testState);
	PREVIOUS_SCENE_ID = SCENE_ID;
	SCENE_ID = sceneId;
	ofLog() << "START SCENE :: SCENE N -> " + ofToString(SCENE_ID);
	startScene(SCENE_ID);
	showPreviousScene(true); // esto tiene que ir despues de startScene SIEMPRE
}

int ofApp::updateTestIdx(int idx, int limit) {
	if (idx + 1 < limit) {
		idx += 1;
	}
	else {
		idx = 0;
	}
	return idx;
}

void ofApp::testManageVideos(int key) {
	if (SCENE_ID == 0) {
		if (key == OF_KEY_LEFT) {
			sceneVideoPlayer[activeVideoPlayerId].cueVideo(true);
		}
		else if (key == OF_KEY_RIGHT) {
			sceneVideoPlayer[activeVideoPlayerId].cueVideo(false);
		}
		else if (key == OF_KEY_UP) {
			sceneVideoPlayer[activeVideoPlayerId].pauseVideo(false);
		}
		else if (key == OF_KEY_DOWN) {
			sceneVideoPlayer[activeVideoPlayerId].pauseVideo(true);
		}
	}
	else if (SCENE_ID == 1) {
		if (key == OF_KEY_LEFT) {
			sceneDataDisplay[activeDataDisplayId].cueVideo(true);
		}
		else if (key == OF_KEY_RIGHT) {
			sceneDataDisplay[activeDataDisplayId].cueVideo(false);
		}
		else if (key == OF_KEY_UP) {
			sceneDataDisplay[activeDataDisplayId].pauseVideo(false);
		}
		else if (key == OF_KEY_DOWN) {
			sceneDataDisplay[activeDataDisplayId].pauseVideo(true);
		}
	}
	else if (SCENE_ID == 2) {
		if (key == OF_KEY_LEFT) {
			sceneGrowStatus.cueVideo(true);
		}
		else if (key == OF_KEY_RIGHT) {
			sceneGrowStatus.cueVideo(false);
		}
		else if (key == OF_KEY_UP) {
			sceneGrowStatus.pauseVideo(false);
		}
		else if (key == OF_KEY_DOWN) {
			sceneGrowStatus.pauseVideo(true);
		}
	}
	else if (SCENE_ID == 3) {
		if (key == OF_KEY_LEFT) {
			scenePillarDump.cueVideo(true);
		}
		else if (key == OF_KEY_RIGHT) {
			scenePillarDump.cueVideo(false);
		}
		else if (key == OF_KEY_UP) {
			scenePillarDump.pauseVideo(false);
		}
		else if (key == OF_KEY_DOWN) {
			scenePillarDump.pauseVideo(true);
		}
	}
	else if (SCENE_ID == 4) {
		if (key == OF_KEY_LEFT) {
			sceneRecap.cueVideo(true);
		}
		else if (key == OF_KEY_RIGHT) {
			sceneRecap.cueVideo(false);
		}
		else if (key == OF_KEY_UP) {
			sceneRecap.pauseVideo(false);
		}
		else if (key == OF_KEY_DOWN) {
			sceneRecap.pauseVideo(true);
		}
	}
	else if (SCENE_ID == 6) {
		if (key == OF_KEY_LEFT) {
			sceneFinalBrick.cueVideo(true);
		}
		else if (key == OF_KEY_RIGHT) {
			sceneFinalBrick.cueVideo(false);
		}
		else if (key == OF_KEY_UP) {
			sceneFinalBrick.pauseVideo(false);
		}
		else if (key == OF_KEY_DOWN) {
			sceneFinalBrick.pauseVideo(true);
		}
	}
}

void ofApp::testLoopModeChangeVideo() {
	tLoopMode_idx = updateTestIdx(tLoopMode_idx, tLoopMode_max);
	cout << "Loop Mode IDX -> " << ofToString(tLoopMode_idx) << endl;
	if (tLoopMode_idx == 0) {
		// :: STATE 1 :: WELCOME ::
		tWelcome_idx = updateTestIdx(tWelcome_idx, tWelcome_vids);
		testState = 1;
		testStateType = tWelcome_idx;
		testPath = "";
		testStartScene(0);
	}
	if (tLoopMode_idx == 1) {
		// :: STATE 2 :: ADVERTISE ::
		tAdvertise_idx = updateTestIdx(tAdvertise_idx, tAdvertise_vids);
		testState = 2;
		testStateType = tAdvertise_idx;
		testPath = "";
		testStartScene(0);
	}
	if (tLoopMode_idx == 2) {
		// :: STATE 6 :: CALL TO ACTION ::
		testState = 6;
		testStateType = 0;
		testPath = "";
		testStartScene(0);
	}
	if (tLoopMode_idx == 3) {
		// :: STATE 7 :: CLAIM ::
		testState = 7;
		testStateType = 0;
		testPath = "";
		testStartScene(0);
	}
	if (tLoopMode_idx == 4) {
		// :: STATE 9 :: KEYNOTE ::
		tKeynote_idx = updateTestIdx(tKeynote_idx, tKeynote_vids);
		testState = 9;
		testStateType = tKeynote_idx;
		testPath = "";
		testStartScene(0);
	}
	if (tLoopMode_idx == 5) {
		// :: STATE 10 :: SCREENSAVER ::
		tScreensaver_idx = updateTestIdx(tScreensaver_idx, tScreensaver_vids);
		testState = 10;
		testStateType = tScreensaver_idx;
		testPath = "";
		testStartScene(0);
	}
}