
#ifndef __example_Simple__ZeroScene__
#define __example_Simple__ZeroScenee__

#include "ofxHapPlayer.h"

class scene_0_initial {
public:
	void setup();
	void start();
	void update();
	void draw(int alpha);

	float	getPercentage();

	float startTime;
	float percentage;
	float sceneDuration;
	ofImage fondo;
};

#endif /* defined(__example_Simple__FirstScene__) */