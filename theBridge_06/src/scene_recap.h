
#ifndef __scene_recap__
#define __scene_recap__

#include "ofxHapPlayer.h"
#include "recapBlock.h"
#include "Constantes.h"
#include "ofxEasing2.h"

#define HOUR_RECAP_VIDEO_PATH		"videos/recap/recap_intro_bridge.mov"
#define DAILY_RECAP_VIDEO_PATH		"videos/recap/daily_recap_intro_bridge.mov"


class scene_recap {
	
public:
		void setup			( );
		void update			( );
		void draw			( int alpha );
		void parseaRecap	( string churro );
		void start			( int tipo, string churro );

		void	pauseVideo(bool paused);
		void	cueVideo(bool rewind);

private:

		void addRecap(int tipo, string texto);

		bool showingRecapLines;
		int minRecapsPerLine;
		int nRecapLines;
		float standardVariationX;
		float timeBetweenJumps;
		float lastJumpTime;
		bool jumpNow;

		double timeIni;

		string copy;

		ofTrueTypeFont ciscoSansBold;
		string dataString;

		string testString;

		float timeNow;

		int tempTypeBlock;

		struct LINEA {
			int posY;
			int posXInicial;				//donde va el primer recap
			int posXSiguiente;				//donde colocar el siguiente recap
			float posXAnimacion;			//donde esta la x inicial durante la animación
			float speed;
			vector <recapBlock> recapBlocks;
		};

		vector <LINEA> lineas;
		int ancho;
		int lineaRecapActual = 0;
		float maxLongitud;

		string recap;

		vector <string> stringGroup;

	    int recapType;
		float counter;
		float sceneDuration;

	    ofxHapPlayer video;
		string actualVideo;

		float posYanterior = 0;
		double timeStartJump;

		ofColor colores[4];
		int colorActual;
		double lastColorChange;


};

#endif /* defined(__example_Simple__FirstScene__) */
