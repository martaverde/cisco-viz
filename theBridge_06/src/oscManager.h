#pragma once

#include "ofxOsc.h"
#include "ofxXmlSettings.h"


//#define LOCAL_PORT 12345
//#define REMOTE_IP "localhost"
//#define REMOTE_PORT 20001

class oscManager{
public:
    void setup();
    void update();

	// :: SETUP ::
	ofxXmlSettings XML;
	int LOCAL_PORT;
	string REMOTE_IP;
	int REMOTE_PORT;
	int MACHINE;
    
    // Receive OSC
    void receiveMessages();
	ofxOscReceiver receiver_;
	
    // Send OSC
    void okConnected();
    void okStartState();
    void okNextState();
    void sendResponse(string response);
    void sendStatePercentage(float percent);
    void sendStateIsFinished();
    ofxOscSender sender;
    
	// :: NEXT STATE ::
    int stateId;
    int stateType;
    int topicId;
    string copy;
	string highlight;
	string factText;
    string dataPath;

	// :: START STATE ::
	bool startState;
	bool updateBricksPendiente;

	// :: STATE SPECIFIC ::
	// :: Recap ::
	int recapType;
	string strRecap;
	// :: Keynote ::
	int keynote;
	// :: Screensaver ::
	int screensaver;
	// :: Welcome ::
	int welcome;
	// :: Advertise ::
	int advertise;
	// :: Grow ::
	int grow1;
	int grow2;
	// :: Bricks ::
	string bricksString;
	vector<int> bricks;
	// :: Dump ::
	// :: Final Brick ::
	// :: Shut Down ::
	int currentBricksPos;
	// :: Milestone ?

	// :: OTHER MSG ::
    string otherMsg;

	// :: GET VARIABLES ::
	int getState();
	int getStateType();
	int getTopicId();
	string getCopy();
	string getRecapString();
	int getRecapType();
	int getFamily();
	string getPath();
	string getHighlights();
	string getFactText();
	int getScreensaver();
	int getKeynote();
	int getWelcome();
	int getAdvertise();
	int getGrow1();
	int getGrow2();
	vector<int> getBricks();
	int getCurrentBricksPos();
};
