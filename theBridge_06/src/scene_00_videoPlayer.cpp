#include "scene_00_videoPlayer.h"
#include "Constantes.h"
#include "scene_0_initial.h"

void scene_00_videoPlayer::setup() {
}

void scene_00_videoPlayer::start(int stateId, int stateType, string _path) {
    // path = _path;
	ofFile videoFile(ofToDataPath(_path));
	if (videoFile.isFile()) {
		path = _path;
	}
	else {
		path = stateToPath(stateId, stateType);
	}
	cout << "desde la escena, path= " << path << endl;
	video.load(path);
	video.play();
	video.setLoopState(OF_LOOP_NONE);
	if (FAST_MODE) {
		video.setSpeed(4.0);
	}
	duration = video.getDuration();
	sceneFinished(false);
}

void scene_00_videoPlayer::update() {
	video.update();
	percentage = video.getPosition();

	if (video.getIsMovieDone() == 1) {
		sceneFinished(true);
	}
}

void scene_00_videoPlayer::draw(int alpha) {

	if (video.isLoaded()) {
		ofSetColor(255, 255, 255, alpha);
		video.draw(0, 0, BRIDGE_WIDTH, BRIDGE_HEIGHT);
	}

	ofSetColor(255);

	/*
	ofDrawBitmapString(percentage, 20, 200);
	ofDrawBitmapString(path, 20, 400);
	// Draw the FPS display
	ofSetColor(255);
	ofDrawBitmapString(ofToString(ofGetFrameRate(), 0) + " FPS", 20, 20);
	*/

}

string scene_00_videoPlayer::stateToPath(int stateId, int stateType) {
	string path = "videos/";
	if (stateId == 1) {
		path += "welcome/";
		if (stateType == 0) {
			path += "welcome_1_bridge.mov";
		}
		else if (stateType == 1) {
			path += "welcome_2_bridge.mov";
		}
	}
	else if (stateId == 2) {
		path += "advertise/";
		if (stateType == 0) {
			path += "ad_cisco_bridge.mov";
		}
		else if (stateType == 1) {
			path += "ad_imagine_bridge.mov";
		}
	}
	else if (stateId == 6) {
		path += "call_to_action/call_to_action_bridge.mov";
	}
	else if (stateId == 7) {
		path += "claim/claim_bridge.mov";
	}
	else if (stateId == 9) {
		path += "keynote/";
		if (stateType == 0) {
			path += "bok_bridge.mov";
		}
		else if (stateType == 1) {
			path += "aok_bridge.mov";
		}
		else if (stateType == 2) {
			path += "bgk_bridge.mov";
		}
	}
	else if (stateId == 10) {
		path += "screensaver/";
		if (stateType == 0) {
			path += "salva_welcome.mov";
		}
		else if (stateType == 1) {
			path += "salva_closing.mov";
		}
		else if (stateType == 2) {
			path += "salva_bok.mov";
		}
		else if (stateType == 3) {
			path += "salva_aok.mov";
		}
		else if (stateType == 4) {
			path += "salva_bgk.mov";
		}
		else if (stateType == 5) {
			path += "salva_final.mov";
		}
		else if (stateType == 6) {
			path += "salva_friday.mov";
		}
	}
	return path;
}

void scene_00_videoPlayer::sceneFinished(bool _finished) {
	finished = _finished;
}

float scene_00_videoPlayer::getPercentage() {
	return percentage;
}

void scene_00_videoPlayer::pauseVideo(bool paused) {
	video.setPaused(paused);
}

void scene_00_videoPlayer::cueVideo(bool rewind) {
	float currentPosition = video.getPosition();
	if (rewind) {
		currentPosition -= CUE_FOR_TESTING;
	}
	else {
		currentPosition += CUE_FOR_TESTING;
	}
	video.setPosition(currentPosition);
}