#pragma once

#include "ofMain.h"
#include "ofxXmlSettings.h"
#include "Constantes.h"
#include "oscManager.h"
#include "Brick.h"
#include "scene_0_initial.h"
#include "scene_00_videoPlayer.h"
#include "scene_data_display.h"
#include "scene_pillar_dump.h"
#include "scene_grow_status.h"
#include "scene_recap.h"
#include "scene_shutdown.h"
#include "scene_final_brick.h"

#define INTERVALO_PING 1000

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		oscManager oscManager;
		
	private:
		
		bool					sanityCheck(int escenaID);

		scene_00_videoPlayer	sceneVideoPlayer[2];	// 0
		scene_data_display		sceneDataDisplay[2];	// 1
		scene_grow_status		sceneGrowStatus;		// 2
		scene_pillar_dump		scenePillarDump;		// 3
		scene_recap				sceneRecap;				// 4
		scene_shutdown			sceneShutdown;			// 5
		scene_final_brick		sceneFinalBrick;		// 6
		scene_0_initial			sceneInitial;			// 7

		// :: Current Scene ::
		int		SCENE_ID;
		float	SCENE_PERCENTAGE;

		// :: Previous Scene ::
		bool	SHOWING_PREVIOUS_SCENE;
		int		PREVIOUS_SCENE_ID;
		float	PREVIOUS_SCENE_START_TIME;
		float	OVERLAP_TIME_IN_SECS;
		int		PREVIOUS_SCENE_ALPHA;

		// :: Overlapping Scenes ::
		int		activeVideoPlayerId;
		int		activeDataDisplayId;
		int		availableVideoPlayerId;
		int		availableDataDisplayId;
		bool	overlappingVideoPlayer;
		bool	overlappingDataDisplay;
		
		void			setupScenes			( );
		int				stateToScene		( int stateId );
		void			startScene			( int sceneId );
		void			showPreviousScene	( bool show );
		void			updateScene			( int sceneId, bool isPrevious );
		void			drawScene			( int sceneId, bool isPrevious );

		void			setupSecuencia	( );
		void			setupGrid		( );
		void			setupMachineId	( );
		void			setupOSC		( );
		void			drawGrid		( );
		//void			loadFondo		( );
		//void			drawGrid		( );

		ofxXmlSettings	XMLinit;
		int				posX;

		ofFbo			mainFBO;
		ofTexture		mainTexture;


		GRID_POSICIONES posicionesGrid[BRIDGE_NUM_POSICIONES_GRID];

		void			updateBricks(vector<int> bricksType);
		vector<Brick>	bricks;

		int				secuenciaPosiciones[BRIDGE_NUM_POSICIONES_GRID];
		int				secuenciaPosicionesActual = 0;

		int				idMachine; //hay 2 bridges
		ofxOscSender	oscSenderCM;
		double			lastPing;

		bool			bChopeoLED = false;


		// :: TEST SCENES ::
		void	testStartScene(int sceneId);
		int		updateTestIdx(int idx, int limit);
		void	testManageVideos(int key);
		// General Data
		int		testState;
		int		testStateType;
		int		testTopicId;
		string	testCopy;
		string	testPath;
		string	testHighlights;
		string	testFact;
		string	testRecap;
		int		testGrow1;
		int		testGrow2;

		// Video Player
		int		tWelcome_idx = -1;
		int		tWelcome_vids = 2;
		int		tAdvertise_idx = -1;
		int		tAdvertise_vids = 2;
		int		tKeynote_idx = -1;
		int		tKeynote_vids = 3;
		int		tScreensaver_idx = -1;
		int		tScreensaver_vids = 7;
		// Recap
		int		tRecap_idx = -1;
		int		tRecap_vids = 2;

		// TEST :: AUTO LOOP
		void	testLoopModeChangeVideo();
		int		tLoopMode_idx;
		int		tLoopMode_max;

		bool KEYBOARD_TEST_MODE = false;

		ofFbo	fboFondo;

};
