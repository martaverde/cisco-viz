


#include "scene_data_display.h"
#include "oscManager.h"
#include "Constantes.h"
//#include "ofxEasing.h"

void scene_data_display::setup(vector<Brick> *bricks, GRID_POSICIONES *pos, int *_secuencia) {
	
	this->posicionesGrid = pos;
	this->bricks_global = bricks;
	this->secuenciaPosiciones = _secuencia;

	// Init
	stateType = 1;
	topicId = 0;
	copyText = "";
	highlights = "";
	path = "/videos/data_display/behavior/featured/";
	brickGrandeCentroPos = (84 * 5) + (84 / 2);;

	FACT_COPY.allocate(BRIDGE_WIDTH, BRIDGE_HEIGHT, GL_RGBA);
	PARAGRAPH_COPY.allocate(BRIDGE_WIDTH, BRIDGE_HEIGHT, GL_RGBA);
	//brickTime.allocate(BRIDGE_WIDTH, BRIDGE_HEIGHT, GL_RGBA);
	
	spacingHighlight = 30;
	// FACTS
	factNumberFontSize = 250;
	factTextFontSize = 130;
	// 1 Paragraph
	heightParagraphs[0] = 400;
	paragraphFontSize[0] = 80;
	//paragraphFontSize[0] = 170;
	// 2 Paragraphs
	heightParagraphs[1] = 300;
	paragraphFontSize[1] = 70;
	//paragraphFontSize[1] = 130;
	// 3 Paragraphs
	heightParagraphs[2] = 220;
	//paragraphFontSize[2] = 95;
	paragraphFontSize[2] = 70;

	ciscoSans.load("fonts/CiscoSansTTBold.ttf", factNumberFontSize, true, true);
	ciscoSans.setLineHeight(34.0f);
	ciscoSans.setLetterSpacing(1.035);
	ciscoSansLight.load("fonts/CiscoSansTTBold.ttf", factTextFontSize, true, true);
	ciscoSansLight.setLineHeight(34.0f);
	ciscoSansLight.setLetterSpacing(1);

	ciscoSansReferencia.load("fonts/CiscoSansTTBold.ttf", 71, true, true);
	ciscoSansReferencia.setLetterSpacing(1.035);


}

//void scene_data_display::start(int stateType_, int topicId_, string copyText_, string highlights_, string path_) {
void scene_data_display::start(int stateType_, int topicId_, string copyText_, string highlights_, string factText_, string path_, int _siguiente ) {
	// Get State values
	stateType = stateType_;
	topicId = topicId_;
	copyText = copyText_;
	highlights = highlights_;
	path = path_;
	this->siguientePosicionBrick = _siguiente;
	cout << "dada_display -> recibe siguiente posBrick= " << this->siguientePosicionBrick << endl;

	bBrickCreated = false; // para crear el brick cuando toca

	// :: Build paths ::
	if (stateType == 0) {
		path = "videos/data_display/" + ofToString(topicId) + "/featured/";
	}
	else if (stateType == 1) {
		path = "videos/data_display/" + ofToString(topicId) + "/secondary/";
	}

	// ::::::: VIDEO ::::::::
	video1.close();
	video2.close();
	video3.close();
	// ::: Load videos for FEATURED and SECONDARY :::
	if (stateType == 0 || stateType == 1) {
		cout << "antes path... " << path << endl;
		dir.listDir(path);
		cout << "despu�s path... " << dir.size() << endl;
		dir.sort();
		cout << "antes bucle" << endl;
		videosCargados = 0;
		if (dir.size() > 0) {
			for (int i = 0; i < (int)dir.size() && i < 3; i++) {
				if (i == 0)
				{
					video1.load(dir.getPath(i));
					video1.setLoopState(OF_LOOP_NONE);
					video1.play();
					videosCargados++;
				}
				if (i == 1)
				{
					video2.load(dir.getPath(i));
					video2.setLoopState(OF_LOOP_NONE);
					video2.play();
					videosCargados++;
				}
				if (i == 2)
				{
					video3.load(dir.getPath(i));
					video3.setLoopState(OF_LOOP_NONE);
					video3.play();
					videosCargados++;
				}
				cout << "video cargado: " << dir.getPath(i) << endl;
			}
		}
		cout << "salida bucle" << endl;
	}
	// ::: Load videos for MILESTONE :::
	else if (stateType == 2) {
		string milestonePathA = "videos/data_display/milestone/milestone_bridge_A.mov";
		string milestonePathB = "videos/data_display/milestone/milestone_bridge_B.mov";
		string milestonePathC = "videos/data_display/milestone/milestone_bridge_C_" + ofToString(topicId) + ".mov";
		video1.load(milestonePathA);
		video1.setLoopState(OF_LOOP_NONE);
		video1.play();
		video2.load(milestonePathB);
		video2.setLoopState(OF_LOOP_NONE);
		video2.play();
		video3.load(milestonePathC);
		video3.setLoopState(OF_LOOP_NONE);
		video3.play();
		videosCargados = 3;
		if (video1.isLoaded())
			cout << "video 1 cargado: " << milestonePathA << endl;
		else
			cout << "video 1 not loaded" << endl;
		if (video2.isLoaded())
			cout << "video 2 cargado: " << milestonePathB << endl;
		else
			cout << "video 2 not loaded" << endl;
		if (video3.isLoaded())
			cout << "video 3 cargado: " << milestonePathC << endl;
		else
			cout << "video 3 not loaded" << endl;
	}



	// :::::::: PLACEHOLDER (minimal) DATA FOR FEATURED AND MILESTONE :::::::::::::::::::::
	vector<string> factParsed_ = ofSplitString(factText_, "|", true, true);
	if (factParsed_.size() == 2) {
		factNumberString = factParsed_[0];
		factTextString = factParsed_[1];
	} else {
		factNumberString = "";
		factTextString = "";
	}
	showingFact = false;

	// ::::::: TEXT ::::::::
	setColors();
	vector<string> copyParsed_ = ofSplitString(copyText, "|", false, true);
	vector<string> highlightsLines_ = ofSplitString(highlights, "|", false, true);
	int pWidth = BRIDGE_WIDTH;
	
	cout << "DataDisplay -> Hay: " << copyParsed_.size() << " lineas " << endl;

	int pLeading = 0;
	if (copyParsed_.size()>1)
		pLeading = paragraphFontSize[copyParsed_.size() - 1] * .65;
	
	paragraphs.clear();

	for (int i = 0; i < 3; i++) {
		if (i < copyParsed_.size()) {
			copyParsed[i] = copyParsed_[i];
		} else {
			copyParsed[i] = "";
		}
		if (i < highlightsLines_.size()) {
			highlightsLinesToParse[i] = highlightsLines_[i];
		}
		else {
			highlightsLinesToParse[i] = "";
		}
	}
	
	float anchoMax = 0;
	float altoMax = 400;

	for (int i = 0; i < copyParsed_.size(); i++) {
		ofxParagraph* p = new ofxParagraph();
		paragraphs.push_back(p);

		paragraphs[i]->setText(copyParsed[i]);
		paragraphs[i]->setFont(ofxSmartFont::add("fonts/CiscoSansTTBold.ttf", 70, "cisco-bold"));
		paragraphs[i]->setWidth(pWidth);
		paragraphs[i]->setSpacing(paragraphFontSize[copyParsed_.size() - 1]/2);
		paragraphs[i]->setLeading(pLeading);
		if (stateType == 0 || stateType == 1) {
			paragraphs[i]->setColor(ofColor(255), secondColor);
		}
		else {
			paragraphs[i]->setColor(ofColor(255), darkBlue);
		}
		paragraphs[i]->setAlignment(ofxParagraph::ALIGN_CENTER);
		//paragraphs[i]->setPosition(BRIDGE_WIDTH / 2 - paragraphs[i]->getWidth() / 2, heightParagraphs[copyParsed_.size()-1] + i*paragraphFontSize[copyParsed_.size() - 1] *1.5f);
		float anchoP = ciscoSansReferencia.getStringBoundingBox(copyParsed[i], 0, 0).getWidth();
		if (anchoP > anchoMax)
			anchoMax = anchoP;
	}
	//if (anchoMax < 1500)
	//	anchoMax = 2000;
	cout << "anchoMax= " << anchoMax << endl;

	fboText.allocate(anchoMax,altoMax, GL_RGBA);
	fboText.begin();
		ofClear(0, 0);
		float altoP = 70;
		float startY;
		if (paragraphs.size() == 1) startY = altoMax / 2 - altoP / 2;
		if (paragraphs.size() == 2) startY = altoMax / 2 - (altoP + 70/3);
		if (paragraphs.size() == 3) startY = altoMax / 2 - (altoP + 70 / 3 + altoP/2);
		for (int i = 0; i < paragraphs.size(); i++)
		{
			paragraphs[i]->setPosition(anchoMax / 2.0f - paragraphs[i]->getWidth() / 2.0f, startY + i * paragraphFontSize[copyParsed_.size() - 1] * 1.5f);
			paragraphs[i]->draw();
			vector <string> partes = ofSplitString(highlightsLinesToParse[i], "//", true, true);
			for (int j = 0; j < partes.size(); j++) {
				vector <string> index = ofSplitString(partes[j], ",", true, true);
				paragraphs[i]->drawHighLight(ofToInt(index[0]), ofToInt(index[1]));
			}
		}
	fboText.end();
	
	//posici�n fija de  n�mero de claim principal. El digito aumenta y desplaza la caja de texto
	factNumberBox = ciscoSans.getStringBoundingBox(factNumberString, 0, 0);
	factTextBox = ciscoSansLight.getStringBoundingBox(factTextString, 0, 0);
	factBoxTotalWidth = factNumberBox.width + factTextBox.width + spacingHighlight;

	//==== motion array load 
	buffer = ofBufferFromFile("motionArray.txt");
	positions = ofToString(buffer);
	result = ofSplitString(positions, ",");

	initTime = ofGetElapsedTimeMillis();
	easedPos = BRIDGE_HEIGHT / 1.5;

	//hay que hacer un fade de los bricks que no sean del tipo que toca
	if (stateType == 1)
	{
		int alpha = ALPHA_FADED;
		for (int i = 0; i < bricks_global->size(); i++)
		{
			if (bricks_global->at(i).getTipo() != topicId)
				bricks_global->at(i).setAlphaTarget(alpha, 0.8f);
		}
	}


	cout << "data display start end" << endl;
	//cout << "dada_display recibe siguiente posBrick= " << this->siguientePosicionBrick << endl;

}


void scene_data_display::update(){
	//cout << ofGetElapsedTimeMillis() - initTime << " update dada_display recibe siguiente posBrick= " << this->siguientePosicionBrick << endl;

	video1.update();
	video2.update();
	video3.update();
	//video1.setPosition(0.99f);
	//video2.setPosition(0.99f);
	//video3.setPosition(0.99f);

	if (videosCargados > 0){
		percentage = video1.getPosition();
		duration = video1.getDuration();
		framesTotal = duration * ofGetFrameRate();
		frameCount = ofMap(percentage, 0, 1, 0, framesTotal);
	}
	
	if (stateType == 0) {
		if (ofGetElapsedTimeMillis() - initTime > 8000 && ofGetElapsedTimeMillis() - initTime < 14000) {
			newPos = ofMap(ofGetElapsedTimeMillis() - initTime, 8000, 14000, 0, 180, true);
			easedPos = ofToFloat(result[int(newPos)]);
			easedPos = ofMap(easedPos, 0, 1, BRIDGE_HEIGHT, -600);
			showingFact = true;
		} else {
			showingFact = false;
		}
	}
	else if (stateType == 2) {
		if (ofGetElapsedTimeMillis() - initTime > 24000 && ofGetElapsedTimeMillis() - initTime < 28500) {
			newPos = ofMap(ofGetElapsedTimeMillis() - initTime, 24000, 28500, 0, 180, true);
			easedPos = ofToFloat(result[int(newPos)]);
			easedPos = ofMap(easedPos, 0, 1, -600, BRIDGE_HEIGHT);
			showingFact = true;
		} else {
			showingFact = false;
		}
	}


	

	// :: FACT ::
	FACT_COPY.begin();
		ofClear(0,0);
		ofPushMatrix();
		ofTranslate(video1.getWidth() / 2, easedPos);
			ofSetColor(firstColor);
			ciscoSans.drawString(factNumberString, -factNumberBox.width/2, 50);
			ofSetColor(secondColor);
			ciscoSansLight.drawString(factTextString, -factTextBox.width/2, 260);
		ofPopMatrix();
	FACT_COPY.end();

	PARAGRAPH_COPY.begin();
	ofClear(0, 0);
	for (int i = 0; i < paragraphs.size(); i++) {
		float anchoTarget = BRIDGE_WIDTH - (1008 * 2);
		if (fboText.getWidth() <=anchoTarget)
			fboText.draw(BRIDGE_WIDTH/2- fboText.getWidth()/2, 420 - fboText.getHeight() / 2);
		else
		{
			float aspect = fboText.getWidth() / fboText.getHeight();
			float sizeX = anchoTarget;
			float sizeY = anchoTarget / aspect;

			fboText.draw(1008, 350 - sizeY / 2, sizeX, sizeY);
			//fboText.draw(1008, 600);
			//ofNoFill();
			//ofDrawRectangle(1008, 600, fboText.getWidth(), fboText.getHeight());
			//ofFill();
		}
		//fboText.draw(1008, 200);
		//paragraphs[i]->draw();
		//// split string from highlights
		//vector <string> partes = ofSplitString(highlightsLinesToParse[i], "//", true, true);
		//for (int j = 0; j < partes.size(); j++) {
		//	vector <string> index = ofSplitString(partes[j], ",", true, true);
		//	paragraphs[i]->drawHighLight(ofToInt(index[0]), ofToInt(index[1]));
		//}
	}
	PARAGRAPH_COPY.end();
	// cout << "data 3" << endl;

	//pintar brick inicio en ofGetWidth()/2, ofGetHeight()/4, escalado + 3
	//brickTime.begin();
	//	ofClear(0, 0);
	//	
	//	ofSetColor(255);
	//	newBrick(topicId);

	//	for (int i = 0; i < groupOfBricks.size(); i++) {
	//			groupOfBricks[i].draw();
	//	}

	//brickTime.end();

	// cout << "data 4" << endl;
	
	//si estamos en feature o milestone, se tiene que crear y mostrar el brick al final
	if ((stateType == 0 || stateType == 2) && !bBrickCreated)
	{
		//if (ofGetElapsedTimeMillis() - initTime > 1000)
		if (ofGetElapsedTimeMillis() - initTime > 29500)
		{
			Brick *brick = new Brick;
			ofVec2f posBrick = posicionesGrid[brickGrandeCentroPos].pos;
			posBrick.x = (float)BRIDGE_WIDTH / 2.0f;
			//posBrick.y = posBrick.y + ancho / 2.0f;
			posBrick.y = 336;
			brick->setup(posBrick, topicId, bricks_global->size(), 0, 0, ofGetElapsedTimeMillis());
			bricks_global->push_back(*brick);
			bricks_global->back().setScaleGlobal(6.0f);
			bricks_global->back().addAnimation(posicionesGrid[brickGrandeCentroPos].pos, Brick::ANIMACION_BRICK::DELAY, 1000);
			bricks_global->back().addAnimation(posicionesGrid[brickGrandeCentroPos].pos, Brick::ANIMACION_BRICK::RESCALE, 2000);
			//bricks_global->back().addAnimation(posicionesGrid[brickGrandeCentroPos].pos, Brick::ANIMACION_BRICK::DELAY, 10000);
			int pos = secuenciaPosiciones[siguientePosicionBrick];
			cout << "siguiente posicion data display = " << siguientePosicionBrick << ", posicionGrid =  " << pos << "coordenadas: " << posicionesGrid[pos].pos << endl;
			bricks_global->back().addAnimation(posicionesGrid[pos].pos, Brick::ANIMACION_BRICK::DUMP_BRIDGE, 0);
			bricks_global->back().startAnimation();
			bBrickCreated = true;
		}
	}

	//en modo secondary hay que rebajar el alpha de los que no son el brick creado
	float ahora = ofGetElapsedTimeMillis() - initTime;
	if (stateType == 1 && (ahora > 17000 && ahora < 22000))
	{
		int alpha = ofMap(ahora, 17000, 21000, ALPHA_FADED, 250, true);
		for (int i = 0; i < bricks_global->size(); i++)
		{
			if (bricks_global->at(i).getTipo() != topicId)
				bricks_global->at(i).setAlphaTarget(alpha, 0.8f);
		}
	}

}

void scene_data_display::draw(int alpha) {
	// ofBackground(0);
	// cout << "draw1" << endl;
	//cout << ofGetElapsedTimeMillis() - initTime << " draw dada_display recibe siguiente posBrick= " << this->siguientePosicionBrick << endl;
	
	//en feature y milestone se tiene que dibujar el brick
	//if ((stateType == 0 || stateType == 2))
		//if (ofGetElapsedTimeMillis() - initTime > 25000)
		//if (ofGetElapsedTimeMillis() - initTime > 100)
				//drawBrick();
	if (ofGetElapsedTimeMillis() - initTime > 14500)
			drawBrick();

	if (stateType == 0) drawFeatured(alpha);
	if (stateType == 1) drawSecondary(alpha);
	if (stateType == 2) drawMilestone(alpha);
	/*
	ofSetColor(frontColor);
	ofDrawRectangle(ofGetWidth() / 2, ofGetHeight() / 2, 100, 100);

	ofSetColor(backColor);
	ofDrawRectangle(ofGetWidth() / 2 + 100, ofGetHeight() / 2, 100, 100);
	*/
	//entre 5 y 8 segundos inicia colocacion brick


	//drawGrid();
	
	// Draw the FPS display
	/*
	ofSetColor(255);
	ofDrawBitmapString(ofToString(ofGetFrameRate(), 0) + " FPS", 20, 20);
	ofDrawBitmapString(percentage, 20, 400);
	*/
}


void scene_data_display::drawBrick() {

	ofSetColor(255);
	ofPushStyle();
	for (int i = 0; i < bricks_global->size(); i++)
		bricks_global->at(i).draw();
	ofPopStyle();

}


void scene_data_display::drawFeatured(int alpha) {
	ofSetColor(255, alpha);
	//ofSetColor(255, 200);
	if (video1.isLoaded() && !video1.getIsMovieDone())
		video1.draw(0, 0);
	if (showingFact)
		FACT_COPY.draw(0, 0);
	if (video2.isLoaded() && !video2.getIsMovieDone())
		video2.draw(0, 0);
	if (frameCount > 15 * ofGetFrameRate() && frameCount < 26 * ofGetFrameRate()) {
		PARAGRAPH_COPY.draw(0, 0);
	}
	if (video3.isLoaded() && !video3.getIsMovieDone())
		video3.draw(0, 0);

}

void scene_data_display::drawSecondary(int alpha) {
	ofSetColor(255, alpha);
	if (video1.isLoaded() && !video1.getIsMovieDone())
		video1.draw(0, 0);
	if (frameCount > 5 * ofGetFrameRate() && frameCount < 15 * ofGetFrameRate())
		PARAGRAPH_COPY.draw(0, 0);
	if (video2.isLoaded() && !video2.getIsMovieDone())
		video2.draw(0, 0);
}

void scene_data_display::drawMilestone(int alpha) {
	if (video1.isLoaded() && !video1.getIsMovieDone())
		video1.draw(0, 0);
	if (frameCount > 15.f * ofGetFrameRate() && frameCount < 25.f * ofGetFrameRate())
		PARAGRAPH_COPY.draw(0, 0);
	if (video2.isLoaded() && !video2.getIsMovieDone())
		video2.draw(0, 0);
	if (showingFact)
		FACT_COPY.draw(0, 0);
	if (video3.isLoaded() && !video3.getIsMovieDone())
		video3.draw(0, 0);
}

void scene_data_display::setColors() {
	white = ofColor(255, 255, 255);

	green = ofColor(115, 183, 54);
	red = ofColor(202, 27, 25);
	yellow = ofColor(248, 172, 0);
	blue = ofColor(26, 171, 227);

	darkGreen = ofColor(34, 92, 58);
	darkRed = ofColor(135, 0, 15);
	darkBlue = ofColor(22, 73, 108);
	darkYellow = ofColor(241, 151, 27);

	if (topicId == 0) {
		firstColor = yellow;
		secondColor = darkYellow;
	}
	else if (topicId == 1 || topicId == 2) {
		firstColor = white;
		secondColor = red;
	}
	else if (topicId == 3 || topicId == 4) {
		firstColor = white;
		secondColor = darkRed;
	}
	else if (topicId == 5) {
		firstColor = red;
		secondColor = darkRed;
	}
	else if (topicId == 6 || topicId == 8) {
		firstColor = darkGreen;
		secondColor = green;
	}
	else if (topicId == 7) {
		firstColor = white;
		secondColor = green;
	}
	else if (topicId == 9) {
		firstColor = white;
		secondColor = blue;
	}

	if (stateType == 2) {
		firstColor = white;
		secondColor = white;
	}

	//return firstColor;
	//return secondColor;
}


void scene_data_display::newBrick(int tipoBrick) {
	Brick tempBrick;
	int in = 3;
	int out = 1;

//indexPosition = buscaPosicionNuevoBrick();
//ofVec2f initPos = posicionesGrid[indexPosition].pos;
	//  std::cout << indexPosition << initPos << endl;
	//BUSCAR POSICI�N INICIAL CUADRADA CON EL V�DEO
//	ofVec2f initPos = gridPositions[50];
	tempBrick.setup(ofVec2f(BRIDGE_WIDTH/2, BRIDGE_HEIGHT/4), tipoBrick, groupOfBricks.size(), in, out, ofGetElapsedTimeMillis());
	groupOfBricks.push_back(tempBrick);

	// std::cout << "Type of new brick: " << tempTypeBrick << endl;
	// std::cout << "Number of bricks: " << groupOfBricks.size() << endl;

//	brickCounter++;
	//indexPosition = filaSwap[counter];
	//shuffle (filaSwap.begin(), filaSwap.end(), std::default_random_engine(seed));
//	timestamp = ofGetTimestampString();


//string brickIsNew = ofToString(groupOfBricks.size()) + "," + ofToString(tipoBrick) + "," + ofToString(indexPosition);
//	std::cout << brickIsNew << endl;
	//data->sendNewBrick(brickIsNew);

}

void scene_data_display::drawGrid() {
	for (int i = 0; i < BRIDGE_WIDTH; i = i + 56) {
		ofSetColor(255);
		ofDrawLine(i, 0, i, BRIDGE_HEIGHT);
	}
	for (int j = 0; j < BRIDGE_HEIGHT; j = j + 56) {
		ofDrawLine(0, j, BRIDGE_WIDTH, j);
	}
}

void scene_data_display::sceneFinished(bool _finished) {
	finished = _finished;
}

float scene_data_display::getPercentage() {
	return percentage;
}


void scene_data_display::pauseVideo(bool paused) {
	if (video1.isLoaded()) {
		video1.setPaused(paused);
	}
	if (video2.isLoaded()) {
		video2.setPaused(paused);
	}
	if (video3.isLoaded()) {
		video3.setPaused(paused);
	}
}

void scene_data_display::cueVideo(bool rewind) {
	if (video1.isLoaded()) {
		float currentPosition = video1.getPosition();
		if (rewind) {
			currentPosition -= CUE_FOR_TESTING;
		}
		else {
			currentPosition += CUE_FOR_TESTING;
		}
		video1.setPosition(currentPosition);
	}
	if (video2.isLoaded()) {
		float currentPosition = video2.getPosition();
		if (rewind) {
			currentPosition -= CUE_FOR_TESTING;
		}
		else {
			currentPosition += CUE_FOR_TESTING;
		}
		video2.setPosition(currentPosition);
	}
	if (video3.isLoaded()) {
		float currentPosition = video3.getPosition();
		if (rewind) {
			currentPosition -= CUE_FOR_TESTING;
		}
		else {
			currentPosition += CUE_FOR_TESTING;
		}
		video3.setPosition(currentPosition);
	}
}