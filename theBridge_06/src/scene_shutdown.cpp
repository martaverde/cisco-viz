

#include "scene_shutdown.h"
#include "Brick.h"
#include "Constantes.h"
//la escena en total dura 1800 segundos

#define SHUTDOWN_BRIDGE_COMPLETED 15 * 1000



//pedir a State machine dos estados de crecimiento, Array. 

void scene_shutdown::setup(vector<Brick> *bricks, GRID_POSICIONES *pos, int *_secuencia) {

	sceneDuration = 1800000;
	
	this->posicionesGrid = pos;
	this->bricks_global = bricks;
	this->secuenciaPosiciones = _secuencia;
}

void scene_shutdown::start() {


	initTime = ofGetElapsedTimeMillis();

	lastIndex = 0;
	cout << "fin setShutdown" << endl;
}


void scene_shutdown::update() {

}

void scene_shutdown::draw(int alpha) {

	// ofBackground(0);
	//ofEnableAlphaBlending();
	ofSetColor(255, alpha);
	//ofEnableBlendMode(OF_BLENDMODE_ALPHA);

	float now = ofGetElapsedTimeMillis() - initTime;

	ofPushStyle();

	for (int i = 0; i < bricks_global->size(); i++) {
		bricks_global->at(i).draw();
	}
	int loop = (int)now % 5000;
	int index = ofMap(loop, 0, 5000, 0, bricks_global->size(), true);

	if (index > lastIndex)
		for (int i = lastIndex + 1; i <= index; i++)
		{
			//cout << "animando brick  " << index << endl;
			bricks_global->at(i).setScaleGlobal(1.0f);
			bricks_global->at(i).addAnimation(bricks_global->at(i).getPos(), Brick::ANIMACION_BRICK::GROW_STATUS, 1000);
			bricks_global->at(i).startAnimation();
		}

	lastIndex = index;

	ofPopStyle();

	//ofSetRectMode(OF_RECTMODE_CENTER);

}



void scene_shutdown::keyReleased(int key) {
}


