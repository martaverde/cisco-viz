#pragma once
#include "ofMain.h"
#include "ofxXmlSettings.h"
#include "ofxNDI.h"



#define PANTALLA_LED_WIDTH		504
#define PANTALLA_LED_HEIGHT		672

class ofApp : public ofBaseApp {

public:
	void setup();
	void update();
	void draw();

	void keyPressed		(int key );
	void mouseMoved		(int x, int y );
	void mouseDragged	(int x, int y, int button );
	void mousePressed	(int x, int y, int button );
	void mouseReleased	(int x, int y, int button );
	void mouseEntered	(int x, int y );
	void mouseExited	(int x, int y );
	void windowResized	(int w, int h );
	void dragEvent		(ofDragInfo dragInfo );
	void gotMessage		(ofMessage msg );

private:
	void updateNDI						( );


	ofxXmlSettings	xml;

	ofxNDIreceiver ndiReceiverFront[6]; // NDI receiver
	ofxNDIreceiver ndiReceiverBack[6];  // NDI receiver

	string ndiReceiverFrontName[6]; // NDI receiver
	string ndiReceiverBackName[6];  // NDI receiver

	ofImage ndiImageFront[6];				// Image to receive
	ofImage ndiImageBack[6];			 // Image to receive

	bool bNDIFrontReady[6];
	bool bNDIBackReady[6];
	double lastNDIupdate;
	float ancho;
	float alto;

};

