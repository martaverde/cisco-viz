#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main( ){
	ofGLFWWindowSettings settings;
	 settings.width = 504 * 4;
     settings.height = 1080;
	 settings.decorated = true;
	 settings.windowMode = OF_WINDOW;
	 ofCreateWindow(settings);
	ofRunApp(new ofApp());

}
