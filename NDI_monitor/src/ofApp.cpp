
#include "ofApp.h"


//--------------------------------------------------------------
void ofApp::setup(){
	

	ancho = 1920.0f / 6.0f;
	float aspect = (float)PANTALLA_LED_WIDTH / (float)PANTALLA_LED_HEIGHT;
	alto = ancho / aspect;
	ofSetWindowShape(1920, alto * 2);

	string s ="0";
	for (int i = 0; i < 1332; i++)
	{
		s = s + "," + ofToString(i);
	}

	ofFile f;
	f.open("posiciones.xml", ofFile::Append);
	f.writeFromBuffer(s);
	f.close();

}

void ofApp::updateNDI()
{

	for (int i = 0; i < 6; i++)
	{
		if (!ndiImageFront[i].isAllocated())
			ndiImageFront[i].allocate(PANTALLA_LED_WIDTH/2, PANTALLA_LED_HEIGHT/2, OF_IMAGE_COLOR_ALPHA);

		if (!ndiImageBack[i].isAllocated())
			ndiImageBack[i].allocate(PANTALLA_LED_WIDTH/2, PANTALLA_LED_HEIGHT/2, OF_IMAGE_COLOR_ALPHA);
		
		if (ndiReceiverFront[i].GetSenderCount()>0)
			if (ndiReceiverFront[i].GetSenderName() != ndiReceiverFrontName[i])
			{
				bNDIFrontReady[i] = false;
				cout << "ndiReceiverFront " << i << " no tiene nombre valido -> " << endl;
			}

		if (ndiReceiverBack[i].GetSenderCount() > 0)
			if (ndiReceiverBack[i].GetSenderName() != ndiReceiverBackName[i])
			{
				bNDIBackReady[i] = false;
				cout << "ndiReceiverBack " << i << " no tiene nombre valido" << endl;

			}
		//bNDIBackReady[i] = false;

	}


	int nsenders = ndiReceiverFront[0].GetSenderCount();
	char name[256];
	cout << "-----------------------------------------------------------" << endl;
	if (nsenders > 0) {
		//cout << "Number of NDI senders found: " << nsenders << endl;
		for (int i = 0; i < nsenders; i++)
		{
			ndiReceiverFront[0].GetSenderName(name, 256, i);
			string nombre(name);
			//cout << "    Sender " << i << " [" << nombre << "]" << endl;
			for (int instance = 0; instance < 6; instance++)
			{
				if (nombre.find("KINECT" + ofToString(instance+1) + "_FRONT") != string::npos)
				{
					if (ndiReceiverFront[instance].GetSenderName() != ndiReceiverFrontName[instance] || ofGetElapsedTimeMillis() < 20000)
					{
						ndiReceiverFront[instance].SetSenderIndex(i);
						ndiReceiverFrontName[instance] = ndiReceiverFront[instance].GetSenderName();
						cout << "Instancia " << instance << " asociada al NDI front " << nombre << endl;
						bNDIFrontReady[instance] = true;
					}
					break;
				}

			}
			for (int instance = 0; instance < 6; instance++)
			{
				if (nombre.find("KINECT" + ofToString(instance + 1) + "_BACK") != string::npos)
				{
					if (ndiReceiverBack[instance].GetSenderName() != ndiReceiverBackName[instance] || ofGetElapsedTimeMillis() < 20000)
					{
						ndiReceiverBack[instance].SetSenderIndex(i);
						ndiReceiverBackName[instance] = ndiReceiverBack[instance].GetSenderName();
						cout << "Instancia " << instance << " asociada al NDI BACK " << nombre << endl;
						bNDIBackReady[instance] = true;
					}
					break;
				}
			}
		}
	}
	else
	{
		cout << "found 0 senderes,  NENG" <<  endl;
	}



}

//--------------------------------------------------------------
//
void ofApp::update(){

	if (ofGetElapsedTimeMillis() - lastNDIupdate > 5000)
	{
		updateNDI();
		lastNDIupdate = ofGetElapsedTimeMillis();
	}
	//cout << (int)ofRandom(0, 11) << endl;
}

//--------------------------------------------------------------
void ofApp::draw(){

	ofBackground(0);


	ofSetColor(255);
	for (int i = 0; i < 6; i++)
	{
		ndiReceiverBack[i].ReceiveImage(ndiImageBack[i]);
		ndiReceiverFront[i].ReceiveImage(ndiImageFront[i]);
		ofSetColor(255);
		if (bNDIFrontReady[i])
			ndiImageFront[i].draw(ancho * i, 0,ancho,alto);
		if (bNDIBackReady[i])
			ndiImageBack[i].draw(ancho * i, alto,ancho,alto);
		ofSetColor(255, 0, 0);
		ofDrawLine(ancho*i, 0, ancho*i, alto * 2);
	}

	ofDrawLine(0, alto, 1920, alto);

	string fps = "FPS: " + ofToString(ofGetFrameRate());
	ofSetWindowTitle(fps);

}




//--------------------------------------------------------------
//
void ofApp::keyPressed(int key){
	//if (key == 'd') kinect.toggleDebug();
	//if (key == 'j') renderer.toggleJoints();
	//if (key == 'b') renderer.toggleBones();
	//if (key == 'h') renderer.toggleHands();
	//if (key == 'r') renderer.toggleRanges();
}

/*
void ofApp::move(ofVec2f position){
    groupOfBricks[groupOfBricks.size()-1].zenoToPoint(position);
}
*/


//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
   
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
   
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
